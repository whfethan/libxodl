#!/usr/bin/env python3
'''
/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/
'''

import os
import sys
import subprocess
import array

OUTPUT_PATH = "src/charfact.c"

FACT_ObjectIDChar    = 1
FACT_IdentStart      = 1 << 1
FACT_IdentContinue   = 1 << 2
FACT_Whitespace      = 1 << 3
FACT_Newline         = 1 << 4
FACT_Base64          = 1 << 5
FACT_Digit           = 1 << 6
FACT_Hex             = 1 << 7
FACT_NonPrintable    = 1 << 8
FACT_ValidEscapeCode = 1 << 9
FACT_Invalid         = 0

charattr_tab = array.array('I', (FACT_Invalid for i in range(0, 128)))

#
# do_exec_with_input(cmd, [input_string]) -> stdout:
# Execute the given commandline with optionally input content and return the
# standard output. Exception would be raised if return code is not 0.
#
def do_exec_with_input(cmd, input_string=""):
    p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    stdout, _ = p.communicate(input_string)
    retcode = p.wait()
    if retcode != 0:
        raise subprocess.CalledProcessError(retcode, cmd)
    return stdout

def __assign_attribute(n, attr):
    charattr_tab[n] |= attr

def assign_attribute(ch, attr):
    __assign_attribute(ord(ch), attr)

def __assign_attribute_batch(nf, nt, attr):
    for i  in range(nf, nt + 1): __assign_attribute(i, attr)

def assign_attribute_batch(ch_start, ch_end, attr):
    __assign_attribute_batch(ord(ch_start), ord(ch_end), attr)

def setup_attrtab():
    # Identifiers
    assign_attribute('_', FACT_IdentStart | FACT_IdentContinue)
    assign_attribute_batch('a', 'z', FACT_IdentStart | FACT_IdentContinue)
    assign_attribute_batch('A', 'Z', FACT_IdentStart | FACT_IdentContinue)
    assign_attribute_batch('0', '9', FACT_IdentContinue)
    assign_attribute('-', FACT_IdentContinue)
    assign_attribute('.', FACT_IdentContinue)

    # ObjectID
    assign_attribute_batch('A', 'F', FACT_ObjectIDChar)
    assign_attribute_batch('0', '9', FACT_ObjectIDChar)

    # Whitespaces
    __assign_attribute(32, FACT_Whitespace)
    __assign_attribute(9, FACT_Whitespace)
    __assign_attribute(11, FACT_Whitespace)
    __assign_attribute(12, FACT_Whitespace)

    # Newline
    assign_attribute('\r', FACT_Newline)
    assign_attribute('\n', FACT_Newline)

    # Base64
    assign_attribute_batch('A', 'Z', FACT_Base64)
    assign_attribute_batch('a', 'z', FACT_Base64)
    assign_attribute_batch('0', '9', FACT_Base64)
    assign_attribute('+', FACT_Base64)
    assign_attribute('/', FACT_Base64)

    # HEX
    assign_attribute_batch('0', '9', FACT_Digit | FACT_Hex)
    assign_attribute_batch('A', 'F', FACT_Hex)
    assign_attribute_batch('a', 'f', FACT_Hex)

    # NonPrintable
    __assign_attribute_batch(1, 0x8, FACT_NonPrintable)
    __assign_attribute_batch(0x0a, 0x1f, FACT_NonPrintable)
    __assign_attribute(0x7f, FACT_NonPrintable)

    # Valid escape sequences
    __assign_attribute(0x27, FACT_ValidEscapeCode)
    __assign_attribute(0x22, FACT_ValidEscapeCode)
    __assign_attribute(0x5c, FACT_ValidEscapeCode)
    assign_attribute('b', FACT_ValidEscapeCode)
    assign_attribute('f', FACT_ValidEscapeCode)
    assign_attribute('n', FACT_ValidEscapeCode)
    assign_attribute('r', FACT_ValidEscapeCode)
    assign_attribute('t', FACT_ValidEscapeCode)
    assign_attribute('u', FACT_ValidEscapeCode)
    assign_attribute('U', FACT_ValidEscapeCode)


def gen_tab_content():
    out = ""

    for attr in charattr_tab:
        out += "0x%04x," % attr

    return out[:-1]

def wrap_content(out):
    return """/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

/*------------------------------------------------------------------------------
  This file is automatically generated by gencharfact.py and should not be
  modified manually.
------------------------------------------------------------------------------*/

#include <stdint.h>

uint16_t gCharFactTab[__UINT8_MAX__] = {
    %s
};

""" % (out)

def main():
    try:
        script_dir = os.path.dirname(sys.argv[0])
        root_dir = os.path.join(script_dir, '..')

        setup_attrtab()
        out = gen_tab_content()
        out = wrap_content(out)

        # Final formatting.
        clang_format_path = 'clang-format'
        out = do_exec_with_input([clang_format_path, '--style=file'], out.encode())

        with open(os.path.join(root_dir, OUTPUT_PATH), 'w') as f:
            f.write(out.decode("utf-8"))

        return 0

    except subprocess.CalledProcessError as e:
        sys.stderr.write("Error calling '{}'\n".format(" ".join(e.cmd)))
        return e.returncode


if __name__ == '__main__':
    sys.exit(main())
