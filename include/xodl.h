/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#ifndef LIBXODL_XODL_H
#define LIBXODL_XODL_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <math.h>

typedef struct SXODLStateImpl XODLState, *PXODLState;
typedef struct SXODLDocumentImpl XODLDocument, *PXODLDocument;
typedef enum {
    XODL_TYPE_Document = 1,
    XODL_TYPE_Schema
} EXODLDocumentType;

typedef void (*FXODLParseErrorHandler)(
    const char *        /* fname */,
    uint32_t            /* line */,
    uint32_t            /* startcol */,
    uint32_t            /* errcol */,
    const char *        /* errmsg */,
    void*               /* usrarg */
);
typedef enum {
    MESSAGE_Verbose,
    MESSAGE_Warning,
    MESSAGE_Error
} EMessageLevel;
typedef void (*FXODLMessageHandler)(
    EMessageLevel       /* eLevel */,
    const char *        /* szFileName */,
    const char *        /* szMessage */,
    void *              /* lpUserArg */
);
typedef bool (*FXODLUserFileLoad)(
    const char *        /* szFileName */,
    void **             /* lpContentOut */,
    size_t *            /* lpulFileSize */,
    void **             /* lppFileArgOut */,
    void *              /* lpUserArg */
);
typedef void (*FXODLUserFileUnload)(
    const char *        /* szFileName */,
    void *              /* lpContent */,
    size_t              /* ulFileSize */,
    void *              /* lpFileArg */,
    void *              /* lpUserArg */
);
typedef struct {
    bool                bUseHardTab : 1;
    bool                bUseBinaryForm : 1;
    bool                bMinify : 1;
    uint8_t             nIndentSize;
} XODLExportSettings, *PXODLExportSettings;

/* Please keep this in sync with the latest XODL specification!! */
#define XODL_IDENTIFIER_MAXLENGTH       255
#define XODL_OID_LENGTH                 24
#define XODL_OBJPATH_MAXLENGTH          65535
#define XODL_MAX_PROXY_REDIRECTION      16

typedef const char * XODLObjectId;
typedef struct SXODLObject XODLObject, *PXODLObject;
typedef struct SXODLObjectMemberIterator XODLObjectMemberIterator, *PXODLObjectMemberIterator;
typedef enum {
    XODL_MEMBER_Object,
    XODL_MEMBER_Property
} EXODLMemberKind;

typedef struct SXODLProperty XODLProperty, *PXODLProperty;
typedef enum {
    XODL_VALUE_Nil,
    XODL_VALUE_String,
    XODL_VALUE_RawString,
    XODL_VALUE_Data,
    XODL_VALUE_Integer,
    XODL_VALUE_Real,
    XODL_VALUE_Boolean,
    XODL_VALUE_Object,
    XODL_VALUE_ObjectID,
    XODL_VALUE_ObjectPath,
    XODL_VALUE_List,
    XODL_VALUE_RegExp,
    XODL_VALUE_TYPE_NAME_INTEGER,
    XODL_VALUE_TYPE_NAME_BOOLEAN,
    XODL_VALUE_TYPE_NAME_REAL,
    XODL_VALUE_TYPE_NAME_STRING,
    XODL_VALUE_TYPE_NAME_OID,
    XODL_VALUE_TYPE_NAME_OBJPATH,
    XODL_VALUE_TYPE_NAME_LIST,
    XODL_VALUE_TYPE_NAME_ANYTYPE
#define XODL_SCHEMA_VALUE_START         XODL_VALUE_RegExp
#define XODL_SCHEMA_VALUE_END           XODL_VALUE_TYPE_NAME_ANYTYPE
} EXODLValueKind;
typedef struct SXODLValue XODLValue, *PXODLValue;

#define OID_VALUE(val)    #val
#define OID_ANON          NULL

#ifndef NAN
#   error NAN is unsupported by the current libc
#endif
#ifndef INFINITY
#   error INFINITY is unsupported by the current libc
#endif
#define XODL_NaN          NAN
#define XODL_Inf          INFINITY

/*------------------------------------------------------------------------------
  XODL State Operations
------------------------------------------------------------------------------*/

/**
 * @brief Create a XODL state instance.
 *
 * @param[in] fnLoadFile Function should the new state use to load a file.
 * @param[in] fnUnloadFile (optional) Function should the new state use to unload a file.
 * @return Pointer to the newly allocated XODLState structure.
 */
PXODLState XODLStateCreate (
    FXODLUserFileLoad       fnLoadFile,
    FXODLUserFileUnload     fnUnloadFile
);

/**
 * @brief Clear all cached documents from a XODL state.
 *
 * @param[in] lpState Pointer to the XODL state structure.
 */
void XODLStateClear (
    PXODLState              lpState
);

/**
 * @brief Free a XODL state.
 *
 * This routine releases all internal resources held by the state and replaces
 * the original pointer to the state with \c NULL .
 *
 * @param[in,out] lppState Pointer of the pointer to the actual XODL state strcuture.
 */
void XODLStateDestroySafe (
    PXODLState *            lppState
);

/**
 * @brief Setup message callback for a XODL state.
 *
 * @param[in] lpState Pointer to the XODL state structure.
 * @param[in] fnMessageHandler Function pointer to the message callback.
 * @param[in] lpUserArg (optional) Pointer to the extra argument needed by the
 * message callback.
 */
void XODLStateSetMessageHandler (
    PXODLState              lpState,
    FXODLMessageHandler     fnMessageHandler,
    void *                  lpUserArg
);

/**
 * @brief Setup parser error handler for a XODL state.
 * 
 * @param[in] lpState Pointer to the XODL state structure.
 * @param[in] fnErrorHandler Function pointer to the parser error handler.
 * @param[in] lpUserArg (optional) Pointer to the extra argument needed by the 
 * error handler.
 */
void XODLStateSetParseErrorHandler (
    PXODLState              lpState,
    FXODLParseErrorHandler  fnErrorHandler,
    void *                  lpUserArg
);

/**
 * @brief Parse a XODL/XODB document.
 *
 * This firstly invokes the \b fnLoadFile() to load the content of the requested file,
 * then pass it to the internal parse routines where a managed XODLDocument structure
 * will be created, and finally \b fnUnloadFile() is called if it's given in
 * \b XODLStateCreate().
 *
 * If no errors occurred during the parsing process, the resulting XODLDocument structure
 * would be cached under the name of either \a szFilePath or \a szAlias if it's known.
 *
 * Messages may be generated if \a lpState has \a fnMessageHandler and/or \a fnErrorHandler
 * set by \b XODLStateSetMessageHandler() and \b XODLStateSetParseErrorHandler() ,
 * respectively.
 *
 * By default dependencies are not handled, but this behaviour can be changed by setting
 * \a flags:
 * \arg \c XODL_LOAD_DEPS - Load the dependencies
 *
 * @param[in] lpState Pointer to the XODL state structure.
 * @param[in] szFilePath Path of the requested file (must be null-terminated).
 * @param[in] szAlias (optional) Alias name of that file (must be null-terminated).
 * @param[in] flags Parser flags.
 * @return Pointer to the XODL document structure.
 * @return NULL on failure.
 */
 #define XODL_LOAD_DEPS             0x1
PXODLDocument XODLStateParseDocument (
    PXODLState              lpState,
    const char *            szFilePath,
    const char *            szAlias,
    int                     flags
);

/**
 * @brief Create a XODL document from the given XODL state.
 *
 * @param[in] lpState Pointer to the XODL state structure.
 * @param[in] szDocumentPath Path/Identifier of this document.
 * @param[in] eType Type of the document.
 * @return Pointer to the newly allocated document structure upon successful completion.
 * @return NULL upon bad argument combination or the document already exists.
 */
PXODLDocument XODLStateCreateDocument (
    PXODLState              lpState,
    const char *            szDocumentPath,
    EXODLDocumentType       eType
);

/**
 * @brief Validate a XODL document with the given XODL Schema.
 *
 * This routine checks if the structure of the \a lpXODL is aligned with the
 * given XODL schema at \a szSchemaPath.
 *
 * If \a szSchemaPath is \c NULL, this routine tries to fetch the path from
 * property \a "XODLSchema". If schema file path doesn't exist in both parameter
 * and the XODL document, no validation would be performed.
 *
 * The given XODL document has to be parsed with the flag \c XODL_LOAD_DEPS, otherwise
 * false is returned.
 *
 * @param[in] lpState Pointer to the XODL state structure.
 * @param[in] lpXODL Pointer to the XODL document structure that will be validated.
 * @param[in] szSchemaPath (optional) Path to the schema document (null-terminated).
 * @return true if the document passed the validation.
 * @return false if the document failed the validation.
 */
bool XODLStateValidateDocument (
    PXODLState              lpState,
    PXODLDocument           lpXODL,
    const char *            szSchemaPath
);

/*------------------------------------------------------------------------------
  XODL Document Operations
------------------------------------------------------------------------------*/

/**
 * @brief Load the dependencies of a XODL document.
 * 
 * @param[in] lpXODL Pointer to the XODL document structure.
 * @return true upon successful completion.
 * @return false if one or more dependencies failed to load, or the document is
 * a XODL Schema.
 */
bool XODLDocumentLoadDependencies(PXODLDocument lpXODL);

/**
 * @brief Release all resources held by a document and itself.
 *
 * \note This routine does not take effect on documents that are managed or created
 * by XODL state.
 *
 * @param[in] lpXODL Pointer to the XODL document structure.
 */
void XODLDocumentDestroy (
    PXODLDocument           lpXODL
);

/**
 * @brief Get the root object from a XODL document structure.
 * 
 * @param lpXODL Pointer to the XODL document structure.
 * @return Pointer to the root object structure (XODLObject) of \a lpXODL.
 */
PXODLObject XODLGetRootObject (
    PXODLDocument           lpXODL
);

/**
 * @brief Query the global object ID table of a XODL document with object id.
 *
 * This routine takes \a OID (Object ID) as key to query its corresponding object
 * in global object table of the given document and return the pointer of that if
 * the object with this OID exists.
 *
 * @param[in] lpXODL Pointer to the XODL document structure where to find the object.
 * @param[in] OID Pointer to the string that holds a valid object ID sequence.
 * @return Pointer to the object structure (XODLObject) upon successful completion.
 * @return \c NULL if the document is schema, object does not exist or the object ID
 *          is invalid.
 */
PXODLObject XODLGetObjectByOID (
    PXODLDocument           lpXODL,
    XODLObjectId            OID
);

/**
 * @brief Resolve an object path.
 *
 * An object path is a string begins with double backslash (\\), following by one or more
 * components separated by forward slash (/). Each component is an identifier of an object
 * or a property. The first component can be an object ID, this is useful to reference an object
 * or a property from another document; there is no restriction on the last component but
 * all non-last components have to be of object. Extra slash at the end of the path is ignored.
 *
 * An object path could point to another object path, this is called "redirection". This
 * routine returns \c NULL if there are too many redirections or circular redirection is
 * detected (i.e., two paths points to each other directly/indirectly).
 *
 * @param[in] lpXODL Pointer to the XODL document structure.
 * @param[in] szObjectPath The object path (null-terminated).
 * @param[in] lpeMemberKind Pointer to an integer used to save the type of the result.
 * @return Pointer to the corresponding object member (either XODLObject or XODLProperty).
 * @return \c NULL if the path does not exist or one of the components is invalid or \a lpXODL
 *         is a schema document.
 */
void *XODLResolveObjectPath (
    PXODLDocument           lpXODL,
    const char *            szObjectPath,
    EXODLMemberKind *       lpeMemberKind
);

/**
 * @brief Get the type of a XODL document.
 *
 * @param[in] lpXODL Pointer to the XODL document structure.
 * @return \c XODL_TYPE_Document if the document is a XODL document.
 * @return \c XODL_TYPE_Schema if the document is a XODL schema.
 */
EXODLDocumentType XODLDocumentGetType (
    PXODLDocument           lpXODL
);

/**
 * @brief Export a XODL document to the file stream.
 *
 * @param[in] lpXODL Pointer to the XODL document structure that to be exported.
 * @param[in] lpExportSettings (optional) Pointer to the export settings structure.
 * @param[in] lpStream Pointer to the POSIX FILE I/O structure.
 * @return true if the operation succeeds.
 * @return false  File I/O error occurred.
 */
bool XODLDocumentExport (
    PXODLDocument           lpXODL,
    PXODLExportSettings     lpExportSettings,
    FILE *                  lpStream
);

/*------------------------------------------------------------------------------
  XODL Object Operations
------------------------------------------------------------------------------*/

/**
 * @brief Create a XODL object structure for a XODL document.
 *
 * There are three types of object in XODL :
 *   - Named Object: Object with an identifier and optionally an object ID.
 *   - Unnamed Object: Object without an identifier.
 *   - Anonymous Object: Object without identifier and object ID.
 *
 * This routine creates schema object if \a flags is given:
 *   - XODL_SCHEMA_OPTIONAL: Mark this schema object as optional.
 *   - XODL_IGNORE_INHERITANCE: Do not resolve inheritance on creation.
 *
 * @param[in] lpXODL Pointer to the XODL document where should the object belongs to.
 * @param[in] lpIdent (optional) The Identifier of the object.
 * @param[in] nIdentSize (if \a szIdent is given) length of the identifier.
 * @param[in] lpInheritPath (optional) Valid object path to which this object inherits from.
 * @param[in] wPathLen (optional) Length of the inherit path.
 * @param[in] OID (optional) Pointer to the string that holds a valid object ID sequence.
 * @param[in] flags (optional) Extra flags used to change the type of object being created.
 * @return Pointer to the newly allocated XODL object structure.
 * @return \c NULL upon bad parameter combination.
 */
PXODLObject XODLObjectCreate (
    PXODLDocument           lpXODL,
    const char *            lpIdent,
    uint8_t                 nIdentSize,
    const char *            lpInheritPath,
    uint16_t                wPathLen,
    XODLObjectId            OID,
#define XODL_SCHEMA_OPTIONAL          1
#define XODL_IGNORE_INHERITANCE       2
    int                     flags
);

/**
 * @brief Fetch identifier of an object.
 * 
 * @param[in] lpObject Pointer to the XODL object structure.
 * @return Identifier of the object.
 * @return \c NULL if the object has no identifier.
 */
const char *XODLObjectGetIdent (
    PXODLObject             lpObject
);

/**
 * @brief Fetch Object ID of an object.
 *
 * @param[in] lpObject Pointer to the object structure.
 * @return String that contains the object ID of the object.
 * @return NULL if the object has no object ID.
 */
const char *XODLObjectGetOID (
    PXODLObject             lpObject
);

/**
 * @brief Get the member count of an object.
 *
 * @param[in] lpObject Pointer to the object structure.
 * @return Count of members of this object.
 */
uint32_t XODLObjectGetMemberCount (
    PXODLObject             lpObject
);

/**
 * @brief Add an object to an object.
 *
 * @param[in] lpObject Pointer to the object structure (parent).
 * @param[in] lpChild Pointer to the object structure (child).
 * @return true upon successful completion.
 * @return false if object type does not match.
 */
bool XODLObjectAddObject (
    PXODLObject             lpObject,
    PXODLObject             lpChild
);

/**
 * @brief Add a property to an object.
 *
 * This routine adds schema property to the given object if \a flags is set:
 *   - XODL_SCHEMA_OPTIONAL: This property should be optional.
 *
 * If one of the following conditions satisfied, this routine returns \c false:
 *   - The property already exists in the object.
 *   - Property value type does not respect \a flags.
 *   - Malformed parameters.
 *
 * @param[in] lpObject Pointer to the XODL object structure.
 * @param[in] lpPropName Pointer to the start of property name.
 * @param[in] nNameLength Length of the property name.
 * @param[in] lpPropValue Pointer to the XODL value structure.
 * @param[in] flags Extra flags for this operation (see description).
 * @return true upon successful completion.
 * @return false otherwise (see description).
 */
bool XODLObjectAddProperty (
    PXODLObject             lpObject,
    const char *            lpPropName,
    uint8_t                 nNameLength,
    PXODLValue              lpPropValue,
    int                     flags
);

/**
 * @brief Delete a member (child object or property) of an object.
 *
 * @param[in] lpObject Pointer to the XODL object structure.
 * @param[in] lpIdent Identifier of the member.
 * @param[in] nLength Length of the identifier.
 * @return true upon successful completion.
 * @return false if requested member does not exist.
 */
bool XODLObjectDeleteMember (
    PXODLObject             lpObject,
    const char *            lpIdent,
    uint8_t                 nLength
);

/**
 * @brief Get a member (child object or property) of an object.
 *
 * @param[in] lpObject Pointer to the XODL object structure.
 * @param[in] lpIdent Identifier of the member.
 * @param[in] nLength Length of the identifier.
 * @param[in] lpeMemberKind Pointer to an integer storage used to save the member kind value.
 * @return Pointer to the member structure upon successful completion.
 * @return NULL otherwise.
 */
void *XODLObjectGetMember (
    PXODLObject             lpObject,
    const char *            lpIdent,
    uint8_t                 nLength,
    EXODLMemberKind *       lpeMemberKind
);

/**
 * @brief Get a property value from an object.
 *
 * This routine is a wrapper of \b XODLObjectGetMember().
 * 
 * @param[in] lpObject Pointer to the XODL object structure.
 * @param[in] lpIdent Pointer to the start of identifier.
 * @param[in] nLength Length of the identifier.
 * @return Pointer to the XODL value structure upon successful completion.
 * @return NULL if no property named \a lpIdent found under the object.
 */
PXODLValue XODLObjectGetPropertyValue (
    PXODLObject             lpObject,
    const char *            lpIdent,
    uint8_t                 nLength
);

/**
 * @brief Get a child object from an object.
 *
 * This routine is a wrapper of \b XODLObjectGetMember().
 *
 * @param[in] lpObject Pointer to the XODL object structure.
 * @param[in] lpIdent Pointer to the start of identifier.
 * @param[in] nLength Length of the identifier.
 * @return Pointer to the object structure upon successful completion.
 * @return NULL if no object named \a lpIdent found under the object.
 */
PXODLObject XODLObjectGetChildObject (
    PXODLObject             lpObject,
    const char *            lpIdent,
    uint8_t                 nLength
);

/**
 * @brief Free an object.
 *
 * @param[in] lpObject Pointer to the XODL object structure.
 */
void XODLObjectDestroy(PXODLObject lpObject);

/**
 * @brief Create an object member iterator for an object.
 * 
 * @param[in] lpObject Pointer to the XODL object structure.
 * @param[in] bIterateParentObject Traverse to the parent object as well if applicable.
 * @return PXODLObjectMemberIterator
 */
PXODLObjectMemberIterator XODLObjectMemberIteratorCreate (
    PXODLObject             lpObject,
    bool                    bIterateParentObject
);

/**
 * @brief Iterate to the next object member.
 *
 * @param[in] lpIterator The object member iterator.
 * @param[in] lpeMemberKind Pointer to somewhere used to fetch the member kind value.
 * @return Pointer to the object member upon successful completion.
 * @return NULL if all members are visited.
 */
void *XODLObjectMemberIteratorNext (
    PXODLObjectMemberIterator lpIterator,
    EXODLMemberKind *       lpeMemberKind
);

/**
 * @brief Free an object member iterator.
 *
 * @param[in] lpIterator The object member iterator.
 */
void XODLObjectMemberIteratorDestroy(PXODLObjectMemberIterator lpIterator);

/*------------------------------------------------------------------------------
  XODL Property Operations
------------------------------------------------------------------------------*/

/**
 * @brief Get the identifier of a property.
 *
 * @param[in] lpProp Pointer to the XODL Property structure.
 * @return The identifier of the property.
 * @return NULL if \a lpProp is invalid.
 */
const char *XODLPropertyGetIdentifier(PXODLProperty lpProp);

/**
 * @brief Get the value of a property.
 *
 * @param[in] lpProp Pointer to the XODL Property structure.
 * @param[in] bResolveObjectPath If object path should be resolved.
 * @return Pointer to XODL value structure.
 * @return NULL if \a lpProp is invalid or object path failed to resolve.
 */
PXODLValue XODLPropertyGetValue(
    PXODLProperty           lpProp,
    bool                    bResolveObjectPath
);

/*------------------------------------------------------------------------------
  XODL Value & List Operations
------------------------------------------------------------------------------*/

/**
 * @brief Create a new XODL value (Nil) from a XODL document.
 *
 * @param[in] lpXODL Pointer to the XODL document structure.
 * @param[in] eValueKind Type of the value to be created.
 * @return Pointer to the newly allocated XODL value structure.
 * @return NULL if the value type does not compatible with the document type.
 */
PXODLValue XODLValueCreate (
    PXODLDocument           lpXODL,
    EXODLValueKind          eValueKind
);

/**
 * @brief Free a XODL value.
 *
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 */
void XODLValueDestroy(PXODLValue lpXODLValue);

/**
 * @brief Get the kind of a XODL value.
 * 
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 * @return Value kind (enumeration) of the given XODL value.
 */
EXODLValueKind XODLValueGetKind(PXODLValue lpXODLValue);

/**
 * @brief Set integer value to a XODL value.
 *
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 * @param[in] i64Value The signed integer value.
 * @return true upon successful completion.
 * @return false bad XODL value kind.
 */
bool XODLValueSetInteger (
    PXODLValue              lpXODLValue,
    int64_t                 i64Value
);

/**
 * @brief Set string data to a XODL value.
 * 
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 * @param[in] lpStr String data.
 * @param[in] ulSize Size of the string.
 * @return true upon successful completion.
 * @return false bad XODL value kind.
 */
bool XODLValueSetString (
    PXODLValue              lpXODLValue,
    const char *            lpStr,
    size_t                  ulSize
);

/**
 * @brief Set byte stream to a XODL value.
 * 
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 * @param[in] lpData Pointer to the start of the byte stream.
 * @param[in] ulSize Size of the byte stream.
 * @return true upon successful completion.
 * @return false bad XODL value kind.
 */
bool XODLValueSetData (
    PXODLValue              lpXODLValue,
    void *                  lpData,
    size_t                  ulSize
);

/**
 * @brief Set double floating point value to a XODL value.
 * 
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 * @param[in] dbValue Double floating-point value.
 * @return true upon successful completion.
 * @return false bad XODL value kind.
 */
bool XODLValueSetReal (
    PXODLValue              lpXODLValue,
    double                  dbValue
);

/**
 * @brief Set boolean value to a XODL value.
 * 
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 * @param[in] bValue Boolean value.
 * @return true upon successful completion.
 * @return false bad XODL value kind.
 */
bool XODLValueSetBoolean (
    PXODLValue              lpXODLValue,
    bool                    bValue
);

/**
 * @brief Set object path data to a XODL value.
 * 
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 * @param[in] lpObjPath Object path.
 * @param[in] ulSize Length of the object path.
 * @return true upon successful completion.
 * @return false bad XODL value kind.
 */
bool XODLValueSetObjectPath (
    PXODLValue              lpXODLValue,
    const char *            lpObjPath,
    size_t                  ulSize
);

/**
 * @brief Set object to a XODL value.
 *
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 * @param[in] lpObject Pointer to the object structure.
 * @return true upon successful completion.
 * @return false bad XODL value kind.
 */
bool XODLValueSetObject (
    PXODLValue              lpXODLValue,
    PXODLObject             lpObject
);

/**
 * @brief Set object ID literal to a XODL value.
 *
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 * @param[in] OID Object ID (it cannot be OID_ANON).
 * @return true upon successful completion.
 * @return false bad XODL value kind.
 */
bool XODLValueSetObjectID (
    PXODLValue              lpXODLValue,
    XODLObjectId            OID
);

/**
 * @brief Set regular expression to a XODL value.
 * 
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 * @param[in] lpRegExp Pointer to the start of the regular expression string.
 * @param[in] nLength Length of the string.
 * @return true upon successful completion.
 * @return false bad XODL value kind.
 */
bool XODLValueSetRegularExpression (
    PXODLValue              lpXODLValue,
    const char *            lpRegExp,
    uint8_t                 nLength
);

/**
 * @brief Get integer value from a XODL value.
 * 
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 * @param[out] lpValueOut Where to save the result value.
 * @return true upon successful completion.
 * @return false bad XODL value kind.
 */
bool XODLValueGetInteger (
    PXODLValue              lpXODLValue,
    int64_t *               lpValueOut
);

/**
 * @brief Get string from a XODL value.
 *
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 * @param[out] lppValueOut Where to save the result value.
 * @return true upon successful completion.
 * @return false bad XODL value kind.
 */
bool XODLValueGetString (
    PXODLValue              lpXODLValue,
    char **                 lppValueOut
);

/**
 * @brief Get byte stream data from a XODL value.
 *
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 * @param[out] lppDataPtr Where to save the pointer to the data stream.
 * @param[out] lpdwDataSize Where to save the size of the data stream.
 * @return true upon successful completion.
 * @return false bad XODL value kind.
 */
bool XODLValueGetData (
    PXODLValue              lpXODLValue,
    void **                 lppDataPtr,
    uint32_t *              lpdwDataSize
);

/**
 * @brief Get double floating point value from a XODL value.
 *
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 * @param[out] lpValueOut Where to save the result value.
 * @return true upon successful completion.
 * @return false bad XODL value kind.
 */
bool XODLValueGetReal (
    PXODLValue              lpXODLValue,
    double *                lpValueOut
);

/**
 * @brief Get boolean value from a XODL value.
 *
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 * @param[out] lpValueOut Where to save the result value.
 * @return true upon successful completion.
 * @return false bad XODL value kind.
 */
bool XODLValueGetBoolean (
    PXODLValue              lpXODLValue,
    bool *                  lpValueOut
);

/**
 * @brief Get object path value from a XODL value.
 *
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 * @param[out] lpszObjPath Where to save the pointer to the object path value.
 * @return true upon successful completion.
 * @return false bad XODL value kind.
 */
bool XODLValueGetObjectPath (
    PXODLValue              lpXODLValue,
    char **                 lpszObjPath
);

/**
 * @brief Get object structure from a XODL value.
 * 
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 * @param[out] lppObjectOut Where to save the pointer to the object structure.
 * @return true upon successful completion.
 * @return false bad XODL value kind.
 */
bool XODLValueGetObject (
    PXODLValue              lpXODLValue,
    PXODLObject *           lppObjectOut
);

/**
 * @brief Get object ID literal from a XODL value.
 *
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 * @param[out] lppOidOut Where to save the pointer to the OID literal string.
 * @return true upon successful completion.
 * @return false bad XODL value kind.
 */
bool XODLValueGetObjectID (
    PXODLValue              lpXODLValue,
    XODLObjectId *          lppOidOut
);

/**
 * @brief Get regular expression from a XODL value.
 *
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 * @param[out] lppRegExp Where to save the result value.
 * @return true upon successful completion.
 * @return false bad XODL value kind.
 */
bool XODLValueGetRegularExpression (
    PXODLValue              lpXODLValue,
    char **                 lppRegExp
);

/**
 * @brief Create a value list on a XODL value.
 *
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 * @param[in] dwItemCount Count of items (0 = auto).
 * @return true upon successful completion.
 * @return false bad XODL value kind.
 */
bool XODLValueSetList (
    PXODLValue              lpXODLValue,
    uint32_t                dwItemCount
);

/**
 * @brief Add an item (value) to a value list.
 * 
 * @param[in] lpXODLValue Pointer to the XODL value structure.
 * @param[in] lpItem Pointer to a XODL value structure that to be added.
 * @return true upon successful completion.
 * @return false bad XODL value kind.
 */
bool XODLValueListAddItem (
    PXODLValue              lpXODLValue,
    PXODLValue              lpItem
);

/**
 * @brief Delete an item from a value list.
 *
 * @param[in] lpXODLValue Pointer to the XODL value structure that is a value list.
 * @param[in] dwIndex Index of the item to be removed.
 * @return true upon successful completion.
 * @return false bad XODL value kind.
 */
bool XODLValueListDeleteItem (
    PXODLValue              lpXODLValue,
    uint32_t                dwIndex
);

/**
 * @brief Get an item from a value list.
 *
 * @param[in] lpXODLValue Pointer to the XODL value structure that is a value list.
 * @param[in] dwIndex Index of the item.
 * @return Pointer to the item (XODL value structure).
 */
PXODLValue XODLValueListGetItem (
    PXODLValue              lpXODLValue,
    uint32_t                dwIndex
);

/**
 * @brief Get the count of items of a value list.
 *
 * @param[in] lpXODLValue Pointer to the XODL value structure that is a value list.
 * @return Count of items.
 */
uint32_t XODLValueListGetItemCount(PXODLValue lpXODLValue);

/**
 * @brief Prepare to iterate a valie list.
 *
 * @param[in] lpXODLValue Pointer to the XODL value structure that is a value list.
 * @return true upon successful completion.
 * @return false XODL value is not a value list.
 */
bool XODLValueListIteratorInit(PXODLValue lpXODLValue);

/**
 * @brief Iterate to the next value item.
 *
 * @param[in] lpXODLValue Pointer to the XODL value structure that is a value list.
 * @return Pointer to the item. (XODL value structure).
 * @return NULL if all items have visited.
 */
PXODLValue XODLValueListIteratorNextItem(PXODLValue lpXODLValue);

#ifdef __cplusplus
}
#endif

#endif /* LIBXODL_XODL_H */
