/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#include <stdio.h>
#include <stdint.h>
#include <sys/fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <err.h>
#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <fcntl.h>
#include <unistd.h>

#include "xodl.h"

#include "../src/buffer.h"
#include "../src/cmacros.h"
#include "../src/hashtable.h"

#define DEFAULT_READ_SIZE           1024
#ifndef MAP_ALIGNED
#define MAP_ALIGNED(x)              0
#endif

#define EXPORT_MODE                 0
#define FORMAT_MODE                 1
#define VALIDATE_MODE               2

static const struct option kLongOptions[] = {
    {"binary", no_argument, NULL, 'b'},
    {"tab-size", required_argument, NULL, 't'},
    {"hardtab", no_argument, NULL, 'T'},
    {"help", no_argument, NULL, 'h'},
    {"minify", no_argument, NULL, 'm'},
    {"byte-array", no_argument, NULL, 0},
    {NULL, required_argument, NULL, 'o'},
    {"validate", required_argument, NULL, 'v'},
    {NULL, required_argument, NULL, 'i'},
    {NULL, 0, NULL, 0}
};

static const char *progname;
static const char *kStandardInput = "<stdin>";
static const char *kPathInserted = "INSERTED";
static const char *kDefaultSchema = "\x1\nDEFAULT_SCHEMA\x02";
// Generated from misc/xodl-schema.xodl
static const uint8_t kFallbackSchemaData[] = {
    0x58, 0x4f, 0x44, 0x42, 0x01, 0x0b, 0x02, 0x00, 0x01, 0x81,
    0x0a, 0x58, 0x4f, 0x44, 0x4c, 0x53, 0x63, 0x68, 0x65, 0x6d, 
    0x61, 0x00, 0x03, 0x01, 0x08, 0x58, 0x4f, 0x44, 0x4c, 0x44, 
    0x65, 0x70, 0x73, 0x00, 0x10, 0x80, 0x01, 0x00, 0x00, 0x00, 
    0x01, 0x10, 0x03, 0x00, 0x08, 0x4f, 0x62, 0x6a, 0x65, 0x63, 
    0x74, 0x49, 0x44, 0x00, 0x17, 0x80, 0x03, 0x00, 0x04, 0x50, 
    0x61, 0x74, 0x68, 0x00, 0x15, 0x80, 0x03, 0x01, 0x05, 0x41, 
    0x6c, 0x69, 0x61, 0x73, 0x15, 0x80, 0x02, 0xff, 0x11, 0xff, 
    0x03, 0x01, 0x0a, 0x58, 0x4f, 0x44, 0x4c, 0x53, 0x63, 0x68, 
    0x65, 0x6d, 0x61, 0x00, 0x15, 0x80, 0x03, 0x00, 0x01, 0x2a, 
    0x19, 0x80, 0x01, 0x05, 0x01, 0x2a, 0x02, 0xff, 0x02, 0xff,
    0x00, 0xff
};
static PHashtable lphtSearchPaths = NULL;

static void AddSearchPath(const char *szPath)
{
    size_t len = strlen(szPath);

    if(lphtSearchPaths == NULL)
        lphtSearchPaths = HashtableCreate(NULL, 1, NULL, true);
    if(szPath[len - 1] == '/')
        len--;
    *HashtableInsert(lphtSearchPaths, szPath, len) = (void*)kPathInserted;
}

static char *CompleteFilePath(const char *szFileName, char *lpBuffer, size_t ulBufferSize)
{
    const char *szDir;
    struct stat st;

    HashtablePrepareForIteration(lphtSearchPaths);
    while(HashtableIterate(lphtSearchPaths, &szDir, NULL)) {
        snprintf(lpBuffer, ulBufferSize, "%s/%s", szDir, szFileName);
        if(0 == stat(lpBuffer, &st))
            return lpBuffer;
    }

    return NULL;
}

static void PrintUsage(int exitcode)
{
    FILE* fp = exitcode ? stderr : stdout;

    fprintf(fp, "XODL Document Utility\n"
                "Copyright (C) 2021 Weihao Feng. All rights reserved.\n\n"
                "Synopsis:\n"
                "    %s [options] [INPUT_FILE]\n"
                "    %s -v <schema_file|auto> [-i <search_path> ...] [INPUT_FILE] ...\n\n", progname, progname);
    fputs(      "Options:\n"
                "    -b, --binary             Convert INPUT_FILE into binary format (XODB)\n"
                "    --byte-array             Export XODB as byte array (implies -b)\n"
                "    -h, --help               Print the help information then quit\n"
                "    -i <search_path>         Add a path to the document search path list\n"
                "    -m, --minify             Minify INPUT_FILE by removing all whitespaces\n"
                "    -o <file>                Write output to <file>\n"
                "    -t <N>, --tab-size=<N>   Specify the size of soft tab (whitespace)\n"
                "    -T, --hardtab            Use hard tab (\\t) instead (discards -t)\n"
                "    -v <schema_file|auto>, --validate=<schema_file|auto>\n"
                "                             Validate the INPUT_FILE with schema_file. \n"
                "                             If schema_file is auto, the validator will try\n"
                "                             to fetch schema_file path by reading property\n"
                "                             \'XODLSchema\' and fallback to the built-in schema\n"
                "                             if none of those attempts succeeds.\n\n"
                "Notes:\n"
                "    * Default tab size is 4.\n"
                "    * When no INPUT_FILE specified, default to read from standard input.\n"
                "    * When no output file specified, default to write to standard output.\n"
                "    * Comments from INPUT_FILE are not preserved in the output!\n", fp);
    exit(exitcode);
}

static void ReportParserError(const char *fname, uint32_t line, uint32_t scol, uint32_t ecol, const char *msg, void *unused)
{
    (void)scol;
    (void)unused;
    fprintf(stderr, "%s:%d:%d: %s\n", fname, line, ecol, msg);
}

static void PrintMessage(EMessageLevel eLevel, const char *szFileName, const char *szMsg, void *unused)
{
    const char *lvltext;
    FILE *lpOut = stdout;

    (void)unused;
    switch(eLevel) {
        case MESSAGE_Verbose: lvltext = "(info)"; break;
        case MESSAGE_Warning: lvltext = "(warn)"; break;
        case MESSAGE_Error: lvltext = "(error)"; lpOut = stderr; break;
    }
    fprintf(lpOut, "%s: %s %s\n", szFileName, lvltext, szMsg);
}

static char *ReadStdin(size_t *lpulReadSize)
{
    char buf[DEFAULT_READ_SIZE];
    char *lpContent = NULL;
    ssize_t r;

    *lpulReadSize = 0;

    while((r = read(STDIN_FILENO, buf, DEFAULT_READ_SIZE)) > 0) {
        lpContent = realloc(lpContent, *lpulReadSize + r + 1);
        memmove(lpContent + *lpulReadSize, buf, r);
        *lpulReadSize += r;
    }
    lpContent[*lpulReadSize] = 0;

    return lpContent;
}

static bool LoadFile(const char *szFileName, void **lppContent, size_t *lpFileSize, void **lppFileArgOut, void *unused)
{
    (void)unused;

    if(szFileName == kStandardInput) {
        *lppContent = ReadStdin(lpFileSize);
        if(*lpFileSize == 0) {
            fprintf(stderr, "%s: failed to read data\n", szFileName);
            return false;
        }

    } else if(szFileName == kDefaultSchema) {
        *lppContent = (void*)kFallbackSchemaData;
        *lpFileSize = sizeof(kFallbackSchemaData);

    } else {
        struct stat st;
        size_t fsize;
        char pathbuf[4096];
        int fd;

        if(szFileName[0] != '/') {
            if(CompleteFilePath(szFileName, pathbuf, sizeof(pathbuf)) == NULL) {
                fprintf(stderr, "%s: file not found\n", szFileName);
                return false;
            }
            fd = open(pathbuf, O_RDONLY);
        } else
            fd = open(szFileName, O_RDONLY);

        if(fd < 0) {
            fprintf(stderr, "%s: open() failed: %s\n", szFileName, strerror(errno));
            return false;
        } else if(-1 == fstat(fd, &st)) {
            fprintf(stderr, "%s: stat() failed: %s:\n", szFileName, strerror(errno));
            close(fd);
            return false;
        }

        fsize = st.st_size;
        *lppContent = mmap(NULL, fsize, PROT_READ, MAP_PRIVATE | MAP_ALIGNED(sysconf(_SC_PAGESIZE)), fd, 0);
        if(*lppContent == MAP_FAILED) {
            fprintf(stderr, "%s: failed to map into virtual memory: %s\n", szFileName, strerror(errno));
            close(fd);
            return false;
        }

        *lpFileSize = fsize;
        *lppFileArgOut = (void*)(intptr_t)fd; // embed the file descriptor into the pointer
    }

    return true;
}

static void UnloadFile(const char *szFileName, void *lpContent, size_t ulFileSize, void *lpFileArg, void *unused)
{
    (void)unused;

    if(szFileName != kStandardInput) {
        munmap(lpContent, ulFileSize);
        close((int)(intptr_t)lpFileArg);
    } else {
        free(lpContent);
    }
}

static int DoExportBinaryArray(PXODLState lpState, PXODLExportSettings lpSettings, const char* szInputFile, const char *szOutputFile)
{
    FILE *fpOut = stdout;
    Buffer ByteBuffer;
    FILE *fpStream;
    int j = 0;
    PXODLDocument lpXODL;

    lpXODL = XODLStateParseDocument(lpState, szInputFile, NULL, 0);
    if(lpXODL == NULL)
        return EXIT_FAILURE;

    fpStream = funopen(&ByteBuffer, NULL, BufferWrite, NULL, BufferClose);
    if(unlikely(fpStream == NULL)) {
        fprintf(stderr, "%s: failed to create memory stream", progname);
        return EXIT_FAILURE;
    }

    BufferInit(&ByteBuffer);
    XODLDocumentExport(lpXODL, lpSettings, fpStream);
    fclose(fpStream);

    if(szOutputFile && !(fpOut = fopen(szOutputFile, "w"))) {
        fprintf(stderr, "%s: failed to open output file %s: %s", progname, szOutputFile, strerror(errno));
        BufferFree(&ByteBuffer);
        return EXIT_FAILURE;
    }

    fputs("static const uint8_t xodb_data[] = {\n", fpOut);
    if(lpSettings->bUseHardTab)
        fputc('\t', fpOut);
    else
        fputs("    ", fpOut);
    for(uint32_t i = 0; i < ByteBuffer.i; i++, j++) {
        fprintf(fpOut, "0x%02x", (uint8_t)ByteBuffer.lpData[i]);
        if(i + 1 < ByteBuffer.i)
            fputs(", ", fpOut);
        if(j == 9) {
            j = -1;
            fputc('\n', fpOut);
            if(i < ByteBuffer.i) {
                if(lpSettings->bUseHardTab)
                    fputc('\t', fpOut);
                else
                    fputs("    ", fpOut);
            }
        }
    }
    fputs("\n};\n", fpOut);
    BufferFree(&ByteBuffer);

    if(szOutputFile)
        fclose(fpOut);

    return EXIT_SUCCESS;
}

static int DoFormat(PXODLState lpState, PXODLExportSettings lpSettings, const char* szInputFile, const char *szOutputFile)
{
    FILE *fpOut = stdout;
    PXODLDocument lpXODL;

    if(NULL == (lpXODL = XODLStateParseDocument(lpState, szInputFile, NULL, 0)))
        return EXIT_FAILURE;

    if(szOutputFile && !(fpOut = fopen(szOutputFile, "w"))) {
        fprintf(stderr, "%s: failed to open output file %s: %s", progname, szOutputFile, strerror(errno));
        return EXIT_FAILURE;
    }

    XODLDocumentExport(lpXODL, lpSettings, fpOut);

    if(szOutputFile)
        fclose(fpOut);

    return EXIT_SUCCESS;
}

static int DoValidate(PXODLState lpState, const char *szInputFile, const char *szFallbackSchema)
{
    PXODLDocument lpXODL;

    if(NULL == (lpXODL = XODLStateParseDocument(lpState, szInputFile, NULL, XODL_LOAD_DEPS)))
        return EXIT_FAILURE;

    if(XODLDocumentGetType(lpXODL) == XODL_TYPE_Schema) {
        fprintf(stderr, "%s: validation is not applicable to XODLSchema document\n", szInputFile);
        return EXIT_FAILURE;
    }

    if(XODLStateValidateDocument(lpState, lpXODL, szFallbackSchema)) {
        fprintf(stdout, "%s: validation passed\n", szInputFile);
        return EXIT_SUCCESS;
    } else {
        fprintf(stderr, "%s: validation failed\n", szInputFile);
        return EXIT_FAILURE;
    }
}

int main(int argc, char **argv)
{
    const char *szOutputFile = NULL, *szInputFile = kStandardInput;
    const char *szFallbackSchemaFile = kDefaultSchema;
    XODLExportSettings ExportSettings = {
        .bMinify = false,
        .bUseBinaryForm = false,
        .bUseHardTab = false,
        .nIndentSize = 4
    };
    int ch, exitcode, optmode = FORMAT_MODE;

    /*
     * Parse commandline options
     */
    progname = argv[0];
    while(-1 != (ch = getopt_long(argc, argv, "Tbhi:mo:t:v:", kLongOptions, NULL))) {
        switch(ch) {
            case 'T': ExportSettings.bUseHardTab = true; break;
            case 'b': ExportSettings.bUseBinaryForm = true; break;
            case 'h': PrintUsage(EXIT_SUCCESS);
            case 'i': AddSearchPath(optarg); break;
            case 'm': ExportSettings.bMinify = true; break;
            case 'o': szOutputFile = optarg; break;
            case 't':
                errno = 0;
                ExportSettings.nIndentSize = strtol(optarg, NULL, 10);
                if(errno)
                    errx(EXIT_FAILURE, "bad argument for -t: %s", strerror(errno));
                break;
            case 'v':
                if(strcmp(optarg, "auto"))
                    szFallbackSchemaFile = optarg;
                optmode = VALIDATE_MODE;
                break;
            case 0:
                ExportSettings.bUseBinaryForm = true;
                optmode = EXPORT_MODE;
                break;
            default:
                PrintUsage(EXIT_FAILURE);
        }
    }
    argc -= optind;
    argv += optind;

    if(argc)
        szInputFile = argv[0];

    /*
     * Setup XODL state
     */
    PXODLState lpState = XODLStateCreate(LoadFile, UnloadFile);
    XODLStateSetMessageHandler(lpState, PrintMessage, NULL);
    XODLStateSetParseErrorHandler(lpState, ReportParserError, NULL);

    AddSearchPath(".");
    switch(optmode) {
        case EXPORT_MODE:
            exitcode = DoExportBinaryArray(lpState, &ExportSettings, szInputFile, szOutputFile);
            break;
        case FORMAT_MODE:
            exitcode = DoFormat(lpState, &ExportSettings, szInputFile, szOutputFile);
            break;
        case VALIDATE_MODE:
            exitcode = DoValidate(lpState, szInputFile, szFallbackSchemaFile);
            break;
    }

    /*
     * Final cleanup
     */
    XODLStateDestroySafe(&lpState);
    HashtableDestroy(lphtSearchPaths);

    return exitcode;
}
