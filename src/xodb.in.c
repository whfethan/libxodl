/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#include <assert.h>
#include <stdint.h>
#include <string.h>

#include "xodl.h"
#include "xodb.h"
#include "cmacros.h"
#include "document.h"
#include "byteswap.h"
#include "delegate.h"

#define XODB_MAGIC          0x42444f58
#define WORD_PREFIX         0xface
#define XODB_VERSION        0x0b01
#define XODB_VM_STACK_SIZE  255

#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#   define NEEDS_BSWAP      0
#   define SWAP16(x)        x
#   define SWAP32(x)        x
#   define SWAP64(x)        x
#else
#   define NEEDS_BSWAP      1
#   define SWAP16(x)        bswap_16(x)
#   define SWAP32(x)        bswap_32(x)
#   define SWAP64(x)        bswap_64(x)
#endif

typedef struct {
    uint32_t                dwMagic;
    uint32_t                dwVersionAndType; // TYPE(16) | VER(16)
} XODBHeader, *PXODBHeader;

/*------------------------------------------------------------------------------
  XODB Implementation

  Binary form of XODL is implemented as series of instructions. Each
  instruction comprises OP code, flags and optional payloads, and is aligned
    +--------+-----------+-----------------------
    | OP (8) | FLAGS (8) | PAYLOADS (2n)
    +--------+-----------+-----------------------
    |<--- INSTHDR ------>|<------- DATA ---------
  to 2 bytes. Therefore, the whole parse stage is basically running bytecode
  inside the VM and this allows us to perform many optimisations. The VM makes
  use of so called "computed goto" technique, which provides good performance
  with high portability.

  XODB must be in little endian.
------------------------------------------------------------------------------*/

enum {
    OP_Exit = 0,            // vm_exit
    OP_ObjectEnter = 1,     // auto
    OP_ObjectLeave = 2,     // auto
    OP_Prop = 3,            // auto
    OP_VNil = 4,            // auto
    OP_VStr = 5,            // auto
    OP_VRStr = 6,           // auto
    OP_VData = 7,           // auto
    OP_VInt = 8,            // auto
    OP_VReal = 9,           // auto
    OP_VInf = 10,           // auto
    OP_VNan = 11,           // auto
    OP_VBool = 12,          // auto
    OP_VOID = 14,           // auto
    OP_VOPath = 15,         // auto
    OP_VListEnter = 16,     // auto
    OP_VListLeave = 17,     // auto
    OP_VSInt = 18,          // auto
    OP_VSBool = 19,         // auto
    OP_VSReal = 20,         // auto
    OP_VSStr = 21,          // auto
    OP_VSOID = 22,          // auto
    OP_VSOPath = 23,        // auto
    OP_VSList = 24,         // auto
    OP_VSAny = 25,          // auto
    OP_LoadDeps = 26,       // auto
};
#define INST_ALIGNMENT              2
#define PADBYTE                     0x00
#define JUNK_OPFLAGS                0xff
#define ALIGNED_SIZE(s, a)          (s + (a - 1)) & ~(a - 1)
#define FLAG(n)                     (1 << n)
#define ENCODE_VAT(v, k)            (uint32_t)((k << 16) | v)
#define VAT_GETVER(vat)             (vat & 0xffff)
#define VAT_GETDOCKIND(vat)         ((vat >> 16) & 0xffff)
#define ENCODE_INSTHDR(op, flags)   (uint16_t)(((flags & 0xff) << 8) | op)
#define OPCODE(hdr)                 (hdr & 0xff)
#define OPFLAGS(hdr)                ((hdr >> 8) & 0xff)

#define REPORT_ERROR(...)           DelegatePrintf(lpDelegate, __VA_ARGS__); \
                                    DelegateCallMessageHandler(lpDelegate, szFileName, MESSAGE_Error)

// Flags for OP_Object*
#define OP_OBJECT_HAS_NAME          FLAG(0)
#define OP_OBJECT_HAS_OID           FLAG(1)
#define OP_OBJECT_AS_MEMBER         FLAG(2)
#define OP_OBJECT_AS_VALUE          FLAG(3)
#define OP_OBJECT_AS_ITEM           FLAG(4)
#define OP_OBJECT_OPTIONAL          FLAG(5)
#define OP_OBJECT_INHERITED         FLAG(6)
#define OP_OBJECT_ROOT              FLAG(7)

#define OP_PROP_OPTIONAL            FLAG(0)

// Flags for OP_V*
#define OP_VALUE_PARENT_LIST        FLAG(6)
#define OP_VALUE_PARENT_PROP        FLAG(7)

/* forward prototypes */
static void WriteHeader(FILE *lpStream, EXODLDocumentType eType);
static size_t EmitInstructionHeader(FILE *lpStream, uint8_t op, uint8_t flags);
static size_t EmitU8(FILE *lpStream, uint8_t byte);
static size_t EmitU16(FILE *lpStream, uint16_t word);
static size_t EmitU32(FILE *lpStream, uint32_t dword);
static size_t EmitU64(FILE *lpStream, uint64_t qword);
static size_t EmitBytes8(FILE *lpStream, const void *lpData);
static size_t EmitBytes(FILE *lpStream, const void *lpData, size_t ulSize);
#if NEEDS_BSWAP
static void SwapBytes8(char *lpIn, char *lpOut);
#endif
static void EmitPads(FILE *lpStream, size_t ulWrittenSize);
static void ExportObject(PXODLObject lpObject, FILE *lpStream, uint8_t flags);
static void ExportProperty(PXODLProperty lpProp, FILE *lpStream);
static void ExportValue(PXODLValue lpValue, FILE *lpStream, uint8_t flags);
static void ExportValueString(PXODLValue lpValue, FILE *lpStream, uint8_t flags);
static void ExportValueData(PXODLValue lpValue, FILE *lpStream, uint8_t flags);
static void ExportValueObjectID(PXODLValue lpValue, FILE *lpStream, uint8_t flags);
static void ExportValueObjectPath(PXODLValue lpValue, FILE *lpStream, uint8_t flags);
static void ExportValueList(PXODLValue lpValue, FILE *lpStream, uint8_t flags);
static void ExportSchemaTypeName(PXODLValue lpValue, FILE *lpStream, uint8_t flags);
static PXODLObject Execute(PXODLDocument lpXODL, const uint16_t *lpEntry, bool bLoadDeps);

bool IsXODBHeader(const char *lpContent, size_t ulSize)
{
    if(ulSize > sizeof(XODBHeader)) {
        PXODBHeader lpHeader = (PXODBHeader)lpContent;

        return XODB_MAGIC  == SWAP32(lpHeader->dwMagic);
    }
    return false;
}

PXODLDocument XODBParseDocument (
    PXODLState              lpState,
    const char *            szFileName,
    const uint8_t *         lpContent,
    size_t                  ulSize,
    bool                    bLoadDeps
) {
    PXODBHeader lpHeader = (PXODBHeader)lpContent;
    uint32_t vat = SWAP32(lpHeader->dwVersionAndType);
    EXODLDocumentType eType;
    PXODLUserDelegate lpDelegate = &lpState->Delegate;

    if(VAT_GETVER(vat) != XODB_VERSION) {
        REPORT_ERROR("incompatible XODB version");
        return NULL;
    } else if(ulSize & (INST_ALIGNMENT-1)) {
        REPORT_ERROR("XODB is not properly aligned (%ld)", ulSize);
        return NULL;
    } else if(unlikely((eType = VAT_GETDOCKIND(vat)) > XODL_TYPE_Schema)) {
        REPORT_ERROR("unknown XODL document type");
        return NULL;
    } else {
        uint16_t last_insthdr = *(uint16_t*)(lpContent + ulSize - sizeof(uint16_t));

        if(OPCODE(last_insthdr) != OP_Exit) {
            REPORT_ERROR("XODB is likely corrupted");
            return NULL;
        }
    }

    if((uintptr_t)lpContent & (INST_ALIGNMENT-1)) {
        REPORT_ERROR("parameter is not properly aligned");
        return NULL;
    }

    PXODLDocument lpDocument = XODLDocumentCreateEx(lpState, eType, NULL);
    lpContent += sizeof(XODBHeader);
    lpDocument->szIdent = (void*)szFileName; // This will be overriden after return.
    lpDocument->lpRootObject = Execute(lpDocument, (void*)lpContent, bLoadDeps);
    if(lpDocument->lpRootObject) {
        lpDocument->lpRootObject->bHasBeenAssigned = true;
        return lpDocument;
    }

    XODLDocumentDestroy(lpDocument);
    return NULL;
}

bool XODBExportDocument(PXODLDocument lpXODLDocument, FILE *lpStream)
{
    if(unlikely(lpStream == stdin))
        return false;

    WriteHeader(lpStream, lpXODLDocument->eType);
    ExportObject(lpXODLDocument->lpRootObject, lpStream, OP_OBJECT_ROOT);
    EmitInstructionHeader(lpStream, OP_Exit, JUNK_OPFLAGS);

    return true;
}

static __FORCE_INLINE__ void *__align_ptr(uintptr_t ptr, int alignment)
{
    uintptr_t ap = ALIGNED_SIZE(ptr, alignment);
    return (void*)ap;
}

static PXODLObject Execute(PXODLDocument lpXODL, const uint16_t *lpEntry, bool bLoadDeps)
{
    static void *lpDispatchTable[UINT8_MAX+1] = {
        %%VM_TABLE_CONTENT%%
    };
    PXODLUserDelegate lpDelegate = &lpXODL->lpState->Delegate;
    const char *szFileName = lpXODL->szIdent;
    void *stack[XODB_VM_STACK_SIZE] = { NULL };
    const uint16_t *pc;
    uint8_t *rpc;
    uint16_t insthdr, w0;
    uint8_t flags = 0, b0;
    uint32_t dw0;
    uint64_t qw0;
#if NEEDS_BSWAP
    double fp0;
#endif
    int sp = -1;
    void *p0, *p1, *p2, *p3;
    #define PUSH(ptr)               stack[++sp] = (void*)ptr
    #define POP()                   --sp;
    #define STACK_TOP()             stack[sp]
    #define STACK_BTM()             stack[0]
    #define FETCH_OP_FLAGS()        flags = OPFLAGS(insthdr)
    #define READ8()                 *rpc++
    #define READ16()                SWAP16(*(uint16_t*)rpc); ADVANCE(2)
    #define READ32()                SWAP32(*(uint32_t*)rpc); ADVANCE(4)
    #define READ64()                SWAP64(*(uint64_t*)rpc); ADVANCE(8)
    #define ADVANCE(n)              rpc += n;
    #define DISPATCH0()             pc = lpEntry; \
                                    insthdr = SWAP16(*pc); pc++; \
                                    rpc = (uint8_t*)pc; \
                                    goto *lpDispatchTable[OPCODE(insthdr)]
    #define DISPATCH()              pc = (uint16_t*)__align_ptr((uintptr_t)rpc, INST_ALIGNMENT); \
                                    insthdr = SWAP16(*pc); pc++; \
                                    rpc = (uint8_t*)pc; \
                                    goto *lpDispatchTable[OPCODE(insthdr)]
    #define VALINST_PROLOGUE(vk) \
                                    FETCH_OP_FLAGS(); \
                                    p0 = STACK_TOP(); \
                                    p1 = XODLValueCreate(lpXODL, vk); \
                                    if(flags & OP_VALUE_PARENT_PROP) { \
                                        POP(); \
                                        XODLPropertySetValue(p0, p1); \
                                    } else if(flags & OP_VALUE_PARENT_LIST) { \
                                        XODLValueListAddItem(p0, p1); \
                                    }

    DISPATCH0();

/*------------------------------------------------------------------------------
    OP_ObjectEnter: Create an empty object and push it to the stack
------------------------------------------------------------------------------*/
__OP_ObjectEnter__:
    FETCH_OP_FLAGS();

    b0 = 0, w0 = 0, p0 = NULL, p1 = NULL, p2 = NULL, dw0 = 0;
    if(flags & OP_OBJECT_HAS_NAME) {
        b0 = READ8();                           // ident_length
        p0 = rpc;                               // ident
        ADVANCE(b0);
    }
    if(flags & OP_OBJECT_INHERITED) {
        w0 = READ16();                          // pathlen
        p1 = rpc;                               // inherit_path
        ADVANCE(w0);
    }
    if(flags & OP_OBJECT_HAS_OID) {
        p2 = rpc;                               // oid
        ADVANCE(XODL_OID_LENGTH)
    }
    if(flags & OP_OBJECT_OPTIONAL)
        dw0 |= XODL_SCHEMA_OPTIONAL;
    if(flags & OP_OBJECT_ROOT)
        dw0 |= XODL_INTERNAL_ROOT_OBJECT;
    if(bLoadDeps == false)
        dw0 |= XODL_IGNORE_INHERITANCE;
    p3 = XODLObjectCreate(lpXODL, p0, b0, p1, w0, p2, dw0);
    if(p3 == NULL)
        return NULL;

    if(flags & OP_OBJECT_AS_MEMBER)
        XODLObjectAddObject(STACK_TOP(), p3);
    else if(flags & OP_OBJECT_AS_ITEM) {
        p0 = XODLValueCreate(lpXODL, XODL_VALUE_Object);
        assert(p0 != NULL);
        XODLValueSetObject(p0, p3);
        XODLValueListAddItem(STACK_TOP(), p0);
    } else if(flags & OP_OBJECT_AS_VALUE) {
        p0 = STACK_TOP();                       // property structure
        POP();
        p1 = STACK_TOP();                       // parent object of this property
        p2 = XODLValueCreate(lpXODL, XODL_VALUE_Object);
        XODLValueSetObject(p2, p3);             // object -> value
        XODLPropertySetValue(p0, p2);
    }
    PUSH(p3);
    DISPATCH();

/*------------------------------------------------------------------------------
    OP_ObjectLeave: Pop out the most recent object
------------------------------------------------------------------------------*/
__OP_ObjectLeave__:
    POP();
    DISPATCH();

/*------------------------------------------------------------------------------
    OP_Prop: Set property structure and push it to the stack
------------------------------------------------------------------------------*/
__OP_Prop__:
    FETCH_OP_FLAGS();
    b0 = READ8();                               // length of ident
    p0 = XODLPropertyCreateEx(lpXODL, (char*)rpc, b0, flags & OP_PROP_OPTIONAL);
    XODLObjectAddPropertyEx(STACK_TOP(), p0);
    PUSH(p0);
    ADVANCE(b0);
    DISPATCH();

/*------------------------------------------------------------------------------
    OP_VNil: Set the property value to 'nil' and add the property to the object
------------------------------------------------------------------------------*/
__OP_VNil__:
    VALINST_PROLOGUE(XODL_VALUE_Nil);
    DISPATCH();

/*------------------------------------------------------------------------------
    OP_VStr: Complete a property or add a new item to the list with
             string literal
------------------------------------------------------------------------------*/
__OP_VStr__:
    VALINST_PROLOGUE(XODL_VALUE_String);
    dw0 = READ32();
    XODLValueSetStringEx(p1, (char*)rpc, dw0);
    ADVANCE(dw0);
    DISPATCH();

/*------------------------------------------------------------------------------
    OP_VRStr: Complete a property or add a new item to the list with
             raw string literal
------------------------------------------------------------------------------*/
__OP_VRStr__:
    VALINST_PROLOGUE(XODL_VALUE_RawString);
    dw0 = READ32();
    XODLValueSetStringEx(p1, (char*)rpc, dw0);
    ADVANCE(dw0);
    DISPATCH();

/*------------------------------------------------------------------------------
    OP_VData: Complete a property or add a new item to the list with
             data stream
------------------------------------------------------------------------------*/
__OP_VData__:
    VALINST_PROLOGUE(XODL_VALUE_Data);
    dw0 = READ32();
    XODLValueSetData(p1, (char*)rpc, dw0);
    ADVANCE(dw0);
    DISPATCH();

/*------------------------------------------------------------------------------
    OP_VInt: Complete a property or add a new item to the list with
             integer data (64-bit)
------------------------------------------------------------------------------*/
__OP_VInt__:
    VALINST_PROLOGUE(XODL_VALUE_Integer);
    qw0 = READ64();
    XODLValueSetInteger(p1, qw0);
    DISPATCH();

/*------------------------------------------------------------------------------
    OP_VReal: Complete a property or add a new item to the list with
             double floating point number
------------------------------------------------------------------------------*/
__OP_VReal__:
    VALINST_PROLOGUE(XODL_VALUE_Real);
#if NEEDS_BSWAP
    SwapBytes8((char*)rpc, (void*)&fp0);
    XODLValueSetReal(p1, fp0);
#else
    XODLValueSetReal(p1, *(double*)rpc);
#endif
    ADVANCE(sizeof(double));
    DISPATCH();

__OP_VInf__:
    VALINST_PROLOGUE(XODL_VALUE_Real);
    XODLValueSetReal(p1, XODL_Inf);
    DISPATCH();

__OP_VNan__:
    VALINST_PROLOGUE(XODL_VALUE_Real);
    XODLValueSetReal(p1, XODL_NaN);
    DISPATCH();

__OP_VBool__:
    VALINST_PROLOGUE(XODL_VALUE_Boolean);
    XODLValueSetBoolean(p1, flags & 0x1);
    DISPATCH();

__OP_VOID__:
    VALINST_PROLOGUE(XODL_VALUE_ObjectID);
    XODLValueSetObjectID(p1, (void*)rpc);
    ADVANCE(XODL_OID_LENGTH);
    DISPATCH();

__OP_VOPath__:
    VALINST_PROLOGUE(XODL_VALUE_ObjectPath);
    w0 = READ16();
    XODLValueSetObjectPath(p1, (void*)rpc, w0);
    ADVANCE(w0);
    DISPATCH();

__OP_VListEnter__:
    VALINST_PROLOGUE(XODL_VALUE_List);
    dw0 = READ32();
    XODLValueSetList(p1, dw0);
    PUSH(p1);
    DISPATCH();

__OP_VListLeave__:
    POP();
    DISPATCH();

__OP_VSInt__:
    VALINST_PROLOGUE(XODL_VALUE_TYPE_NAME_INTEGER);
    DISPATCH();

__OP_VSBool__:
    VALINST_PROLOGUE(XODL_VALUE_TYPE_NAME_BOOLEAN);
    DISPATCH();

__OP_VSReal__:
    VALINST_PROLOGUE(XODL_VALUE_TYPE_NAME_REAL);
    DISPATCH();

__OP_VSStr__:
    VALINST_PROLOGUE(XODL_VALUE_TYPE_NAME_STRING);
    DISPATCH();

__OP_VSOID__:
    VALINST_PROLOGUE(XODL_VALUE_TYPE_NAME_OID);
    DISPATCH();

__OP_VSOPath__:
    VALINST_PROLOGUE(XODL_VALUE_TYPE_NAME_OBJPATH);
    DISPATCH();

__OP_VSList__:
    VALINST_PROLOGUE(XODL_VALUE_TYPE_NAME_LIST);
    DISPATCH();

__OP_VSAny__:
    VALINST_PROLOGUE(XODL_VALUE_TYPE_NAME_ANYTYPE);
    DISPATCH();

__OP_LoadDeps__:
    if(bLoadDeps) {
        p0 = XODLObjectGetPropertyValue(STACK_BTM(), "XODLDeps", 8);
        assert(p0 != NULL);
        if(false == XODLDocumentLoadDeps(lpXODL->lpState, lpXODL, p0))
            return NULL;
    }
    DISPATCH();

unknown_op:
    REPORT_ERROR("unknown instruction 0x%02x", pc[-1] & 0xff);
    return NULL;

vm_exit:
    return STACK_BTM();
}

static void ExportObject(PXODLObject lpObject, FILE *lpStream, uint8_t extflags)
{
    uint8_t flags = extflags;
    size_t written;

    if((lpObject->bSchemaObject == false && lpObject->szIdent && lpObject->szIdent[0] != ANON_OBJECT_IDENT_PREFIX) ||
        (lpObject->bSchemaObject == true && lpObject->lpRegExp))
        flags |= OP_OBJECT_HAS_NAME;
    if(lpObject->szInheritObjectPath)
        flags |= OP_OBJECT_INHERITED;
    if(lpObject->szObjectID != OID_ANON)
        flags |= OP_OBJECT_HAS_OID;
    if(lpObject->bSchemaObject && lpObject->bSchemaOptionalObject)
        flags |= OP_OBJECT_OPTIONAL;

    written = EmitInstructionHeader(lpStream, OP_ObjectEnter, flags);

    // Payload format: |namelen(8)|name(n)|inherit_objpath_len(16)|objpath(n)|oid|
    if(flags & (OP_OBJECT_HAS_NAME | OP_OBJECT_ROOT)) {
        if(lpObject->bSchemaObject == false || (flags & OP_OBJECT_ROOT)) {
            assert(strlen(lpObject->szIdent) < XODL_IDENTIFIER_MAXLENGTH);
            written += EmitU8(lpStream, strlen(lpObject->szIdent) & 0xff);
            written += EmitBytes(lpStream, (uint8_t*)lpObject->szIdent, strlen(lpObject->szIdent));
        } else {
            //assert(strlen(lpObject->lpRegExp->szExp) < XODL_IDENTIFIER_MAXLENGTH);
            uint8_t namelen = strlen(lpObject->lpRegExp->szExp) & 0xff;

            written += EmitU8(lpStream, namelen);
            written += EmitBytes(lpStream, lpObject->lpRegExp->szExp, namelen);
        }
    }
    if(flags & OP_OBJECT_INHERITED) {
        uint32_t len = strlen(lpObject->szInheritObjectPath);

        assert(len < __UINT16_MAX__);
        written += EmitU16(lpStream, len);
        written += EmitBytes(lpStream, (uint8_t*)lpObject->szInheritObjectPath, len);
    }
    if(flags & OP_OBJECT_HAS_OID) {
        assert(strlen(lpObject->szObjectID) == XODL_OID_LENGTH);
        written += EmitBytes(lpStream, (uint8_t*)lpObject->szObjectID, XODL_OID_LENGTH);
    }
    EmitPads(lpStream, written);

    PXODLObjectMemberIterator lpIterator = XODLObjectMemberIteratorCreate(lpObject, false);
    void *lpMember;
    EXODLMemberKind eMemberKind;
    while((lpMember = XODLObjectMemberIteratorNext(lpIterator, &eMemberKind)) != NULL) {
        switch(eMemberKind) {
            case XODL_MEMBER_Object:
                ExportObject(lpMember, lpStream, OP_OBJECT_AS_MEMBER);
                break;
            case XODL_MEMBER_Property:
                ExportProperty(lpMember, lpStream);
                if((flags & OP_OBJECT_ROOT) && !strcmp(AS_PROP(lpMember)->szIdent, "XODLDeps"))
                    EmitInstructionHeader(lpStream, OP_LoadDeps, JUNK_OPFLAGS);
                break;
        }
    }
    XODLObjectMemberIteratorDestroy(lpIterator);

    // Emit an OP_ObjectLeave instruction. It has no payload and flags
    EmitInstructionHeader(lpStream, OP_ObjectLeave, JUNK_OPFLAGS);
}

static void ExportProperty(PXODLProperty lpProp, FILE *lpStream)
{
    size_t written;
    uint8_t flags = 0, namelen;

    if(lpProp->bOptional)
        flags |= OP_PROP_OPTIONAL;

    written = EmitInstructionHeader(lpStream, OP_Prop, flags);
    // Payload format: |len(8)|name..|
    if(lpProp->bSchemaProperty == false) {
        namelen = strlen(lpProp->szIdent);
        written += EmitU8(lpStream, namelen);
        written += EmitBytes(lpStream, (uint8_t*)lpProp->szIdent, namelen);
    } else {
        namelen = strlen(lpProp->lpRegExp->szExp);
        written += EmitU8(lpStream, namelen);
        written += EmitBytes(lpStream, (uint8_t*)lpProp->lpRegExp->szExp, namelen);
    }
    EmitPads(lpStream, written);

    ExportValue(lpProp->lpValue, lpStream, OP_VALUE_PARENT_PROP);
}

static void ExportValue(PXODLValue lpValue, FILE *lpStream, uint8_t flags)
{
    switch(lpValue->eKind) {
        case XODL_VALUE_Nil:
            EmitInstructionHeader(lpStream, OP_VNil, flags);
            break;
        case XODL_VALUE_String:                /* FALLTHROUGH */
        case XODL_VALUE_RawString:
            ExportValueString(lpValue, lpStream, flags);
            break;
        case XODL_VALUE_Data:
            ExportValueData(lpValue, lpStream, flags);
            break;
        case XODL_VALUE_Integer:
            EmitInstructionHeader(lpStream, OP_VInt, flags);
            EmitU64(lpStream, lpValue->i64Val);
            break;
        case XODL_VALUE_Real:
            EmitInstructionHeader(lpStream, OP_VReal, flags);
            EmitBytes8(lpStream, &lpValue->dbVal);
            break;
        case XODL_VALUE_Boolean:
            EmitInstructionHeader(lpStream, OP_VBool, lpValue->i64Val | flags);
            break;
        case XODL_VALUE_Object: {
            uint8_t objflags;

            switch(flags) {
                case OP_VALUE_PARENT_LIST: objflags = OP_OBJECT_AS_ITEM; break;
                case OP_VALUE_PARENT_PROP: objflags = OP_OBJECT_AS_VALUE; break;
                default:
                    objflags = OP_OBJECT_AS_MEMBER; break;
            }
            ExportObject(lpValue->lpObject, lpStream, objflags);
        }   break;
        case XODL_VALUE_ObjectID:
            ExportValueObjectID(lpValue, lpStream, flags);
            break;
        case XODL_VALUE_ObjectPath:
            ExportValueObjectPath(lpValue, lpStream, flags);
            break;
        case XODL_VALUE_List:
            ExportValueList(lpValue, lpStream, flags);
            break;
        case XODL_VALUE_TYPE_NAME_INTEGER:     /* FALLTHROUGH */
        case XODL_VALUE_TYPE_NAME_BOOLEAN:     /* FALLTHROUGH */
        case XODL_VALUE_TYPE_NAME_REAL:        /* FALLTHROUGH */
        case XODL_VALUE_TYPE_NAME_STRING:      /* FALLTHROUGH */
        case XODL_VALUE_TYPE_NAME_OID:         /* FALLTHROUGH */
        case XODL_VALUE_TYPE_NAME_OBJPATH:     /* FALLTHROUGH */
        case XODL_VALUE_TYPE_NAME_LIST:        /* FALLTHROUGH */
        case XODL_VALUE_TYPE_NAME_ANYTYPE:
            ExportSchemaTypeName(lpValue, lpStream, flags);
            break;
        default:
            __UNREACHABLE("unhandled value kind");
    }
}

static void ExportValueString(PXODLValue lpValue, FILE *lpStream, uint8_t flags)
{
    size_t written;
    uint32_t dwLength = strlen(lpValue->szVal);
    uint8_t opcode = lpValue->eKind == XODL_VALUE_String ? OP_VStr : OP_VRStr;

    written = EmitInstructionHeader(lpStream, opcode, flags);
    // Payload format: |length(32)|string|
    written += EmitU32(lpStream, dwLength);
    written += EmitBytes(lpStream, lpValue->szVal, dwLength);
    EmitPads(lpStream, written);
}

static void ExportValueData(PXODLValue lpValue, FILE *lpStream, uint8_t flags)
{
    size_t written;

    written = EmitInstructionHeader(lpStream, OP_VData, flags);
    // Payload format: |length(32)|data|
    written += EmitU32(lpStream, lpValue->lpRawData->dwSize);
    written += EmitBytes(lpStream, lpValue->lpRawData->lpData, lpValue->lpRawData->dwSize);
    EmitPads(lpStream, written);
}

static void ExportValueObjectID(PXODLValue lpValue, FILE *lpStream, uint8_t flags)
{
    size_t written;

    written = EmitInstructionHeader(lpStream, OP_VOID, flags);
    // Payload format: |OID|
    //written += EmitU8(lpStream, XODL_OID_LENGTH);
    written += EmitBytes(lpStream, lpValue->szVal, XODL_OID_LENGTH);
    EmitPads(lpStream, written);
}

static void ExportValueObjectPath(PXODLValue lpValue, FILE *lpStream, uint8_t flags)
{
    size_t written;
    uint16_t pathlen = strlen(lpValue->szVal);

    written = EmitInstructionHeader(lpStream, OP_VOPath, flags);
    // Payload format: |length(16)|path|
    written += EmitU16(lpStream, pathlen);
    written += EmitBytes(lpStream, lpValue->szVal, pathlen);
    EmitPads(lpStream, written);
}

static void ExportSchemaTypeName(PXODLValue lpValue, FILE *lpStream, uint8_t flags)
{
    uint8_t opcode;

    switch(lpValue->eKind) {
        case XODL_VALUE_TYPE_NAME_INTEGER: opcode = OP_VSInt; break;
        case XODL_VALUE_TYPE_NAME_BOOLEAN: opcode = OP_VSBool; break;
        case XODL_VALUE_TYPE_NAME_REAL:    opcode = OP_VSReal; break;
        case XODL_VALUE_TYPE_NAME_STRING:  opcode = OP_VSStr; break;
        case XODL_VALUE_TYPE_NAME_OID:     opcode = OP_VSOID; break;
        case XODL_VALUE_TYPE_NAME_OBJPATH: opcode = OP_VSOPath; break;
        case XODL_VALUE_TYPE_NAME_LIST:    opcode = OP_VSList; break;
        case XODL_VALUE_TYPE_NAME_ANYTYPE: opcode = OP_VSAny; break;
        default:
            __UNREACHABLE("bug: unhandled schema type");
    }
    EmitInstructionHeader(lpStream, opcode, flags);
}

static void ExportValueList(PXODLValue lpValue, FILE *lpStream, uint8_t flags)
{
    EmitInstructionHeader(lpStream, OP_VListEnter, flags);
    // Payload format: |itemcount(32)|
    EmitU32(lpStream, XODLValueListGetItemCount(lpValue));

    XODLValueListIteratorInit(lpValue);
    for(PXODLValue lpItem = XODLValueListIteratorNextItem(lpValue); lpItem;
        lpItem = XODLValueListIteratorNextItem(lpValue)) {
            ExportValue(lpItem, lpStream, OP_VALUE_PARENT_LIST);
    }

    EmitInstructionHeader(lpStream, OP_VListLeave, JUNK_OPFLAGS);
}

static size_t EmitInstructionHeader(FILE *lpStream, uint8_t op, uint8_t flags)
{
    uint16_t insthdr = SWAP16(ENCODE_INSTHDR(op, flags));

    return fwrite(&insthdr, 1, sizeof(insthdr), lpStream);
}

static __FORCE_INLINE__ size_t EmitU8(FILE *lpStream, uint8_t byte)
{
    return fwrite(&byte, 1, sizeof(uint8_t), lpStream);
}

static __FORCE_INLINE__ size_t EmitBytes(FILE *lpStream, const void *lpData, size_t ulSize)
{
    return fwrite(lpData, 1, ulSize, lpStream);
}

static __FORCE_INLINE__ size_t EmitU16(FILE *lpStream, uint16_t word)
{
    word = SWAP16(word);
    return fwrite(&word, 1, sizeof(uint16_t), lpStream);
}

static __FORCE_INLINE__ size_t EmitU32(FILE *lpStream, uint32_t dword)
{
    dword = SWAP64(dword);
    return fwrite(&dword, 1, sizeof(uint32_t), lpStream);
}

static __FORCE_INLINE__ size_t EmitU64(FILE *lpStream, uint64_t qword)
{
    qword = SWAP64(qword);
    return fwrite(&qword, 1, sizeof(uint64_t), lpStream);
}

static __FORCE_INLINE__ size_t EmitBytes8(FILE *lpStream, const void *lpData)
{
#if NEEDS_BSWAP
    const uint8_t *p = (uint8_t*)lpData + 8;

    fputc(*--p, lpStream);
    fputc(*--p, lpStream);
    fputc(*--p, lpStream);
    fputc(*--p, lpStream);
    fputc(*--p, lpStream);
    fputc(*--p, lpStream);
    fputc(*--p, lpStream);
    fputc(*--p, lpStream);

    return 8;
#else
    return fwrite(lpData, 1, 8, lpStream);
#endif
}

#if NEEDS_BSWAP
static __FORCE_INLINE__ void SwapBytes8(char *lpIn, char *lpOut)
{
    for(int i = 0; i < 8; i++)
        lpOut[i] = lpIn[8 - i - 1];
}
#endif

static __FORCE_INLINE__ void EmitPads(FILE *lpStream, size_t ulWrittenSize)
{
    size_t ulAlignedSize = ALIGNED_SIZE(ulWrittenSize, INST_ALIGNMENT);
    int n = ulAlignedSize - ulWrittenSize;

    while(n--)
        EmitU8(lpStream, PADBYTE);
}

static void WriteHeader(FILE *lpStream, EXODLDocumentType eType)
{
    XODBHeader Hdr;

    Hdr.dwMagic = SWAP32(XODB_MAGIC);
    Hdr.dwVersionAndType = SWAP32(ENCODE_VAT(XODB_VERSION, eType));
    fwrite(&Hdr, 1, sizeof(Hdr), lpStream);
}
