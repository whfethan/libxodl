/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#include <stdio.h>
#include <stdarg.h>

#include "cmacros.h"
#include "delegate.h"
#include "document.h"
#include "buffer.h"

#define HasMessageBuffer(d)         (d->MessageBuffer.dwAllocated)

void DelegateInit (PXODLUserDelegate lpDelegate)
{
    lpDelegate->fnMessageHandler = NULL;
    lpDelegate->lpUserMessageHandlerArgument = NULL;
    lpDelegate->fnParseErrorHandler = NULL;
    lpDelegate->lpUserParseErrorHandlerArgument = NULL;
    lpDelegate->MessageBuffer.dwAllocated = 0;
}

void DelegateSetMessageHandler (
    PXODLUserDelegate       lpDelegate,
    FXODLMessageHandler     fnMessageHandler,
    void *                  lpUserArg
) {
    lpDelegate->fnMessageHandler = fnMessageHandler;
    lpDelegate->lpUserMessageHandlerArgument = lpUserArg;
    if(fnMessageHandler && lpDelegate->MessageBuffer.dwAllocated == 0)
        BufferInit(&lpDelegate->MessageBuffer);
}

void DelegateSetParseErrorHandler (
    PXODLUserDelegate       lpDelegate,
    FXODLParseErrorHandler  fnParseErrorHandler,
    void *                  lpUserArg
) {
    lpDelegate->fnParseErrorHandler = fnParseErrorHandler;
    lpDelegate->lpUserParseErrorHandlerArgument = lpUserArg;
    if(fnParseErrorHandler && lpDelegate->MessageBuffer.dwAllocated == 0)
        BufferInit(&lpDelegate->MessageBuffer);
}

void DelegateFree (PXODLUserDelegate lpDelegate)
{
    if(HasMessageBuffer(lpDelegate))
        BufferFree(&lpDelegate->MessageBuffer);
}

void DelegateClearMessage (PXODLUserDelegate lpDelegate)
{
    if(HasMessageBuffer(lpDelegate))
        BufferClear(&lpDelegate->MessageBuffer);
}

void DelegatePrintf (PXODLUserDelegate lpDelegate, const char *szFormatString, ...)
{
    FILE *lpStream;
    va_list ap;

    if(HasMessageBuffer(lpDelegate)) {
        lpStream = funopen(&lpDelegate->MessageBuffer, NULL, BufferWrite, NULL, BufferClose);
        va_start(ap, szFormatString);
        vfprintf(lpStream, szFormatString, ap);
        va_end(ap);
        fclose(lpStream);
    }
}

__FORCE_INLINE__ void DelegateCallParseErrorHandler (
    PXODLUserDelegate       lpDelegate,
    const char *            szFileName,
    uint32_t                dwLine,
    uint32_t                dwCol,
    uint32_t                dwErrCol
) {
    if(lpDelegate->fnParseErrorHandler) {
        lpDelegate->fnParseErrorHandler(szFileName, dwLine, dwCol, dwErrCol,
            lpDelegate->MessageBuffer.lpData, lpDelegate->lpUserParseErrorHandlerArgument);
        BufferClear(&lpDelegate->MessageBuffer);
    }
}

__FORCE_INLINE__ void DelegateCallMessageHandler (
    PXODLUserDelegate       lpDelegate,
    const char *            szFileName,
    EMessageLevel           eMessageLevel
) {
    if(lpDelegate->fnMessageHandler) {
        lpDelegate->fnMessageHandler(eMessageLevel, szFileName,
            lpDelegate->MessageBuffer.lpData, lpDelegate->lpUserMessageHandlerArgument);
        BufferClear(&lpDelegate->MessageBuffer);
    }
}

void DelegateWriteObjectInfo (
    PXODLUserDelegate       lpDelegate,
    PXODLObject             lpObject
) {
    if(lpObject->bSchemaObject == false) {
        if(lpObject->szIdent && lpObject->szIdent[0] != ANON_OBJECT_IDENT_PREFIX)
            DelegatePrintf(lpDelegate, "%s", lpObject->szIdent);
        if(lpObject->szObjectID) {
            if(BufferGetPosition(&lpDelegate->MessageBuffer)) {
                DelegatePrintf(lpDelegate, "(#%s)", lpObject->szObjectID);
            } else
                DelegatePrintf(lpDelegate, "#%s", lpObject->szObjectID);
        }
        if(!BufferGetPosition(&lpDelegate->MessageBuffer)) {
            DelegatePrintf(lpDelegate, "<anonymous object>");
        }
    } else {
        DelegatePrintf(lpDelegate, "\"%s\"", lpObject->szIdent);
    }
}
