/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#ifndef LIBXODL_HASHTABLE_H
#define LIBXODL_HASHTABLE_H

#include <stdbool.h>
#include <stdint.h>

#include "memmgr.h"

typedef struct SHashtableImpl Hashtable, *PHashtable;
typedef void (*FStandardDataDestructor)(void* /*lpData*/);
#define HT_ITEM_NOTFOUND ((void*)-1)
#define HASHTABLE_SHARED_TYPE_DECLARED

PHashtable  HashtableCreate(PMemoryManager lpOptMemMgr, uint32_t dwSize, FStandardDataDestructor fnFree, bool bCloneKeyOnInsert);
void**      HashtableInsert(PHashtable lpHashtable, const char *lpKey, uint32_t dwKeySize);
void*       HashtableQuery(PHashtable lpHashtable, const char *lpKey, uint32_t dwKeySize);
bool        HashtableDelete(PHashtable lpHashtable, const char *lpKey, uint32_t dwKeySize);
bool        HashtableMarkItemAsDeleted(PHashtable lpHashtable, const char *lpKey, uint32_t dwKeySize);
void        HashtableClear(PHashtable lpHashtable);
uint32_t    HashtableGetItemCount(PHashtable lpHashtable);
void        HashtableDestroy(PHashtable lpHashtable);
bool        HashtablePrepareForIteration(PHashtable lpHashtable);
bool        HashtableIterate(PHashtable lpHashtable, const char **lppItemKey, void **lppItemDataPtr);

#endif /* LIBXODL_HASHTABLE_H */
