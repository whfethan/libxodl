/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#include <assert.h>

#include "cmacros.h"
#include "token.h"

const char *TokenKindToString(ETokenKind eTokenKind)
{
    switch(eTokenKind) {
        case TOKEN_EOF:     return "<EOF>";
        case TOKEN_Comma:   return ",";
        case TOKEN_LeftBrace: return "{";
        case TOKEN_RightBrace: return "}";
        case TOKEN_Equal:   return "=";
        case TOKEN_LeftSquare: return "[";
        case TOKEN_RightSquare: return "]";
        case TOKEN_Semicolon: return ";";
        case TOKEN_Colon: return ":";
        case TOKEN_QuestionMark: return "?";
        case TOKEN_Identifier: return "<IDENTIFIER>";
        case TOKEN_ObjectId: return "<OID>";
        case TOKEN_String: return "<STRING>";
        case TOKEN_RawString: return "<RAW STRING>";
        case TOKEN_Base64String: return "<ENCODED BYTE STRING>";
        case TOKEN_Integer: return "<INTEGER(Decimal)>";
        case TOKEN_IntegerB: return "<INTEGER(Binary)>";
        case TOKEN_IntegerO: return "<INTEGER(Octal)>";
        case TOKEN_IntegerH: return "<INTEGER(Hexadecimal)>";
        case TOKEN_Real: return "<REAL>";
        case TOKEN_Boolean: return "<BOOL>";
        case TOKEN_ObjectPath: return "<OBJPATH>";
        case TOKEN_Nil: return "nil";
#if 0
        case TOKEN_Inf: return "inf";
        case TOKEN_Nan: return "nan";
#endif
        case TOKEN_INTEGER: return "INTEGER";
        case TOKEN_BOOLEAN: return "BOOLEAN";
        case TOKEN_REAL: return "REAL";
        case TOKEN_STRING: return "STRING";
        case TOKEN_OID: return "OID";
        case TOKEN_OBJPATH: return "OBJPATH";
        case TOKEN_LIST: return "LIST";
        case TOKEN_ANYTYPE: return "*";
        default:
            __UNREACHABLE("unexpected token kind!!");
    }

    return "(unknown)";
}
