/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#ifndef LIBXODL_CMACROS_H
#define LIBXODL_CMACROS_H

#ifndef __has_builtin
#   define __has_builtin(x)        0
#endif
#ifndef __has_include
#   define __has_include(x)        0
#endif
#ifndef __has_attribute
#   define __has_attribute         0
#endif
#ifndef __has_feature
#   define __has_feature           0
#endif

#ifndef likely
#   if __has_builtin(__builtin_expect)
#       define likely(exp)    __builtin_expect((exp), 1)
#   else
#       define likely(exp)    exp
#   endif
#endif
#ifndef unlikely
#   if __has_builtin(__builtin_expect)
#       define unlikely(exp)  __builtin_expect((exp), 0)
#   else
#       define unlikely(exp)  exp
#   endif
#endif

#ifndef MAX
#   define MAX(a, b) ((a) >= (b) ? (a) : (b))
#endif
#ifndef MIN
#   define MIN(a, b) ((a) <= (b) ? (a) : (b))
#endif

#if __has_builtin(__builtin_unreachable)
#   define __UNREACHABLE(msg) __builtin_unreachable(); assert(1 == 0 && msg)
#else
#   define __UNREACHABLE(msg) assert(1 == 0 && msg)
#endif

#if __has_attribute(__unused__)
#   define __UNUSED      __attribute__((__unused__))
#else
#   define __UNUSED
#endif

#if __has_attribute(always_inline)
#   define __FORCE_INLINE__  __attribute__((always_inline)) inline
#else
#   ifdef _MSC_VER
#       define __FORCE_INLINE__ __forceinline
#   else
#       define __FORCE_INLINE__  inline
#   endif
#endif

#ifndef __offsetof
#   include <stddef.h>
#   define __offsetof(t, m)      offsetof(t, m)
#endif

#include "platform_compat.h"

#endif /* LIBXODL_CMACROS_H */
