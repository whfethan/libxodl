/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#ifndef LIBXODL_DOCUMENT_H
#define LIBXODL_DOCUMENT_H

#include "xodl.h"
#include "memmgr.h"
#include "hashtable.h"
#include "delegate.h"

struct SXODLStateImpl {
    PHashtable          lpCachedDocuments;
    FXODLUserFileLoad   fnLoadFile;
    FXODLUserFileUnload fnUnloadFile;
    void *              lpUserLoadArg;
    XODLUserDelegate    Delegate;
};

// Container header for XODLObject and XODLProperty
#define HEADER_CONTENT          uint32_t dwSignature, reserved0
typedef struct {
    uint32_t                    dwSignature;
    uint32_t                    reserved0;
} InternalMemberHeader, *PInternalMemberHeader;

struct SXODLDocumentImpl {
    PMemoryManager              lpMemMgr;
    PXODLState                  lpState;
    char *                      szIdent;
    PXODLObject                 lpRootObject;
    PHashtable                  lpGlobalOIDs;
    EXODLDocumentType           eType;
    int32_t                     nRefCount : 29;
    int32_t                     bHasDeps : 1;
    int32_t                     bDepsLoaded : 1;
};

typedef struct SRegExp {
    void *                      lpCompiledData;
    char                        szExp[0];
} RegularExpression, *PRegularExpression;

#define XODL_OBJECT_SIGNATURE   0xfafb94ad
struct SXODLObject {
    HEADER_CONTENT;                                 /* reserved0 is used as reference counter */
    PXODLDocument               lpXODL;
    union {
        char *                  szIdent;
        PRegularExpression      lpRegExp;
    };
    char *                      szObjectID;
    PHashtable                  lpChildren;         /* PInternalMemberHeader */
    PXODLObject                 lpParent;
    char *                      szInheritObjectPath;
    PXODLObject                 lpOriginObject;
    bool                        bHasBoundToValue : 1;
    bool                        bHasBeenAssigned : 1;
    bool                        bSchemaObject : 1;
    bool                        bSchemaOptionalObject : 1;
};

#define XODL_PROPERTY_SIGNATURE 0xfb4a8823
struct SXODLProperty {
    HEADER_CONTENT;
    PXODLDocument               lpXODL;
    union {
        char *                  szIdent;
        PRegularExpression      lpRegExp;
    };
    PXODLValue                  lpValue;
    PXODLValue                  lpResolvedValue;
    bool                        bSchemaProperty : 1;
    bool                        bOptional : 1;
};

typedef struct SXODLValueList XODLValueList, *PXODLValueList;
struct SXODLValueList {
    PXODLValue *                lppItems;
    uint32_t                    dwCountOfItems;
    uint32_t                    dwAllocatedSlots;
    uint32_t                    dwIdx;
    uint32_t                    i;
};

struct SRawData {
    uint32_t                    dwSize;
    char                        lpData[0];
};

struct SXODLValue {
    EXODLValueKind              eKind;
    int32_t                     bInitialised : 1;
    int32_t                     nRefCount : 30;
    union {
        char *                  szVal;
        struct SRawData *       lpRawData;
        int64_t                 i64Val;
        double                  dbVal;
        PXODLObject             lpObject;
        PXODLValueList          lpList;
        PRegularExpression      lpRegExp;
    };
};

struct SXODLObjectMemberIterator {
    PXODLObject                 lpCurrentObject;
    PHashtable                  lpObjectMembers;
    bool                        bIterateParentObject;
};

#define DEFAULT_CHILDREN_COUNT      2
#define MEMORY_MANAGER(xodl)        xodl->lpMemMgr
#define ANON_OBJECT_IDENT_PREFIX    '>'
#define AS_OBJECT(v)                ((PXODLObject)(v))
#define AS_PROP(v)                  ((PXODLProperty)(v))
#define AS_GENERIC(v)               ((PInternalMemberHeader)(v))

// Private flag for XODLObjectCreate()
#define XODL_INTERNAL_ROOT_OBJECT   4

PXODLDocument XODLDocumentCreateEx(PXODLState lpState, EXODLDocumentType eDocuType, PXODLObject lpRootObject);
void          XODLValueSetStringEx(PXODLValue lpValue, const char *lpStr, size_t ulSize);
EXODLMemberKind XODLMemberGetKind(void *lpMember);

PRegularExpression XODLRegExpCreate(PMemoryManager lpMemMgr, const char *lpRegExp, uint8_t nLength);
bool XODLRegExpMatch(PRegularExpression lpRegExp, const char *szStr);
void XODLRegExpDestroy(PRegularExpression lpRegExp);

void XODLValueAppendString(PXODLValue lpXODLValue, const char *lpStr, size_t ulSize, bool bRawString);

/* Used by XODB VM */
PXODLProperty XODLPropertyCreateEx(PXODLDocument lpXODL, const char *lpIdent, uint8_t nLength, bool bOptional);
void          XODLPropertySetValue(PXODLProperty lpProp, PXODLValue lpVal);
void          XODLObjectAddPropertyEx(PXODLObject lpObject, PXODLProperty lpProp);

/* From state.c */
bool XODLDocumentLoadDeps(PXODLState lpState, PXODLDocument lpXODL, PXODLValue lpDepList);

#endif /* LIBXODL_DOCUMENT_H */
