/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#ifndef LIBXODL_CHARFACT_H
#define LIBXODL_CHARFACT_H

#include <stdint.h>

typedef enum {
    FACT_ObjectIDChar    = 1,
    FACT_IdentStart      = 1 << 1,
    FACT_IdentContinue   = 1 << 2,
    FACT_Whitespace      = 1 << 3,
    FACT_Newline         = 1 << 4,
    FACT_Base64          = 1 << 5,
    FACT_Digit           = 1 << 6,
    FACT_Hex             = 1 << 7,
    FACT_NonPrintable    = 1 << 8,
    FACT_ValidEscapeCode = 1 << 9,
    FACT_Invalid         = 0
} ECharacterFact;

extern uint16_t gCharFactTab[__UINT8_MAX__];
#define GetCharacterFacts(ch) gCharFactTab[(uint8_t)ch]

#define isxdigit(ch) (GetCharacterFacts(ch) & FACT_Hex)
#define isdigit(ch)  (GetCharacterFacts(ch) & FACT_Digit)

#endif /* LIBXODL_CHARFACT_H */
