/* Copyright 2019 Herman ten Brugge
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

/*-T-T--------------------------------------------------------------------------
  NEEDS:    src/charfact.c
--------------------------------------------------------------------------T-T-*/

#include "convert.h"
#include "charfact.h"
#include "cmacros.h"
#include "range_test.h"

static uint32_t convert_num[128] = {
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 1, 2, 3, 4, 5, 6, 7,
    8, 9, 0, 0, 0, 0, 0, 0,
    0, 10, 11, 12, 13, 14, 15, 16,
    17, 18, 19, 20, 21, 22, 23, 24,
    25, 26, 27, 28, 29, 30, 31, 32,
    33, 34, 35, 0, 0, 0, 0, 0,
    0, 10, 11, 12, 13, 14, 15, 16,
    17, 18, 19, 20, 21, 22, 23, 24,
    25, 26, 27, 28, 29, 30, 31, 32,
    33, 34, 35, 0, 0, 0, 0, 0,
};

static unsigned char uppercase[256] = {
    0, 1, 2, 3, 4, 5, 6, 7,
    8, 9, 10, 11, 12, 13, 14, 15,
    16, 17, 18, 19, 20, 21, 22, 23,
    24, 25, 26, 27, 28, 29, 30, 31,
    32, 33, 34, 35, 36, 37, 38, 39,
    40, 41, 42, 43, 44, 45, 46, 47,
    48, 49, 50, 51, 52, 53, 54, 55,
    56, 57, 58, 59, 60, 61, 62, 63,
    64, 65, 66, 67, 68, 69, 70, 71,
    72, 73, 74, 75, 76, 77, 78, 79,
    80, 81, 82, 83, 84, 85, 86, 87,
    88, 89, 90, 91, 92, 93, 94, 95,
    96, 65, 66, 67, 68, 69, 70, 71,
    72, 73, 74, 75, 76, 77, 78, 79,
    80, 81, 82, 83, 84, 85, 86, 87,
    88, 89, 90, 123, 124, 125, 126, 127,
    128, 129, 130, 131, 132, 133, 134, 135,
    136, 137, 138, 139, 140, 141, 142, 143,
    144, 145, 146, 147, 148, 149, 150, 151,
    152, 153, 154, 155, 156, 157, 158, 159,
    160, 161, 162, 163, 164, 165, 166, 167,
    168, 169, 170, 171, 172, 173, 174, 175,
    176, 177, 178, 179, 180, 181, 182, 183,
    184, 185, 186, 187, 188, 189, 190, 191,
    192, 193, 194, 195, 196, 197, 198, 199,
    200, 201, 202, 203, 204, 205, 206, 207,
    208, 209, 210, 211, 212, 213, 214, 215,
    216, 217, 218, 219, 220, 221, 222, 223,
    224, 225, 226, 227, 228, 229, 230, 231,
    232, 233, 234, 235, 236, 237, 238, 239,
    240, 241, 242, 243, 244, 245, 246, 247,
    248, 249, 250, 251, 252, 253, 254, 255
};

static uint32_t valid_num[256] = {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
    0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

int64_t StringToInt64 (const char *str, char **endptr, int base)
{
    uint64_t n;
    unsigned int sign = 0;
    unsigned char *cp = (unsigned char *)str;
#define M(x) (UINT64_C(9223372036854775807) / (x))
    static const uint64_t maxp[37] = {
        0, 0, M(2), M(3), M(4), M(5), M(6), M(7), M(8), M(9), M(10),
        M(11), M(12), M(13), M(14), M(15), M(16), M(17), M(18),
        M(19), M(20), M(21), M(22), M(23), M(24), M(25), M(26),
        M(27), M(28), M(29), M(30), M(31), M(32), M(33), M(34),
        M(35), M(36)};
#undef M
#define M(x) (UINT64_C(9223372036854775808) / (x))
    static const uint64_t maxn[37] = {
        0, 0, M(2), M(3), M(4), M(5), M(6), M(7), M(8), M(9), M(10),
        M(11), M(12), M(13), M(14), M(15), M(16), M(17), M(18),
        M(19), M(20), M(21), M(22), M(23), M(24), M(25), M(26),
        M(27), M(28), M(29), M(30), M(31), M(32), M(33), M(34),
        M(35), M(36)};
#undef M
#define R(x) (UINT64_C(9223372036854775807) % (x))
    static const uint64_t remp[37] = {
        0, 0, R(2), R(3), R(4), R(5), R(6), R(7), R(8), R(9), R(10),
        R(11), R(12), R(13), R(14), R(15), R(16), R(17), R(18),
        R(19), R(20), R(21), R(22), R(23), R(24), R(25), R(26),
        R(27), R(28), R(29), R(30), R(31), R(32), R(33), R(34),
        R(35), R(36)};
#undef R
#define R(x) (UINT64_C(9223372036854775808) % (x))
    static const uint64_t remn[37] = {
        0, 0, R(2), R(3), R(4), R(5), R(6), R(7), R(8), R(9), R(10),
        R(11), R(12), R(13), R(14), R(15), R(16), R(17), R(18),
        R(19), R(20), R(21), R(22), R(23), R(24), R(25), R(26),
        R(27), R(28), R(29), R(30), R(31), R(32), R(33), R(34),
        R(35), R(36)};
#undef R

#if 0
  while (isspace (*cp)) {
    cp++;
  }
#endif
    if (*cp == '+')
        cp++;
    else if (*cp == '-') {
        sign = 1;
        cp++;
    }

    n = 0;
    if (unlikely(base) &&
        (base != 10 || *cp < '1') &&
        (base != 8 || *cp != '0' || cp[1] < '0' || cp[1] > '7') &&
        (base != 16 || *cp != '0' || (cp[1] != 'x' && cp[1] != 'X'))) {
        if (IsValueInRange(base, 2, 36)) {
            uint64_t max = sign ? maxn[base] : maxp[base];
            uint64_t rem = sign ? remn[base] : remp[base];

            if (base <= 10) {
                unsigned char u = *cp;
                unsigned char end = '0' + base;

                if (u >= '0' && u < end) {
                    do {
                        uint32_t v = convert_num[u];

                        if (unlikely(n >= max) && (n > max || (n == max && v > rem)))
                            break;
                        n = n * base + v;
                        u = *++cp;
                    } while (u >= '0' && u < end);
                } else {
                    cp = (unsigned char *)str;
                }
            } else {
                unsigned char u = uppercase[*cp];
                unsigned char end = 'A' + base - 10;

                if (valid_num[u] && u < end) {
                    do {
                        uint32_t v = convert_num[u];

                        if (unlikely(n >= max) && (n > max || (n == max && v > rem)))
                            break;
                        n = n * base + v;
                        u = uppercase[*++cp];
                    } while (valid_num[u] && u < end);
                } else {
                    cp = (unsigned char *)str;
                }
            }
        } else {
            cp = (unsigned char *)str;
        }

    } else if (*cp == '0') {
        cp++;
        if ((*cp == 'x' || *cp == 'X') && isxdigit(cp[1])) {
            uint64_t max = sign ? maxn[16] : maxp[16];
            uint64_t rem = sign ? remn[16] : remp[16];
            unsigned char u = *++cp;

            do {
                uint32_t v = convert_num[u];

                if (unlikely(n >= max) && (n > max || (n == max && v > rem)))
                    break;
                n = n * 16 + v;
                u = *++cp;
            } while (isxdigit(u));
        } else {
            uint64_t max = sign ? maxn[8] : maxp[8];
            uint64_t rem = sign ? remn[8] : remp[8];
            unsigned char u = *cp;

            while (IsValueInRange(u, '0', '7')) {
                uint32_t v = convert_num[u];

                if (unlikely(n >= max) && (n > max || (n == max && v > rem)))
                    break;
                n = n * 8 + v;
                u = *++cp;
            }
        }

    } else if (isdigit(*cp)) {
        uint64_t max = sign ? maxn[10] : maxp[10];
        uint64_t rem = sign ? remn[10] : remp[10];
        unsigned char u = *cp;

        do {
            uint32_t v = convert_num[u];

            if (unlikely(n >= max) && (n > max || (n == max && v > rem)))
                break;
            n = n * 10 + v;
            u = *++cp;
        } while (isdigit(u));

    } else {
        cp = (unsigned char *)str;
    }

    if (endptr)
        *endptr = (char *)cp;

    return sign ? -(int64_t)n : (int64_t)n;
}
