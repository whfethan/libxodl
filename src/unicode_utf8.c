/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/
/*
    Copyright (c) 2017 CK Tan
    https://github.com/cktan/tomlc99

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include "unicode_utf8.h"
#include "range_test.h"
#include "cmacros.h"

int ConvertUCSToUTF8(uint32_t dwCodePoint, char *lpOutBuffer)
{
    /* http://stackoverflow.com/questions/6240055/manually-converting-unicode-codepoints-into-utf-8-and-utf-16 */
    // UTF-16 surrogates and UCS noncharacters shouldn't appear right here
    if(unlikely(IsValueInRange(dwCodePoint, 0xd800, 0xdfff) == true))
        return -1;
    else if(unlikely(IsValueInRange(dwCodePoint, 0xfffe, 0xffff) == true))
        return -1;

    if(dwCodePoint <= 0x7f) {
        // 0x00000000 - 0x0000007F: 0xxxxxxx
        lpOutBuffer[0] = (uint8_t)dwCodePoint;
        return 1;

    } else if(dwCodePoint <= 0x7ff) {
        // 0x00000080 - 0x000007FF: 110xxxxx 10xxxxxx
        lpOutBuffer[0] = 0xc0 | (dwCodePoint >> 6);
        lpOutBuffer[1] = 0x80 | (dwCodePoint & 0x3f);
        return 2;

    } else if(dwCodePoint <= 0xffff) {
        // 0x00000800 - 0x0000FFFF: 1110xxxx 10xxxxxx 10xxxxxx
        lpOutBuffer[0] = 0xe0 | (dwCodePoint >> 12);
        lpOutBuffer[1] = 0x80 | ((dwCodePoint >> 6) & 0x3f);
        lpOutBuffer[2] = 0x80 | (dwCodePoint & 0x3f);
        return 3;

    } else if(dwCodePoint <= 0x1fffff) {
        // 0x00010000 - 0x001FFFFF: 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
        lpOutBuffer[0] = 0xf0 | (dwCodePoint >> 18);
        lpOutBuffer[1] = 0x80 | ((dwCodePoint >> 12) & 0x3f);
        lpOutBuffer[2] = 0x80 | ((dwCodePoint >> 6) & 0x3f);
        lpOutBuffer[3] = 0x80 | (dwCodePoint & 0x3f);
        return 4;

    } else if(dwCodePoint <= 0x3ffffff) {
        // 0x00200000 - 0x03FFFFFF: 111110xx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx
        lpOutBuffer[0] = 0xf8 | (dwCodePoint >> 24);
        lpOutBuffer[1] = 0x80 | ((dwCodePoint >> 18) & 0x3f);
        lpOutBuffer[2] = 0x80 | ((dwCodePoint >> 12) & 0x3f);
        lpOutBuffer[3] = 0x80 | ((dwCodePoint >> 6) & 0x3f);
        lpOutBuffer[4] = 0x80 | (dwCodePoint & 0x3f);
        return 5;

    } else if(dwCodePoint <= 0x7fffffff) {
        // 0x04000000 - 0x7FFFFFFF: 1111110x 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx
        lpOutBuffer[0] = 0xfc | (dwCodePoint >> 30);
        lpOutBuffer[1] = 0x80 | ((dwCodePoint >> 24) & 0x3f);
        lpOutBuffer[2] = 0x80 | ((dwCodePoint >> 18) & 0x3f);
        lpOutBuffer[3] = 0x80 | ((dwCodePoint >> 12) & 0x3f);
        lpOutBuffer[4] = 0x80 | ((dwCodePoint >> 6) & 0x3f);
        lpOutBuffer[5] = 0x80 | (dwCodePoint & 0x3f);
        return 6;
    }

    return false;
}

uint8_t ConvertUTF8ToUCS(const uint8_t *lpData, size_t ulSize, uint32_t *lpdwCodePoint)
{
    const uint8_t *p = lpData;
    uint8_t i = *p++;
    uint32_t dwCodePoint;
#define UPDATE_CODEPOINT()      i = *p++; \
                                if (0x2 != (i >> 6)) return 0; \
                                dwCodePoint = (dwCodePoint << 6) | (i & 0x3f)
#define CHECK_SIZE(s)           if(unlikely(ulSize < s)) return 0

    // 0x00000000 - 0x0000007F: 0xxxxxxx
    if (0 == (i >> 7)) {
        CHECK_SIZE(1);
        dwCodePoint = i;
        *lpdwCodePoint = dwCodePoint;
        return 1;
    }

    // 0x00000080 - 0x000007FF: 110xxxxx 10xxxxxx
    if (0x6 == (i >> 5)) {
        CHECK_SIZE(2);
        dwCodePoint = i & 0x1f;
        UPDATE_CODEPOINT();
        *lpdwCodePoint = dwCodePoint;
        return p - lpData;
    }

    // 0x00000800 - 0x0000FFFF: 1110xxxx 10xxxxxx 10xxxxxx
    if (0xe == (i >> 4)) {
        CHECK_SIZE(3);
        dwCodePoint = i & 0x0F;
        UPDATE_CODEPOINT();
        UPDATE_CODEPOINT();
        *lpdwCodePoint = dwCodePoint;
        return p - lpData;
    }

    // 0x00010000 - 0x001FFFFF: 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
    if (0x1e == (i >> 3)) {
        CHECK_SIZE(4);
        dwCodePoint = i & 0x07;
        UPDATE_CODEPOINT();
        UPDATE_CODEPOINT();
        UPDATE_CODEPOINT();
        *lpdwCodePoint = dwCodePoint;
        return p - lpData;
    }

    // 0x00200000 - 0x03FFFFFF: 111110xx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx
    if (0x3e == (i >> 2)) {
        CHECK_SIZE(5);
        dwCodePoint = i & 0x03;
        UPDATE_CODEPOINT();
        UPDATE_CODEPOINT();
        UPDATE_CODEPOINT();
        UPDATE_CODEPOINT();
        *lpdwCodePoint = dwCodePoint;
        return p - lpData;
    }

    // 0x04000000 - 0x7FFFFFFF: 1111110x 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx
    if (0x7e == (i >> 1)) {
        CHECK_SIZE(6);
        dwCodePoint = i & 0x01;
        UPDATE_CODEPOINT();
        UPDATE_CODEPOINT();
        UPDATE_CODEPOINT();
        UPDATE_CODEPOINT();
        UPDATE_CODEPOINT();
        *lpdwCodePoint = dwCodePoint;
        return p - lpData;
    }

    return 0;
}
