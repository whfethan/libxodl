/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/
/**
 * \file
 * <PRE>
 * MODP_B64 - High performance base64 encoder/decoder
 * Version 1.3 -- 17-Mar-2006
 * http://modp.com/release/base64
 *
 * Copyright &copy; 2005, 2006  Nick Galbreath -- nickg [at] modp [dot] com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 *
 *   Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 *
 *   Neither the name of the modp.com nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This is the standard "new" BSD license:
 * http://www.opensource.org/licenses/bsd-license.php
 * </PRE>
 */

#include <assert.h>
#include <stdio.h>

#include "base64.h"
#include "cmacros.h"

/* if on motoral, sun, ibm; uncomment this */
/* #define WORDS_BIGENDIAN 1 */
/* else for Intel, Amd; uncomment this */
/* #undef WORDS_BIGENDIAN */

#include "modp_b64_data.h"

#define BADCHAR 0x01FFFFFF
/**
 * you can control if we use padding by commenting out this
 * next line.  However, I highly recommend you use padding and not
 * using it should only be for compatability with a 3rd party.
 * Also, 'no padding' is not tested!
 */
#define DOPAD 1

/*
 * if we aren't doing padding
 * set the pad character to NULL
 */
#ifndef DOPAD
#undef CHARPAD
#define CHARPAD '\0'
#endif

void Base64Encode(char *lpOutBuffer, void *pData, uint32_t dwSize)
{
    uint32_t i = 0;
    uint8_t* p = (uint8_t*)lpOutBuffer;
    const char *str = pData;

    /* unsigned here is important! */
    uint8_t t1, t2, t3;
    if (dwSize > 2) {
        for (; i < dwSize - 2; i += 3) {
            t1 = str[i]; t2 = str[i+1]; t3 = str[i+2];
            *p++ = e0[t1];
            *p++ = e1[((t1 & 0x03) << 4) | ((t2 >> 4) & 0x0F)];
            *p++ = e1[((t2 & 0x0F) << 2) | ((t3 >> 6) & 0x03)];
            *p++ = e2[t3];
        }
    }
    switch (dwSize - i) {
    case 0:
        break;
    case 1:
        t1 = str[i];
        *p++ = e0[t1];
        *p++ = e1[(t1 & 0x03) << 4];
        *p++ = CHARPAD;
        *p++ = CHARPAD;
        break;
    default: /* case 2 */
        t1 = str[i]; t2 = str[i+1];
        *p++ = e0[t1];
        *p++ = e1[((t1 & 0x03) << 4) | ((t2 >> 4) & 0x0F)];
        *p++ = e2[(t2 & 0x0F) << 2];
        *p++ = CHARPAD;
    }
    *p = '\0';
#if 0
    return p - (uint8_t*)dest;
#endif
}

void Base64EncodeToStream(FILE *lpStream, void *lpData, uint32_t dwSize)
{
    uint32_t i = 0;
    const char *str = lpData;

    /* unsigned here is important! */
    uint8_t t1, t2, t3;
    if (dwSize > 2) {
        for (; i < dwSize - 2; i += 3) {
            t1 = str[i]; t2 = str[i+1]; t3 = str[i+2];
            fputc(e0[t1], lpStream);
            fputc(e1[((t1 & 0x03) << 4) | ((t2 >> 4) & 0x0F)], lpStream);
            fputc(e1[((t2 & 0x0F) << 2) | ((t3 >> 6) & 0x03)], lpStream);
            fputc(e2[t3], lpStream);
        }
    }
    switch (dwSize - i) {
    case 0:
        break;
    case 1:
        t1 = str[i];
        fputc(e0[t1], lpStream);
        fputc(e1[(t1 & 0x03) << 4], lpStream);
        fputc(CHARPAD, lpStream);
        fputc(CHARPAD, lpStream);
        break;
    default: /* case 2 */
        t1 = str[i]; t2 = str[i+1];
        fputc(e0[t1], lpStream);
        fputc(e1[((t1 & 0x03) << 4) | ((t2 >> 4) & 0x0F)], lpStream);
        fputc(e2[(t2 & 0x0F) << 2], lpStream);
        fputc(CHARPAD, lpStream);
    }
}

#ifdef WORDS_BIGENDIAN   /* BIG ENDIAN -- SUN / IBM / MOTOROLA */
void Base64Decode(char* dest, const char* src, uint32_t len)
{
    if (len == 0) return;
#ifdef DOPAD
    /* if padding is used, then the message must be at least
       4 chars and be a multiple of 4.
       there can be at most 2 pad chars at the end */
#if 0
    if (len < 4 || (len % 4 != 0)) return MODP_B64_ERROR;
#endif
    if (src[len-1] == CHARPAD) {
        len--;
        if (src[len -1] == CHARPAD) {
            len--;
        }
    }
#endif  /* DOPAD */
    uint32_t i;
    int leftover = len % 4;
    uint32_t chunks = (leftover == 0) ? len / 4 - 1 : len /4;
    uint8_t* p = (uint8_t*) dest;
    uint32_t x = 0;
    uint32_t* destInt = (uint32_t*) p;
    uint32_t* srcInt = (uint32_t*) src;
    uint32_t y = *srcInt++;

    for (i = 0; i < chunks; ++i) {
        x = d0[y >> 24 & 0xff] | d1[y >> 16 & 0xff] |
            d2[y >> 8 & 0xff] | d3[y & 0xff];
#if 0
        if (x >= BADCHAR)  return MODP_B64_ERROR;
#endif
        *destInt = x << 8;
        p += 3;
        destInt = (uint32_t*)p;
        y = *srcInt++;
    }

    switch (leftover) {
    case 0:
        x = d0[y >> 24 & 0xff] | d1[y >> 16 & 0xff] |
            d2[y >>  8 & 0xff] | d3[y & 0xff];
#if 0
        if (x >= BADCHAR)  return MODP_B64_ERROR;
#endif
        *p++ = ((uint8_t*)&x)[1];
        *p++ = ((uint8_t*)&x)[2];
        *p = ((uint8_t*)&x)[3];
#if 0
        return (chunks+1)*3;
#endif
        return;
    case 1:
        x = d3[y >> 24];
        *p =  (uint8_t)x;
        break;
    case 2:
        x = d3[y >> 24] *64 + d3[(y >> 16) & 0xff];
        *p =  (uint8_t)(x >> 4);
        break;
    default:  /* case 3 */
        x = (d3[y >> 24] *64 + d3[(y >> 16) & 0xff])*64 +
            d3[(y >> 8) & 0xff];
        *p++ = (uint8_t) (x >> 10);
        *p = (uint8_t) (x >> 2);
        break;
    }
#if 0
    if (x >= BADCHAR) return MODP_B64_ERROR;
    return 3*chunks + (6*leftover)/8;
#endif
}

#else /* LITTLE  ENDIAN -- INTEL AND FRIENDS */
void Base64Decode(char* dest, const char* src, uint32_t len)
{
    if (len == 0) return;
#ifdef DOPAD
    /*
     * if padding is used, then the message must be at least
     * 4 chars and be a multiple of 4
     */
#if 0
    if (len < 4 || (len % 4 != 0)) return MODP_B64_ERROR; /* error */
#endif
    /* there can be at most 2 pad chars at the end */
    if (src[len-1] == CHARPAD) {
        len--;
        if (src[len -1] == CHARPAD) {
            len--;
        }
    }
#endif
    uint32_t i;
    int leftover = len % 4;
    uint32_t chunks = (leftover == 0) ? len / 4 - 1 : len /4;
    uint8_t* p = (uint8_t*)dest;
    uint32_t x = 0;
    const uint8_t* y = (uint8_t*)src;

    for (i = 0; i < chunks; ++i, y += 4) {
        x = d0[y[0]] | d1[y[1]] | d2[y[2]] | d3[y[3]];
#if 0
        if (x >= BADCHAR) return MODP_B64_ERROR;
#endif
        *p++ =  ((uint8_t*)(&x))[0];
        *p++ =  ((uint8_t*)(&x))[1];
        *p++ =  ((uint8_t*)(&x))[2];
    }
    switch (leftover) {
    case 0:
        x = d0[y[0]] | d1[y[1]] | d2[y[2]] | d3[y[3]];
#if 0
        if (x >= BADCHAR) return MODP_B64_ERROR;
#endif
        *p++ =  ((uint8_t*)(&x))[0];
        *p++ =  ((uint8_t*)(&x))[1];
        *p =    ((uint8_t*)(&x))[2];
#if 0
        return (chunks+1)*3;
#endif
        break;
    case 1:  /* with padding this is an impossible case */
        x = d0[y[0]];
        *p = *((uint8_t*)(&x)); // i.e. first char/byte in int
        break;
    case 2: // * case 2, 1  output byte */
        x = d0[y[0]] | d1[y[1]];
        *p = *((uint8_t*)(&x)); // i.e. first char
        break;
    default: /* case 3, 2 output bytes */
        x = d0[y[0]] | d1[y[1]] | d2[y[2]];  /* 0x3c */
        *p++ =  ((uint8_t*)(&x))[0];
        *p =  ((uint8_t*)(&x))[1];
        break;
    }
#if 0
    if (x >= BADCHAR) return MODP_B64_ERROR;
    return 3*chunks + (6*leftover)/8;
#endif
}

#endif  /* if bigendian / else / endif */

uint32_t Base64GetEncodedDataSize(uint32_t dwDataSize)
{
    return (((4 * dwDataSize) / 3) + 3) & ~3;
}

uint32_t Base64GetDecodedDataSize(const char *lpBase64Data, uint32_t dwBase64Size)
{
    uint8_t nPaddings = 0;
    const char *p = lpBase64Data + dwBase64Size - 1;

    while(*p-- == CHARPAD) nPaddings++;
    if(unlikely(nPaddings > 2))
        return (uint32_t)-1;

    return (3 * (dwBase64Size / 4)) - nPaddings;
}
