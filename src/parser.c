/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

/*-T-T--------------------------------------------------------------------------
  NEEDS:    src/lexer.c
  NEEDS:    src/document.c
  NEEDS:    src/base64.c
  NEEDS:    src/convert_i64.c
  NEEDS:    src/convert_real.c
  NEEDS:    src/schema.c
  NEEDS:    src/delegate.c
--------------------------------------------------------------------------T-T-*/

#include <assert.h>
#include <stdint.h>
#include <string.h>
#include <libgen.h>

#include "buffer.h"
#include "cmacros.h"
#include "delegate.h"
#include "lexer.h"
#include "document.h"
#include "parser.h"
#include "base64.h"
#include "convert.h"
#include "token.h"
#include "schema.h"
#include "xodl.h"

#define TOKEN_ARGS(t)                       t.dwLine, t.dwCol, t.dwCol
#define REPORT_ERROR(T, ...)                DelegatePrintf(lpDelegate, __VA_ARGS__); \
                                            DelegateCallParseErrorHandler(lpDelegate, lpXODL->szIdent, TOKEN_ARGS(T))

/* forward prototypes */
static bool ReadToken(PLexerState lpLexer, PXODLUserDelegate lpDelegate, PToken lpToken);
static bool ExpectToken(PLexerState lpLexer, PXODLUserDelegate lpDelegate, ETokenKind eDesiredToken, PToken lpTokenOut);
static PXODLValue ParseString(PXODLDocument lpXODL, PLexerState lpLexer, PXODLUserDelegate lpDelegate);
static PXODLValue ParseBase64String(PXODLDocument lpXODL, PLexerState lpLexer, PXODLUserDelegate lpDelegate);
static PXODLValue ParseInteger(PXODLDocument lpXODL, PLexerState lpLexer, PXODLUserDelegate lpDelegate, uint8_t nBase);
static PXODLValue ParseReal(PXODLDocument lpXODL, PLexerState lpLexer, PXODLUserDelegate lpDelegate);
static PXODLValue ParseList(PXODLDocument lpXODL, PLexerState lpLexer, PXODLUserDelegate lpDelegate);
static PXODLValue ParseValue(PXODLDocument lpXODL, PLexerState lpLexer, PXODLUserDelegate lpDelegate);
static PXODLObject ParseObject(PXODLDocument lpXODL, PLexerState lpLexer, PXODLUserDelegate lpDelegate, bool bRootObject, bool bLoadDeps, int flags);

PXODLDocument ParseXODLDocument (
    PXODLState          lpState,
    const char *        fname,
    void *              lpData,
    size_t              ulSize,
    bool                bLoadDeps
) {
    LexerState Lexer;
    PXODLDocument lpXODL;

#ifndef NDEBUG
    char *p = lpData;
    assert(ulSize > 0);
    assert((p[ulSize] == 0 || p[ulSize - 1] == 0) && "data is not properly terminated");
#endif

    LexerStateInit(&Lexer, &lpState->Delegate, fname, lpData);
    lpXODL = XODLDocumentCreateEx(lpState, XODL_TYPE_Document, NULL);
    lpXODL->szIdent = (void*)fname; // This will be overriden after return.
    lpXODL->lpRootObject = ParseObject(lpXODL, &Lexer, &lpState->Delegate, true /*root*/, bLoadDeps, 0);
    if(lpXODL->lpRootObject == NULL) {
        XODLDocumentDestroy(lpXODL);
        return NULL;
    }
    lpXODL->lpRootObject->bHasBeenAssigned = true;

    return lpXODL;
}

static PXODLObject ParseObject (
    PXODLDocument       lpXODL,
    PLexerState         lpLexer,
    PXODLUserDelegate   lpDelegate,
    bool                bRootObject,
    bool                bLoadDeps,
    int                 flags
) {
    Token T;
    const char *lpIdent = NULL;
    const char *lpOID = OID_ANON;
    const char *lpInheritObjectPath = NULL;
    char ident[XODL_IDENTIFIER_MAXLENGTH];
    uint32_t dwIdentSize = 0;
    uint16_t wInheritPathSize = 0;
    ETokenKind eNextToken;
    PXODLObject lpObject;
    EXODLDocumentType eFixedType;

    // Parse the object header
    if(false == ReadToken(lpLexer, lpDelegate, &T))
        return NULL;

    if(T.eKind == TOKEN_Identifier) {
        lpIdent = T.lpStart;
        dwIdentSize = T.dwLength;

        // The identifier of a root object has to be either 'XODL' or 'XODLSchema',
        // and correct the document type accordingly
        if(bRootObject) {
            if(!strncmp(lpIdent, "XODL", dwIdentSize))
                eFixedType = XODL_TYPE_Document;
            else if(!strncmp(lpIdent, "XODLSchema", dwIdentSize))
                eFixedType = XODL_TYPE_Schema;
            else {
                REPORT_ERROR(T, "expected either 'XODL' or 'XODLSchema'");
                return NULL;
            }
        }

        switch(LexerPeek(lpLexer, NULL, 1)) {
            case TOKEN_LeftSquare:
                LexerReadToken(lpLexer, &T); // Skip the left sqaure
                goto parse_inherit_path;
            case TOKEN_Colon:
                if(false == ExpectToken(lpLexer, lpDelegate, TOKEN_ObjectId, &T))
                    return NULL;
                goto parse_object_id;
            case TOKEN_ERROR:
                return NULL;
            default:
                break;
        }

    } else if(T.eKind == TOKEN_LeftSquare) {
parse_inherit_path:
        if(false == ExpectToken(lpLexer, lpDelegate, TOKEN_ObjectPath, &T))
            return NULL;
        lpInheritObjectPath = T.lpStart;
        wInheritPathSize = T.dwLength;
        // TODO: check the if the object path is valid
        if(false == ExpectToken(lpLexer, lpDelegate, TOKEN_RightSquare, &T))
            return NULL;

        if(TOKEN_Colon == LexerPeek(lpLexer, NULL, 1)){
            if(false == ExpectToken(lpLexer, lpDelegate, TOKEN_ObjectId, &T))
                return NULL;
            goto parse_object_id;
        }

    } else if(T.eKind == TOKEN_String || T.eKind == TOKEN_RawString) {
        if(bRootObject) {
            REPORT_ERROR(T, "schema object as root object");
            return NULL;
        } else if(lpXODL->eType != XODL_TYPE_Schema) {
            REPORT_ERROR(T, "schema object is not allowed in XODL document");
            return NULL;
        }
        // we don't escape strings in schema document
        if(T.eKind == TOKEN_String) {
            lpIdent = T.lpStart + 1;
            dwIdentSize = T.dwLength - 2;
        } else {
            lpIdent = T.lpStart + 2;
            dwIdentSize = T.dwLength - 3;
        }

    } else if(T.eKind == TOKEN_ObjectId) {
parse_object_id:
        // Root object cannot have object ID
        if(bRootObject) {
            REPORT_ERROR(T, "object ID is assigned to a root object");
            return NULL;
        }
        lpOID = T.lpStart + 1; // Skip the leading #

    } else if(T.eKind != TOKEN_LeftBrace) {
        REPORT_ERROR(T, "expected %s", TokenKindToString(TOKEN_Identifier));
        return NULL;
    }

    if(bRootObject && lpIdent == NULL) {
        REPORT_ERROR(T, "expected either 'XODL' or 'XODLSchema'");
        return NULL;
    }
    if(lpXODL->eType == XODL_TYPE_Schema && lpOID != OID_ANON) {
        REPORT_ERROR(T, "schema object with object ID");
        return NULL;
    }

    if(bLoadDeps == false)
        flags |= XODL_IGNORE_INHERITANCE;
    lpObject = XODLObjectCreate(lpXODL, lpIdent, dwIdentSize, lpInheritObjectPath, wInheritPathSize, (void*)lpOID, flags);
    if(bRootObject)
        lpXODL->eType = eFixedType;
    if(!lpObject) return NULL;

    // Parse the object body { ... }
    if(T.eKind != TOKEN_LeftBrace && false == ExpectToken(lpLexer, lpDelegate, TOKEN_LeftBrace, &T))
        goto parse_failed;

    if(lpXODL->eType == XODL_TYPE_Document) {
        while((eNextToken = LexerPeek(lpLexer, &T, 1)) != TOKEN_EOF) {
            switch(eNextToken) {
                case TOKEN_LeftBrace:
                    REPORT_ERROR(T, "anonymous object cannot be an object member");
                    goto parse_failed;

                case TOKEN_Identifier:
                    if(TOKEN_Equal == LexerPeek(lpLexer, NULL, 2)) {
                        PXODLValue lpPropValue = NULL;

                        // Parse property: IDENT '=' <VALUE>;
                        LexerReadToken(lpLexer, &T);
                        // Note: It's safe to reuse lpIdent cause a copy of that should've made in XODLObjectCreate()
                        lpIdent = T.lpStart;
                        dwIdentSize = T.dwLength;

                        LexerReadToken(lpLexer, &T); // Swallow the '='
                        lpPropValue = ParseValue(lpXODL, lpLexer, lpDelegate);
                        if(lpPropValue == NULL)
                            goto parse_failed;
                        else if(false == ExpectToken(lpLexer, lpDelegate, TOKEN_Semicolon, &T))
                            goto parse_failed;

                        if(false == XODLObjectAddProperty(lpObject, lpIdent, dwIdentSize, lpPropValue, 0))
                            goto parse_failed;

                        if(bRootObject && dwIdentSize == 8 && !strncmp(lpIdent, "XODLDeps", 8)) {
                            if(bLoadDeps && false == XODLDocumentLoadDeps(lpXODL->lpState, lpXODL, lpPropValue))
                                goto parse_failed;
                        }
                        break;
                    }
                    /* FALLTHROUGH */

                case TOKEN_LeftSquare:              /* FALLTHROUGH */
                case TOKEN_ObjectId: {
                    PXODLObject lpChild = ParseObject(lpXODL, lpLexer, lpDelegate, false/*non_root*/, false/*no_dep*/, 0);

                    if(unlikely(lpChild == NULL))
                        goto parse_failed;
                    if(false == XODLObjectAddObject(lpObject, lpChild)) {
                        DelegatePrintf(lpDelegate, "'%s' already exists under ", lpChild->szIdent);
                        DelegateWriteObjectInfo(lpDelegate, lpObject);
                        DelegateCallParseErrorHandler(lpDelegate, lpLexer->szFileName, TOKEN_ARGS(T));
                        goto parse_failed;
                    }
                }   break;

                case TOKEN_RightBrace:
                    goto finished;

                case TOKEN_ERROR:
                    goto parse_failed;

                default:
                    REPORT_ERROR(T, "unexpected token %s", TokenKindToString(T.eKind));
                    goto parse_failed;
            }
        }

    } else {
        // parse schema object body
        while((eNextToken = LexerPeek(lpLexer, &T, 1)) != TOKEN_EOF) {
            int flags = 0;
parse_member_again:
            switch(eNextToken) {
                case TOKEN_LeftBrace:
                    REPORT_ERROR(T, "anonymous object cannot be an object member");
                    goto parse_failed;

                case TOKEN_QuestionMark:
                    LexerReadToken(lpLexer, &T);
                    eNextToken = LexerPeek(lpLexer, &T, 1);
                    flags = XODL_SCHEMA_OPTIONAL;
                    goto parse_member_again;

                case TOKEN_String:      /* FALLTHROUGH */
                    if(TOKEN_Equal == LexerPeek(lpLexer, NULL, 2)) {
                        PXODLValue lpValue;

                        LexerReadToken(lpLexer, &T);
                        if(eNextToken == TOKEN_String) {
                            lpIdent = T.lpStart + 1;
                            dwIdentSize = T.dwLength - 2;
                        } else {
                            lpIdent = T.lpStart + 2;
                            dwIdentSize = T.dwLength - 3;
                        }

                        LexerReadToken(lpLexer, &T); // swallow the '='
                        LexerPeek(lpLexer, &T, 1);
                        lpValue = ParseValue(lpXODL, lpLexer, lpDelegate);
                        if(lpValue == NULL)
                            goto parse_failed;
                        else if(!IsValidSchemaValueKind(lpValue->eKind)) {
                            REPORT_ERROR(T, "invalid schema value");
                            goto parse_failed;
                        } else if(false == ExpectToken(lpLexer, lpDelegate, TOKEN_Semicolon, &T))
                            goto parse_failed;

                        if(false == XODLObjectAddProperty(lpObject, lpIdent, dwIdentSize, lpValue, flags)) {
                            strlcpy(ident, lpIdent, dwIdentSize + 1);
                            DelegatePrintf(lpDelegate, "'%s' already exists under object ", ident);
                            DelegateWriteObjectInfo(lpDelegate, lpObject);
                            DelegateCallParseErrorHandler(lpDelegate, lpLexer->szFileName, TOKEN_ARGS(T));
                            goto parse_failed;
                        }
                    } else {
                        PXODLObject lpChild = ParseObject(lpXODL, lpLexer, lpDelegate, false, false, flags);

                        assert(lpChild->bSchemaObject == true);
                        if(lpChild == NULL)
                            goto parse_failed;
                        else if(false == XODLObjectAddObject(lpObject, lpChild)) {
                            REPORT_ERROR(T, "schema object \"%s\" already exists under object \"%s\"", lpChild->szIdent, lpObject->szIdent);
                            goto parse_failed;
                        }
                    }
                    break;

                case TOKEN_ObjectPath:
                    REPORT_ERROR(T, "object reference is not allowed in schema document");
                    goto parse_failed;

                case TOKEN_RightBrace:
                    goto finished;

                case TOKEN_ERROR:
                    goto parse_failed;

                default:
                    REPORT_ERROR(T, "unexpected token %s", TokenKindToString(T.eKind));
                    goto parse_failed;
            }
        }
    }

finished:
    if(false == ExpectToken(lpLexer, lpDelegate, TOKEN_RightBrace, &T))
        // unclosed object body
        goto parse_failed;

    return lpObject;

parse_failed:
    XODLObjectDestroy(lpObject);
    return NULL;
}

static PXODLValue ParseValue(PXODLDocument lpXODL, PLexerState lpLexer, PXODLUserDelegate lpDelegate)
{
    Token T;
    ETokenKind eNextToken;
    EXODLValueKind eKind;
    PXODLValue lpSchemaValue;

    eNextToken = LexerPeek(lpLexer, &T, 1);
    if(eNextToken == TOKEN_EOF) {
        REPORT_ERROR(T, "EOF reached unexpectedly");
        return NULL;
    } else if(eNextToken == TOKEN_ERROR)
        return NULL;

    switch(eNextToken) {
        case TOKEN_String:          /* FALLTHROUGH */
        case TOKEN_RawString:
            return ParseString(lpXODL, lpLexer, lpDelegate);

        case TOKEN_Base64String:
            return ParseBase64String(lpXODL, lpLexer, lpDelegate);

        case TOKEN_Integer:
            return ParseInteger(lpXODL, lpLexer, lpDelegate, 10);

        case TOKEN_IntegerB:
            return ParseInteger(lpXODL, lpLexer, lpDelegate, 2);

        case TOKEN_IntegerH:
            return ParseInteger(lpXODL, lpLexer, lpDelegate, 16);

        case TOKEN_IntegerO:
            return ParseInteger(lpXODL, lpLexer, lpDelegate, 8);

        case TOKEN_Boolean: {
            LexerReadToken(lpLexer, &T);
            PXODLValue lpValue = XODLValueCreate(lpXODL, XODL_VALUE_Boolean);

            if(lpValue == NULL) {
                REPORT_ERROR(T, "boolean value is not allowed in the current document");
                return NULL;
            }
            XODLValueSetBoolean(lpValue, T.lpStart[0] == 't');

            return lpValue;
        }

        case TOKEN_Real:
            return ParseReal(lpXODL, lpLexer, lpDelegate);

        case TOKEN_ObjectPath: {
            LexerReadToken(lpLexer, &T);
            PXODLValue lpValue = XODLValueCreate(lpXODL, XODL_VALUE_ObjectPath);

            if(lpValue == NULL) {
                REPORT_ERROR(T, "object path is not allowed in the current document");
                return NULL;
            }
            XODLValueSetObjectPath(lpValue, T.lpStart, T.dwLength);

            return lpValue;
        }

        case TOKEN_Nil: {
            PXODLValue lpValue = XODLValueCreate(lpXODL, XODL_VALUE_Nil);

            if(lpValue == NULL) {
                REPORT_ERROR(T, "nil is not allowed in the current document");
                return NULL;
            }

            return lpValue;
        }

        case TOKEN_LeftSquare:
            return ParseList(lpXODL, lpLexer, lpDelegate);

        case TOKEN_ObjectId:
            if(TOKEN_LeftBrace != LexerPeek(lpLexer, NULL, 2)) {
                LexerReadToken(lpLexer, &T);
                PXODLValue lpValue = XODLValueCreate(lpXODL, XODL_VALUE_ObjectID);

                if(lpValue == NULL) {
                    REPORT_ERROR(T, "object ID literal is not allowed in the current document");
                    return NULL;
                }
                XODLValueSetObjectID(lpValue, (void*)(T.lpStart + 1));
                return lpValue;
            }                       /* FALLTHROUGH */

        case TOKEN_Identifier:
            REPORT_ERROR(T, "only anonymous object can be used as value");
            return NULL;

        case TOKEN_INTEGER:
            LexerReadToken(lpLexer, &T);
            eKind = XODL_VALUE_TYPE_NAME_INTEGER;
            goto create_schema_val;

        case TOKEN_BOOLEAN:
            LexerReadToken(lpLexer, &T);
            eKind = XODL_VALUE_TYPE_NAME_BOOLEAN;
            goto create_schema_val;

        case TOKEN_REAL:
            LexerReadToken(lpLexer, &T);
            eKind = XODL_VALUE_TYPE_NAME_REAL;
            goto create_schema_val;

        case TOKEN_STRING:
            LexerReadToken(lpLexer, &T);
            eKind = XODL_VALUE_TYPE_NAME_STRING;
            goto create_schema_val;

        case TOKEN_OID:
            LexerReadToken(lpLexer, &T);
            eKind = XODL_VALUE_TYPE_NAME_OID;
            goto create_schema_val;

        case TOKEN_OBJPATH:
            LexerReadToken(lpLexer, &T);
            eKind = XODL_VALUE_TYPE_NAME_OBJPATH;
            goto create_schema_val;

        case TOKEN_LIST:
            LexerReadToken(lpLexer, &T);
            eKind = XODL_VALUE_TYPE_NAME_LIST;
            goto create_schema_val;

        case TOKEN_ANYTYPE:
            LexerReadToken(lpLexer, &T);
            eKind = XODL_VALUE_TYPE_NAME_ANYTYPE;
            goto create_schema_val;

        case TOKEN_LeftBrace: {
            PXODLObject lpObject = ParseObject(lpXODL, lpLexer, lpDelegate, false, false, 0);
            PXODLValue lpValue;

            if(NULL == lpObject)
                return NULL;
            XODLValueSetObject((lpValue = XODLValueCreate(lpXODL, XODL_VALUE_Object)), lpObject);

            return lpValue;
        }

        default:
            REPORT_ERROR(T, "unexpected token %s", TokenKindToString(T.eKind));
            return NULL;
    }
    __UNREACHABLE("bug: code execution out of control");

create_schema_val:
    if(!(lpSchemaValue = XODLValueCreate(lpXODL, eKind))) {
        REPORT_ERROR(T, "schema values cannot be used in XODL document");
        return NULL;
    }

    return lpSchemaValue;
}

static PXODLValue ParseList(PXODLDocument lpXODL, PLexerState lpLexer, PXODLUserDelegate lpDelegate)
{
    Token T;
    PXODLValue lpValueList = XODLValueCreate(lpXODL, XODL_VALUE_List);
    bool bNeedComma = false;

    LexerReadToken(lpLexer, &T);        // swallow the '['
    XODLValueSetList(lpValueList, 0);
    for(;;) {
        ETokenKind eNextToken = LexerPeek(lpLexer, NULL, 1);

        switch(eNextToken) {
            default: {
                PXODLValue lpItem = ParseValue(lpXODL, lpLexer, lpDelegate);

                if(lpItem == NULL)
                    return NULL;
                XODLValueListAddItem(lpValueList, lpItem);
                bNeedComma = true;
            }   break;

            case TOKEN_Comma:
                LexerReadToken(lpLexer, &T);
                if(bNeedComma == false) {
                    REPORT_ERROR(T, "unexpected comma");
                    return false;
                }
                bNeedComma = false;
                break;

            case TOKEN_RightSquare:
                LexerReadToken(lpLexer, &T);    // swallow the ']'
                return lpValueList;

            case TOKEN_ERROR:
            case TOKEN_EOF:
                goto unfinished_list;
        }
    }

unfinished_list:
    LexerReadToken(lpLexer, &T);
    REPORT_ERROR(T, "unclosed list expression");

    return NULL;
}

static PXODLValue ParseInteger(PXODLDocument lpXODL, PLexerState lpLexer, PXODLUserDelegate lpDelegate, uint8_t nBase)
{
    Token T;
    int64_t val;
    PXODLValue lpValue;
    char *endp = NULL;

    LexerReadToken(lpLexer, &T);
    lpValue = XODLValueCreate(lpXODL, XODL_VALUE_Integer);
    if(lpValue == NULL) {
        REPORT_ERROR(T, "integer value is not expected here");
        goto failed;
    }

    val = StringToInt64(T.lpStart, &endp, nBase);
    if(endp != T.lpStart + T.dwLength) {
        REPORT_ERROR(T, "malformed integer value");
        goto failed;
    }

    XODLValueSetInteger(lpValue, val);
    return lpValue;

failed:
    XODLValueDestroy(lpValue);
    return NULL;
}

static PXODLValue ParseReal(PXODLDocument lpXODL, PLexerState lpLexer, PXODLUserDelegate lpDelegate)
{
    Token T;
    double dbVal;
    PXODLValue lpValue;

    LexerReadToken(lpLexer, &T);
    lpValue = XODLValueCreate(lpXODL, XODL_VALUE_Real);
    if(lpValue == NULL) {
        REPORT_ERROR(T, "real number is not expected here");
        goto failed;
    }

    if(!strncmp(T.lpStart, "inf", T.dwLength))
        dbVal = INFINITY;
    else if(!strncmp(T.lpStart, "nan", T.dwLength))
        dbVal = NAN;
    else if(StringToDouble(T.lpStart, NULL, &dbVal)) {
        REPORT_ERROR(T, "malformed real number");
        goto failed;
    }
    XODLValueSetReal(lpValue, dbVal);

    return lpValue;

failed:
    XODLValueDestroy(lpValue);
    return NULL;
}

static PXODLValue ParseBase64String(PXODLDocument lpXODL, PLexerState lpLexer, PXODLUserDelegate lpDelegate)
{
    Token T;
    uint32_t dwDecodedSize;
    const char *lpBase64;
    PXODLValue lpValue;

    LexerReadToken(lpLexer, &T);
    lpValue = XODLValueCreate(lpXODL, XODL_VALUE_Data);
    if(lpValue == NULL) {
        REPORT_ERROR(T, "byte data is not expected here");
        goto failed;
    }

    lpBase64 = T.lpStart + 2;
    dwDecodedSize = Base64GetDecodedDataSize(lpBase64, T.dwLength - 3);
    struct SRawData *lpRawData = MemoryManagerMalloc(MemoryManagerGetInstance(lpValue), sizeof(struct SRawData) + dwDecodedSize);
    Base64Decode(lpRawData->lpData, lpBase64, T.dwLength - 3);

    lpValue->lpRawData = lpRawData;

    return lpValue;

failed:
    XODLValueDestroy(lpValue);
    return NULL;
}

static PXODLValue ParseString(PXODLDocument lpXODL, PLexerState lpLexer, PXODLUserDelegate lpDelegate)
{
    Token T;
    PXODLValue lpValue;
    const char *lpStr;
    uint32_t dwLength;
    EXODLValueKind eKind;

    (void)lpDelegate;

    LexerReadToken(lpLexer, &T);
    switch(T.eKind) {
        case TOKEN_String:
            lpStr = T.lpStart + 1;
            dwLength = T.dwLength - 2;
            eKind = XODL_VALUE_String;
            break;
        case TOKEN_RawString:
            lpStr = T.lpStart + 2;
            dwLength = T.dwLength - 3;
            eKind = XODL_VALUE_RawString;
            break;
        default:
            __UNREACHABLE("bug: unexpected token kind");
    }

    if(lpXODL->eType != XODL_TYPE_Schema) {
        ETokenKind eNextToken;

        lpValue = XODLValueCreate(lpXODL, eKind);
        XODLValueSetString(lpValue, lpStr, dwLength);

        while(TOKEN_EOF != (eNextToken = LexerPeek(lpLexer, NULL, 1))) {
            bool bRawString;

            lpStr = NULL;
            switch(eNextToken) {
                case TOKEN_String:
                    LexerReadToken(lpLexer, &T);
                    lpStr = T.lpStart + 1;
                    dwLength = T.dwLength - 2;
                    bRawString = false;
                    break;
                case TOKEN_RawString:
                    LexerReadToken(lpLexer, &T);
                    lpStr = T.lpStart + 2;
                    dwLength = T.dwLength - 3;
                    bRawString = true;
                    break;
                case TOKEN_ERROR:
                    return NULL;
                default:
                    return lpValue;
            }
            if(lpStr)
                XODLValueAppendString(lpValue, lpStr, dwLength, bRawString);
        }
    } else {
        lpValue = XODLValueCreate(lpXODL, XODL_VALUE_RegExp);
        XODLValueSetRegularExpression(lpValue, lpStr, dwLength);
    }

    return lpValue;
}

static __FORCE_INLINE__ bool ReadToken(PLexerState lpLexer, PXODLUserDelegate lpDelegate, PToken lpToken)
{
    if(LexerReadToken(lpLexer, lpToken) == false) {
        DelegatePrintf(lpDelegate, "EOF reached unexpectedly");
        DelegateCallParseErrorHandler(lpDelegate, lpLexer->szFileName, lpToken->dwLine, lpToken->dwCol, lpToken->dwCol);
        return false;
    }

    return true;
}

static bool ExpectToken(PLexerState lpLexer, PXODLUserDelegate lpDelegate, ETokenKind eDesiredToken, PToken lpTokenOut)
{
    if(unlikely(LexerReadToken(lpLexer, lpTokenOut) == false)) {
        if(!lpLexer->bErrorOccurred) {
            DelegatePrintf(lpDelegate, "EOF reached unexpectedly");
            DelegateCallParseErrorHandler(lpDelegate, lpLexer->szFileName, lpTokenOut->dwLine, lpTokenOut->dwCol, lpTokenOut->dwCol);
        }
        return false;
    } else if(lpTokenOut->eKind != eDesiredToken) {
        DelegatePrintf(lpDelegate, "expected %s but got %s", TokenKindToString(eDesiredToken), TokenKindToString(lpTokenOut->eKind));
        DelegateCallParseErrorHandler(lpDelegate, lpLexer->szFileName, lpTokenOut->dwLine, lpTokenOut->dwCol, lpTokenOut->dwCol);
        return false;
    }
    return true;
}
