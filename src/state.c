/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

/*-T-T--------------------------------------------------------------------------
  NEEDS:    src/buffer.c
  NEEDS:    src/document.c
  NEEDS:    src/hashtable.c
  NEEDS:    src/parser.c
  NEEDS:    src/xodb.c
  NEEDS:    src/delegate.c
  NEEDS:    src/validate.c
--------------------------------------------------------------------------T-T-*/

#include <assert.h>
#include <string.h>

#include "memmgr.h"
#include "xodl.h"
#include "hashtable.h"
#include "cmacros.h"
#include "delegate.h"
#include "document.h"
#include "parser.h"
#include "xodb.h"
#include "validate.h"

PXODLState XODLStateCreate(FXODLUserFileLoad fnLoadFile, FXODLUserFileUnload fnUnloadFile)
{
    if(unlikely(fnLoadFile == NULL))
        return NULL;

    PXODLState lpState = malloc(sizeof(XODLState));

    lpState->lpCachedDocuments = HashtableCreate(NULL, 2, (FStandardDataDestructor)XODLDocumentDestroy, false);
    lpState->fnLoadFile = fnLoadFile;
    lpState->fnUnloadFile = fnUnloadFile;
    DelegateInit(&lpState->Delegate);

    return lpState;
}

void XODLStateClear(PXODLState lpState)
{
    HashtableClear(lpState->lpCachedDocuments);
}

void XODLStateDestroySafe(PXODLState *lppState)
{
    assert(lppState != NULL && *lppState != NULL);
    PXODLState lpState = *lppState;

    HashtableDestroy(lpState->lpCachedDocuments);
    DelegateFree(&lpState->Delegate);
    free(lpState);

    *lppState = NULL;
}

void XODLStateSetMessageHandler (
    PXODLState              lpState,
    FXODLMessageHandler     fnMessageHandler,
    void *                  lpUserArg
) {
    DelegateSetMessageHandler(&lpState->Delegate, fnMessageHandler, lpUserArg);
}

void XODLStateSetParseErrorHandler (
    PXODLState              lpState,
    FXODLParseErrorHandler  fnErrorHandler,
    void *                  lpUserArg
) {
    DelegateSetParseErrorHandler(&lpState->Delegate, fnErrorHandler, lpUserArg);
}

PXODLDocument XODLStateParseDocument (
    PXODLState              lpState,
    const char *            szFilePath,
    const char *            szAlias,
    int                     flags
) {
    assert(lpState != NULL && szFilePath != NULL);
    PXODLDocument lpXODL;
    const char *szRegisterName = szAlias ? szAlias : szFilePath;
    uint32_t dwRegisterNameSize = strlen(szRegisterName);

    lpXODL = HashtableQuery(lpState->lpCachedDocuments, szRegisterName, dwRegisterNameSize);
    if(lpXODL == HT_ITEM_NOTFOUND) {
        void *lpContent, *lpFileArg;
        size_t ulFileSize;

        if(lpState->fnLoadFile(szFilePath, &lpContent, &ulFileSize, &lpFileArg, lpState->lpUserLoadArg) == false)
            return NULL;

        if(!IsXODBHeader(lpContent, ulFileSize))
            lpXODL = ParseXODLDocument(lpState, szFilePath, lpContent, ulFileSize, flags & XODL_LOAD_DEPS);
        else
            lpXODL = XODBParseDocument(lpState, szFilePath, lpContent, ulFileSize, flags & XODL_LOAD_DEPS);

        if(lpXODL != NULL) {
            lpXODL->szIdent = MemoryManagerDuplicateString(lpXODL->lpMemMgr, szRegisterName, dwRegisterNameSize);
            *HashtableInsert(lpState->lpCachedDocuments, lpXODL->szIdent, dwRegisterNameSize) = lpXODL;
        }

        if(lpState->fnUnloadFile)
            lpState->fnUnloadFile(szFilePath, lpContent, ulFileSize, lpFileArg, lpState->lpUserLoadArg);
    }

    return lpXODL;
}

PXODLDocument XODLStateCreateDocument (
    PXODLState              lpState,
    const char *            szDocumentPath,
    EXODLDocumentType       eType
) {
    PXODLDocument lpXODL;
    PXODLObject lpRootObject;

    assert(lpState != NULL);

    if(unlikely(szDocumentPath == NULL || szDocumentPath[0] == 0))
        return NULL;

    lpXODL = HashtableQuery(lpState->lpCachedDocuments, szDocumentPath, strlen(szDocumentPath));
    if(unlikely(lpXODL != HT_ITEM_NOTFOUND))
        return NULL;

    lpRootObject = eType == XODL_TYPE_Document ?
                            XODLObjectCreate(lpXODL, "XODL", 4, NULL, 0, OID_ANON, 0) :
                            XODLObjectCreate(lpXODL, "XODLSchema", 10, NULL, 0, OID_ANON, XODL_INTERNAL_ROOT_OBJECT);
    lpRootObject->bHasBeenAssigned = true;
    lpXODL = XODLDocumentCreateEx(lpState, eType, lpRootObject);
    lpXODL->szIdent = MemoryManagerDuplicateString(lpXODL->lpMemMgr, szDocumentPath, strlen(szDocumentPath));

    *HashtableInsert(lpState->lpCachedDocuments, lpXODL->szIdent, strlen(szDocumentPath)) = lpXODL;

    return lpXODL;
}

bool XODLStateValidateDocument (
    PXODLState              lpState,
    PXODLDocument           lpXODL,
    const char *            szSchemaPath
) {
    PXODLDocument lpXODLSchema;
    PXODLUserDelegate lpDelegate = &lpState->Delegate;

    assert(lpState != NULL && lpXODL != NULL);

    const char *szIdent = lpXODL->szIdent ? lpXODL->szIdent : "<unassigned>";

    if(szSchemaPath == NULL) {
        // Read XODLSchema property for the path
        PXODLValue lpValue = XODLObjectGetPropertyValue(lpXODL->lpRootObject, "XODLSchema", 10);

        if(!lpValue)
            // if no XODLSchema specified then a document is always valid
            return true;
        else if(lpValue->eKind != XODL_VALUE_String && lpValue->eKind != XODL_VALUE_RawString) {
            DelegatePrintf(lpDelegate, "value for property \"XODLSchema\" is not a string literal");
            DelegateCallMessageHandler(lpDelegate, NULL, MESSAGE_Error);

            return false;
        }
        szSchemaPath = lpValue->szVal;
    }

    lpXODLSchema = XODLStateParseDocument(lpState, szSchemaPath, NULL, 0);
    if(lpXODLSchema == NULL)
        return false;

    return ValidateDocument(lpDelegate, szIdent, lpXODL, lpXODLSchema);
}

static bool ReadDependencyInfo(PXODLValue lpDepItem, char **lppObjectID, char **lppPath, char **lppAlias)
{
    PXODLObject lpDepObj = NULL;
    PXODLValue lpValue;

    XODLValueGetObject(lpDepItem, &lpDepObj);
    assert(lpDepObj != NULL);

    // Error checkings for dependency records must've done before reaching here.
    lpValue = XODLObjectGetPropertyValue(lpDepObj, "ObjectID", 8);
    assert(lpValue != NULL);
    XODLValueGetObjectID(lpValue, (XODLObjectId*)lppObjectID);
    assert(*lppObjectID != NULL);

    lpValue = XODLObjectGetPropertyValue(lpDepObj, "Path", 4);
    assert(lpValue != NULL);
    XODLValueGetString(lpValue, lppPath);
    assert(*lppPath != NULL);

    lpValue = XODLObjectGetPropertyValue(lpDepObj, "Alias", 5);
    assert(lpValue != NULL);
    if(!lpValue || !XODLValueGetString(lpValue, lppAlias))
        *lppAlias = NULL;

    return true;
}

/* INTERNAL API */
bool XODLDocumentLoadDeps(PXODLState lpState, PXODLDocument lpXODL, PXODLValue lpDepList)
{
    PXODLUserDelegate lpDelegate = &lpState->Delegate;
    const char *szFileName = lpXODL->szIdent;
    bool bSuccess = true;

    assert(lpXODL->eType == XODL_TYPE_Document);
    assert(lpDepList && XODLValueGetKind(lpDepList) == XODL_VALUE_List);
    assert(lpXODL->bHasDeps && lpXODL->bDepsLoaded == false);

    XODLValueListIteratorInit(lpDepList);
    for(PXODLValue lpItem = XODLValueListIteratorNextItem(lpDepList); lpItem; lpItem = XODLValueListIteratorNextItem(lpDepList)) {
        char *szObjectID, *szPath, *szAlias;
        PXODLDocument lpDocument;

        ReadDependencyInfo(lpItem, &szObjectID, &szPath, &szAlias);

        DelegatePrintf(lpDelegate, "trying to load %s", szPath);
        DelegateCallMessageHandler(lpDelegate, szFileName, MESSAGE_Verbose);

        lpDocument = XODLStateParseDocument(lpState, szPath, szAlias, XODL_LOAD_DEPS);
        if(lpDocument == NULL) {
            DelegatePrintf(lpDelegate, "failed to load %s", szPath);
            DelegateCallMessageHandler(lpDelegate, szFileName, MESSAGE_Error);
            bSuccess = false;
            continue;
        }
        lpDocument->nRefCount++;

        // Register the document's root object to the global object table
        *HashtableInsert(lpXODL->lpGlobalOIDs, szObjectID, XODL_OID_LENGTH) = lpDocument->lpRootObject;

        DelegatePrintf(lpDelegate, "registered %s as #%s", szPath, szObjectID);
        DelegateCallMessageHandler(lpDelegate, szFileName, MESSAGE_Verbose);
    }
    if(bSuccess)
        lpXODL->bDepsLoaded = true;

    return bSuccess;
}
