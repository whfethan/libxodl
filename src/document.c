/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

/*-T-T--------------------------------------------------------------------------
  NEEDS:    src/hashtable.c
  NEEDS:    src/memmgr.c
  NEEDS:    src/charfact.c
  NEEDS:    src/exporter.c
  NEEDS:    src/unicode_utf8.c
  NEEDS:    src/schema.c
--------------------------------------------------------------------------T-T-*/

#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <libgen.h>

#include "delegate.h"
#include "xodl.h"
#include "hashtable.h"
#include "memmgr.h"
#include "cmacros.h"
#include "range_test.h"
#include "charfact.h"
#include "document.h"
#include "unicode_utf8.h"
#include "schema.h"
#include "regexp_interface.h"

/* forward prototypes */
static void FreeObjectMember(void *lpMember);
static void FreeValuePayload(PXODLValue lpValue);
static void FreeObject(PXODLObject lpObject);
static void CleanUpValueList(PXODLValueList lpList);
static PXODLValueList ValueListCreate(PMemoryManager lpMemMgr, uint32_t dwCountOfItems);
static void ValueListAdd(PMemoryManager lpMemMgr, PXODLValueList lpList, PXODLValue lpValue);
static bool ValueListDelete(PXODLValueList lpList, uint32_t dwIndex);
static PXODLValue ValueListGet(PXODLValueList lpList, uint32_t dwIndex);
static void ValueListDestroy(PXODLValueList lpList);

static XODLValue kDeletedValue;

/*------------------------------------------------------------------------------
  XODL Document Implementation
------------------------------------------------------------------------------*/

bool XODLDocumentLoadDependencies(PXODLDocument lpXODL)
{
    assert(lpXODL != NULL && lpXODL->lpState);

    if(unlikely(lpXODL->eType == XODL_TYPE_Schema))
        return false;
    else if(!lpXODL->bHasDeps || lpXODL->bDepsLoaded)
        return true;

    PXODLValue lpDepList = XODLObjectGetPropertyValue(lpXODL->lpRootObject, "XODLDeps", 8);
    assert(lpDepList != NULL);

    return XODLDocumentLoadDeps(lpXODL->lpState, lpXODL, lpDepList);
}

void XODLDocumentDestroy(PXODLDocument lpXODL)
{
    assert(lpXODL != NULL);
    if(--lpXODL->nRefCount < 0) {
        MemoryManagerDestroy(lpXODL->lpMemMgr);
        free(lpXODL);
    }
}

PXODLObject XODLGetRootObject(PXODLDocument lpXODL)
{
    return lpXODL->lpRootObject;
}

PXODLObject XODLGetObjectByOID(PXODLDocument lpXODL, XODLObjectId OID)
{
    if(lpXODL->eType != XODL_TYPE_Document)
        return NULL;

    PXODLObject lpObject = HashtableQuery(lpXODL->lpGlobalOIDs, OID, XODL_OID_LENGTH);
    if(lpObject != HT_ITEM_NOTFOUND && lpObject->bHasBeenAssigned)
        return lpObject;

    return NULL;
}

static bool ResolveProxyObject(PXODLObject lpObject)
{
    EXODLMemberKind eMemberKind;
    PXODLObject lpResolvedObject;

    lpResolvedObject = XODLResolveObjectPath(lpObject->lpXODL, lpObject->szInheritObjectPath, &eMemberKind);
    if(lpResolvedObject && eMemberKind == XODL_MEMBER_Object) {
        lpResolvedObject->reserved0++;
        lpObject->lpOriginObject = lpResolvedObject;

        return true;
    }

    return false;
}

void *XODLResolveObjectPath (
    PXODLDocument           lpXODL,
    const char *            szObjectPath,
    EXODLMemberKind *       lpeMemberKind
) {
    char bufPathComp[XODL_IDENTIFIER_MAXLENGTH + 1];
    char *bufp2 = bufPathComp + XODL_IDENTIFIER_MAXLENGTH;
    void *lpResult = lpXODL->lpRootObject;

    assert(lpXODL != NULL);
    assert(szObjectPath != NULL && *szObjectPath != 0);
    assert(lpeMemberKind != NULL);

    *lpeMemberKind = XODL_MEMBER_Object;

    if(unlikely(szObjectPath[0] != '\\' || szObjectPath[1] != '\\'))
        return NULL;

    const char *p = szObjectPath + 2; // skip the leading "\\"

    if(unlikely(*p == 0)) return NULL; // an empty path \\ is invalid

    while(*p != 0 && lpResult && *lpeMemberKind == XODL_MEMBER_Object) {
        char *bufp = bufPathComp;
        const char *startp = p;
        bool bLooksLikeObjectID = true;

        // extract path component
        for(; bufp < bufp2 && *p && *p != '/'; p++) {
            int charfacts = GetCharacterFacts((uint8_t)*p);

            if(likely(charfacts & (FACT_ObjectIDChar | FACT_IdentStart | FACT_IdentContinue))) {
                bLooksLikeObjectID &= (charfacts & FACT_ObjectIDChar);
                *bufp++ = *p;
            } else
                return NULL; // the path component contains invalid character
        }
        *bufp = 0;

        if(unlikely(bufp == bufp2 && *p != 0 && *p != '/') || p == startp)
            // The component is too long or too short
            return NULL;

        uint8_t nComponentLength = p - startp;
        bLooksLikeObjectID &= (nComponentLength == XODL_OID_LENGTH);
        if(bLooksLikeObjectID == false) {
            // If it's an identifier, make sure it begins with FACT_IdentStart
            if(unlikely(0 == (GetCharacterFacts((uint8_t)bufPathComp[0]) & FACT_IdentStart)))
                return NULL;

            lpResult = XODLObjectGetMember(lpResult, bufPathComp, nComponentLength, lpeMemberKind);
        } else {
            PXODLObject lpObject = XODLGetObjectByOID(lpXODL, bufPathComp);
            if(lpResult != lpXODL->lpRootObject) {
                // Object ID is not allowed in the middle of a path. However, it is possible
                // that an identifier looks like an object ID.
                if(lpObject != NULL)
                    return NULL;
            }
            lpResult = lpObject;
        }

        if(*p == '/') p++;
    }
    if(*p == 0 || (*p == '/' && p[1] == 0))
        return lpResult;

    return NULL;
}

EXODLDocumentType XODLDocumentGetType(PXODLDocument lpXODL)
{
    return lpXODL->eType;
}

/* INTERNAL API */
PXODLDocument XODLDocumentCreateEx(PXODLState lpState, EXODLDocumentType eDocuType, PXODLObject lpRootObject)
{
    PXODLDocument lpDocument = malloc(sizeof(XODLDocument));

    lpDocument->eType = eDocuType;
    lpDocument->lpState = lpState;
    lpDocument->szIdent = NULL;
    lpDocument->lpMemMgr = MemoryManagerCreate();
    lpDocument->lpRootObject = lpRootObject;
    lpDocument->lpGlobalOIDs = NULL;
    if(eDocuType == XODL_TYPE_Document)
        lpDocument->lpGlobalOIDs = HashtableCreate(lpDocument->lpMemMgr, 2, (FStandardDataDestructor)FreeObject, false);
    lpDocument->nRefCount = 0;
    lpDocument->bDepsLoaded = false;
    lpDocument->bHasDeps = false;

    return lpDocument;
}

/* INTERNAL API */
__FORCE_INLINE__ EXODLMemberKind XODLMemberGetKind(void *lpMember)
{
    switch(AS_GENERIC(lpMember)->dwSignature) {
        case XODL_OBJECT_SIGNATURE:
            return XODL_MEMBER_Object;
        case XODL_PROPERTY_SIGNATURE:
            return XODL_MEMBER_Property;
        default:
            __UNREACHABLE("unknown signature");
    }
}

/*------------------------------------------------------------------------------
  XODL Object Implementation
------------------------------------------------------------------------------*/

PXODLObject XODLObjectCreate (
    PXODLDocument           lpXODL,
    const char *            lpIdent,
    uint8_t                 nIdentSize,
    const char *            lpInheritPath,
    uint16_t                wPathLen,
    XODLObjectId            OID,
    int                     flags
) {
    PMemoryManager lpMemMgr = MEMORY_MANAGER(lpXODL);
    PXODLObject lpObject;

    if(lpXODL->eType == XODL_TYPE_Document || (flags & XODL_INTERNAL_ROOT_OBJECT)) {
        lpObject = MemoryManagerMalloc(lpMemMgr, sizeof(XODLObject));
        lpObject->dwSignature = XODL_OBJECT_SIGNATURE;

        lpObject->szIdent = NULL;
        lpObject->szObjectID = OID_ANON;
        lpObject->lpXODL = lpXODL;
        lpObject->reserved0 = 0; // reference counter

        if(lpIdent)
            lpObject->szIdent = MemoryManagerDuplicateString(lpMemMgr, lpIdent, nIdentSize);

        if(OID != OID_ANON) {
            lpObject->szObjectID = MemoryManagerDuplicateString(lpMemMgr, OID, XODL_OID_LENGTH);
            // Register the object to the document's global object table
            *HashtableInsert(lpXODL->lpGlobalOIDs, lpObject->szObjectID, XODL_OID_LENGTH) = lpObject;

            if(nIdentSize == 0 || lpIdent == NULL) {
                // Give the unnamed object a name based on its object ID
                lpObject->szIdent = MemoryManagerMalloc(lpMemMgr, XODL_OID_LENGTH + 2);
                lpObject->szIdent[0] = ANON_OBJECT_IDENT_PREFIX;
                memmove(lpObject->szIdent + 1, OID, XODL_OID_LENGTH);
                lpObject->szIdent[XODL_OID_LENGTH + 1] = 0;
            }
        }

        if(lpInheritPath && wPathLen > 0) {
            if(lpInheritPath[wPathLen - 1] == '/')
                wPathLen--;

            lpObject->szInheritObjectPath = MemoryManagerDuplicateString(lpMemMgr, lpInheritPath, wPathLen);
            lpObject->lpOriginObject = NULL;
            if(!(flags & XODL_IGNORE_INHERITANCE) && !ResolveProxyObject(lpObject)) {
                PXODLUserDelegate lpDelegate = &lpXODL->lpState->Delegate;

                DelegatePrintf(lpDelegate, "failed to resolve parent object path %s", lpObject->szInheritObjectPath);
                DelegateCallMessageHandler(lpDelegate, lpXODL->szIdent, MESSAGE_Error);

                return NULL;
            }

            if(lpObject->szIdent == NULL) {
                char *szBasePart = basename((void*)lpInheritPath);

                if(unlikely(szBasePart == NULL))
                    return NULL;
                lpObject->szIdent = MemoryManagerDuplicateString(lpMemMgr, szBasePart, strlen(szBasePart));
            }
        } else {
            lpObject->szInheritObjectPath = NULL;
            lpObject->lpOriginObject = NULL;
        }

        lpObject->lpChildren = HashtableCreate(lpMemMgr, DEFAULT_CHILDREN_COUNT, (FStandardDataDestructor)FreeObjectMember, false);
        lpObject->lpParent = NULL;
        lpObject->bHasBoundToValue = false;
        lpObject->bHasBeenAssigned = false;
        lpObject->bSchemaObject = false;
        lpObject->bSchemaOptionalObject = false;

    } else {
        if(unlikely(lpInheritPath != NULL || OID != OID_ANON))
            return NULL;

        lpObject = MemoryManagerMalloc(lpMemMgr, sizeof(XODLObject));
        lpObject->dwSignature = XODL_OBJECT_SIGNATURE;
        lpObject->lpRegExp = nIdentSize ? XODLRegExpCreate(lpMemMgr, lpIdent, nIdentSize) : NULL;
        if(nIdentSize) {
            lpObject->lpRegExp = XODLRegExpCreate(lpMemMgr, lpIdent, nIdentSize);
            if(lpObject->lpRegExp == NULL)
                return NULL;
        }
        lpObject->szObjectID = NULL;
        lpObject->lpChildren = HashtableCreate(lpMemMgr, DEFAULT_CHILDREN_COUNT, (FStandardDataDestructor)FreeObjectMember, false);
        lpObject->lpParent = NULL;
        lpObject->lpXODL = lpXODL;
        lpObject->szInheritObjectPath = NULL;
        lpObject->lpOriginObject = NULL;
        lpObject->bHasBoundToValue = false;
        lpObject->bHasBeenAssigned = false;
        lpObject->bSchemaObject = true;
        lpObject->bSchemaOptionalObject = flags & XODL_SCHEMA_OPTIONAL;
    }

    return lpObject;
}

__FORCE_INLINE__ const char *XODLObjectGetIdent(PXODLObject lpObject)
{
    assert(lpObject != NULL);
    return lpObject->szIdent;
}

__FORCE_INLINE__ const char *XODLObjectGetOID(PXODLObject lpObject)
{
    assert(lpObject != NULL);
    return lpObject->szObjectID;
}

uint32_t XODLObjectGetMemberCount(PXODLObject lpObject)
{
    return HashtableGetItemCount(lpObject->lpChildren);
}

bool XODLObjectAddObject (
    PXODLObject             lpObject,
    PXODLObject             lpChild
) {
    assert(lpObject != NULL && lpChild != NULL);

    if(unlikely(lpChild->bHasBoundToValue || lpChild->bHasBeenAssigned))
        return false;
    if(unlikely(lpObject->lpXODL != lpChild->lpXODL))
        return false;

    void **lppMemberPtr = HashtableInsert(lpObject->lpChildren, lpChild->szIdent, strlen(lpChild->szIdent));

    if(*lppMemberPtr == NULL) {
        lpChild->lpParent = lpObject;
        lpChild->bHasBeenAssigned = true;
        *lppMemberPtr = lpChild;

        return true;
    }

    return false;
}

static bool CheckXODLDepsValue(PXODLValue lpValue)
{
    PXODLValue lpDepRecord;

    if(unlikely(lpValue->eKind != XODL_VALUE_List))
        return false;

    XODLValueListIteratorInit(lpValue);
    while((lpDepRecord = XODLValueListIteratorNextItem(lpValue))) {
        PXODLObject lpDepObject;
        PXODLValue lpPropValue;

        if(XODLValueGetObject(lpDepRecord, &lpDepObject) == false)
            return false;

        lpPropValue = XODLObjectGetPropertyValue(lpDepObject, "ObjectID", 8);
        if(!lpPropValue || lpPropValue->eKind != XODL_VALUE_ObjectID)
            return false;
        lpPropValue = XODLObjectGetPropertyValue(lpDepObject, "Path", 4);
        if(!lpPropValue || !IsValueInRange(lpPropValue->eKind, XODL_VALUE_String, XODL_VALUE_RawString))
            return false;
        lpPropValue = XODLObjectGetPropertyValue(lpDepObject, "Alias", 5);
        if(lpPropValue && !IsValueInRange(lpPropValue->eKind, XODL_VALUE_String, XODL_VALUE_RawString))
            return false;
    }

    return true;
}

bool XODLObjectAddProperty (
    PXODLObject             lpObject,
    const char *            lpPropName,
    uint8_t                 nNameLength,
    PXODLValue              lpPropValue,
    int                     flags
) {
    assert(lpObject != NULL);
    assert(lpPropName != NULL && nNameLength > 0 && lpPropValue != NULL);

    PMemoryManager lpMemMgr = MemoryManagerGetInstance(lpObject);
    PXODLUserDelegate lpDelegate = &lpObject->lpXODL->lpState->Delegate;

    if(unlikely(HT_ITEM_NOTFOUND != HashtableQuery(lpObject->lpChildren, lpPropName, nNameLength))) {
        char ident[XODL_IDENTIFIER_MAXLENGTH];

        strlcpy(ident, lpPropName, nNameLength + 1);
        DelegatePrintf(lpDelegate, "property %s already exists under object ", ident);
        DelegateWriteObjectInfo(lpDelegate, lpObject);
        DelegateCallMessageHandler(lpDelegate, lpObject->lpXODL->szIdent, MESSAGE_Error);
        return false;
    }

    // If it's XODLDeps, make sure the value is valid.
    if(lpObject == lpObject->lpXODL->lpRootObject && nNameLength == 8 && !strncmp(lpPropName, "XODLDeps", 8)) {
        if(false == CheckXODLDepsValue(lpPropValue)) {
            DelegatePrintf(lpDelegate, "invalid value for XODLDeps");
            DelegateCallMessageHandler(lpDelegate, lpObject->lpXODL->szIdent, MESSAGE_Error);
            return false;
        }
        lpObject->lpXODL->bHasDeps = true;
    }

    PXODLProperty lpProp = MemoryManagerMalloc(lpMemMgr, sizeof(XODLProperty));

    lpProp->dwSignature = XODL_PROPERTY_SIGNATURE;
    lpProp->lpXODL = lpObject->lpXODL;
    lpProp->lpValue = lpPropValue;
    lpProp->lpResolvedValue = NULL;

    if(lpObject->lpXODL->eType == XODL_TYPE_Document) {
        char *szPropName = MemoryManagerDuplicateString(lpMemMgr, lpPropName, nNameLength);

        lpProp->szIdent = szPropName;
        lpProp->bSchemaProperty = false;
        lpProp->bOptional = false;
        *HashtableInsert(lpObject->lpChildren, szPropName, nNameLength) = lpProp;
    } else {
        lpProp->lpRegExp = XODLRegExpCreate(lpMemMgr, lpPropName, nNameLength);
        lpProp->bSchemaProperty = true;
        lpProp->bOptional = flags & XODL_SCHEMA_OPTIONAL;
        *HashtableInsert(lpObject->lpChildren, lpProp->lpRegExp->szExp, nNameLength) = lpProp;
    }
    lpPropValue->nRefCount++;

    return true;
}

/* INTERNAL API */
__FORCE_INLINE__ void XODLObjectAddPropertyEx(PXODLObject lpObject, PXODLProperty lpProp)
{
    if(lpProp->bSchemaProperty == false) {
        assert(lpObject->lpXODL->eType == XODL_TYPE_Document);
        *HashtableInsert(lpObject->lpChildren, lpProp->szIdent, strlen(lpProp->szIdent)) = lpProp;
    } else {
        assert(lpObject->lpXODL->eType == XODL_TYPE_Schema);
        const char *szIdent = lpProp->lpRegExp->szExp;

        *HashtableInsert(lpObject->lpChildren, szIdent, strlen(szIdent)) = lpProp;
    }
}

bool XODLObjectDeleteMember (
    PXODLObject             lpObject,
    const char *            lpIdent,
    uint8_t                 nLength
) {
    assert(lpObject != NULL && lpIdent != NULL && nLength > 0);
    return HashtableDelete(lpObject->lpChildren, lpIdent, nLength);
}

void *XODLObjectGetMember (
    PXODLObject             lpObject,
    const char *            lpIdent,
    uint8_t                 nLength,
    EXODLMemberKind *       lpeMemberKind
) {
    assert(lpObject != NULL);
    assert(lpIdent && nLength > 0);

    void *lpResult = HashtableQuery(lpObject->lpChildren, lpIdent, nLength);

    if(lpResult != HT_ITEM_NOTFOUND) {
        *lpeMemberKind = XODLMemberGetKind(lpResult);
        return lpResult;
    } else if(lpObject->szInheritObjectPath) {
#if 0
        if(lpObject->lpOriginObject == NULL && !ResolveProxyObject(lpObject))
            return NULL;
#endif
        assert(lpObject->lpOriginObject != NULL);
        return XODLObjectGetMember(lpObject->lpOriginObject, lpIdent, nLength, lpeMemberKind);
    }

    return NULL;
}

PXODLValue XODLObjectGetPropertyValue (
    PXODLObject             lpObject,
    const char *            lpIdent,
    uint8_t                 nLength
) {
    assert(lpObject && lpIdent && nLength);
    EXODLMemberKind eKind;
    PXODLProperty lpProp = XODLObjectGetMember(lpObject, lpIdent, nLength, &eKind);

    if(lpProp && eKind == XODL_MEMBER_Property)
        return lpProp->lpValue;

    return NULL;
}

PXODLObject XODLObjectGetChildObject (
    PXODLObject             lpObject,
    const char *            lpIdent,
    uint8_t                 nLength
) {
    assert(lpObject && lpIdent && nLength);
    EXODLMemberKind eKind;
    PXODLObject lpChild = XODLObjectGetMember(lpObject, lpIdent, nLength, &eKind);

    if(lpChild && eKind == XODL_MEMBER_Object)
        return lpChild;

    return NULL;
}

void XODLObjectDestroy(PXODLObject lpObject)
{
    if(lpObject->bHasBoundToValue || lpObject->reserved0)
        // We are unable to know which value is bound to this object so just return
        return;
    if(lpObject->bHasBeenAssigned) {
        if(lpObject->lpParent == NULL)
            // root object is handled by document level APIs
            return;
        // Delete its global ID record if it has an object ID
        if(lpObject->szObjectID)
            HashtableMarkItemAsDeleted(lpObject->lpXODL->lpGlobalOIDs, lpObject->szObjectID, XODL_OID_LENGTH);
        XODLObjectDeleteMember(lpObject->lpParent, lpObject->szIdent, strlen(lpObject->szIdent));
    } else {
        if(lpObject->szObjectID)
            HashtableDelete(lpObject->lpXODL->lpGlobalOIDs, lpObject->szObjectID, XODL_OID_LENGTH);
        else
            FreeObject(lpObject);
    }
}

static void FreeObject(PXODLObject lpObject)
{
    if(lpObject->szIdent) {
        if(lpObject->bSchemaObject == false)
            MemoryManagerFree(lpObject->szIdent);
        else
            XODLRegExpDestroy(lpObject->lpRegExp);
    }
    if(lpObject->szObjectID)
        MemoryManagerFree(lpObject->szObjectID);
    if(lpObject->szInheritObjectPath)
        MemoryManagerFree(lpObject->szInheritObjectPath);
    HashtableDestroy(lpObject->lpChildren);
    MemoryManagerFree(lpObject);
}

static void FreeProperty(PXODLProperty lpProp)
{
    if(lpProp->bSchemaProperty)
        XODLRegExpDestroy(lpProp->lpRegExp);
    else
        MemoryManagerFree(lpProp->szIdent);
    XODLValueDestroy(lpProp->lpValue);
    MemoryManagerFree(lpProp);
}

static void FreeObjectMember(void *lpMember)
{
    switch(AS_GENERIC(lpMember)->dwSignature) {
        case XODL_OBJECT_SIGNATURE:
            FreeObject(lpMember);
            break;
        case XODL_PROPERTY_SIGNATURE:
            FreeProperty(lpMember);
            break;
        default:
            __UNREACHABLE("bug: unexpected member kind");
    }
}

PXODLObjectMemberIterator XODLObjectMemberIteratorCreate (
    PXODLObject             lpObject,
    bool                    bIterateParentObject
) {
    assert(lpObject != NULL);
    PXODLObjectMemberIterator lpIterator = MemoryManagerMalloc(MemoryManagerGetInstance(lpObject), sizeof(XODLObjectMemberIterator));

    HashtablePrepareForIteration(lpObject->lpChildren);
    lpIterator->lpCurrentObject = lpObject;
    lpIterator->lpObjectMembers = lpObject->lpChildren;
    lpIterator->bIterateParentObject = bIterateParentObject;

    return lpIterator;
}

void *XODLObjectMemberIteratorNext (
    PXODLObjectMemberIterator lpIterator,
    EXODLMemberKind *       lpeMemberKind
) {
    void *lpMember;
    PXODLObject lpCurrent;
    const char *szIdent;

    lpCurrent = lpIterator->lpCurrentObject;
next_member:
    if(HashtableIterate(lpCurrent->lpChildren, &szIdent, (void**)&lpMember)) {
        if(lpCurrent->lpChildren != lpIterator->lpObjectMembers) {
            void *p2 = HashtableQuery(lpIterator->lpObjectMembers, szIdent, strlen(szIdent));

            if(p2 != HT_ITEM_NOTFOUND)
                goto next_member;
        }
        *lpeMemberKind = XODLMemberGetKind(lpMember);
        return lpMember;
    } else if(lpCurrent->szInheritObjectPath && lpIterator->bIterateParentObject) {
        if(!lpCurrent->lpOriginObject && !ResolveProxyObject(lpCurrent))
            return NULL;
        lpIterator->lpCurrentObject = lpCurrent->lpOriginObject;
        lpCurrent = lpIterator->lpCurrentObject;
        HashtablePrepareForIteration(lpCurrent->lpChildren);
        goto next_member;
    }

    return NULL;
}

__FORCE_INLINE__ void XODLObjectMemberIteratorDestroy(PXODLObjectMemberIterator lpIterator)
{
    MemoryManagerFree(lpIterator);
}

/*------------------------------------------------------------------------------
  XODL Regular Expression Implementation
------------------------------------------------------------------------------*/

/* INTERNAL API */
PRegularExpression XODLRegExpCreate(PMemoryManager lpMemMgr, const char *lpRegExp, uint8_t nLength)
{
    assert(lpRegExp != NULL);
    PRegularExpression lpRE = MemoryManagerMalloc(lpMemMgr, sizeof(RegularExpression) + nLength + 1);

    strlcpy(lpRE->szExp, lpRegExp, nLength + 1);

    if(lpRegExp[0] != '*' && nLength > 1) {
        lpRE->lpCompiledData = RegularExpressionCompile(lpRE->szExp);
        if(lpRE->lpCompiledData == NULL)
            return NULL;
    } else
        lpRE->lpCompiledData = NULL;

    return lpRE;
}

#if 0
/* INTERNAL API */
bool XODLRegExpCompile(PRegularExpression lpRegExp)
{
    assert(lpRegExp != NULL);

    if(likely(lpRegExp->lpCompiledData == NULL)) {
        lpRegExp->lpCompiledData = RegularExpressionCompile(lpRegExp->szExp);
        if(lpRegExp->lpCompiledData == NULL)
            return false;
    }

    return true;
}
#endif

/* INTERNAL API */
__FORCE_INLINE__ bool XODLRegExpMatch(PRegularExpression lpRegExp, const char *szStr)
{
    if(lpRegExp->lpCompiledData == NULL) {
        assert(!strcmp(lpRegExp->szExp, "*") && "regular expression is not compiled");
        return false;
    }
    return RegularExpressionMatch(lpRegExp->lpCompiledData, szStr);
}

/* INTERNAL API */
void XODLRegExpDestroy(PRegularExpression lpRegExp)
{
    if(likely(lpRegExp->lpCompiledData != NULL))
        RegularExpressionFree(lpRegExp->lpCompiledData);
    MemoryManagerFree(lpRegExp);
}

/*------------------------------------------------------------------------------
  XODL Property Implementation
------------------------------------------------------------------------------*/

__FORCE_INLINE__ const char *XODLPropertyGetIdentifier(PXODLProperty lpProp)
{
    assert(lpProp != NULL);

    if(unlikely(lpProp->dwSignature != XODL_PROPERTY_SIGNATURE))
        return NULL;

    return lpProp->szIdent;
}

PXODLValue XODLPropertyGetValue(
    PXODLProperty           lpProp,
    bool                    bResolveProxy
) {
    assert(lpProp != NULL);
    if(XODLValueGetKind(lpProp->lpValue) != XODL_VALUE_ObjectPath)
        return lpProp->lpValue;
    else if(bResolveProxy) {
        if(lpProp->lpResolvedValue == NULL) {
            EXODLMemberKind eKind;
            void *lpEndPoint = XODLResolveObjectPath(lpProp->lpXODL, lpProp->lpValue->szVal, &eKind);

            if(lpEndPoint == NULL || eKind == XODL_MEMBER_Object)
                return NULL;
            lpProp->lpResolvedValue = AS_PROP(lpEndPoint)->lpValue;
        }
        return lpProp->lpResolvedValue;
    }

    return NULL;
}

/* INTERNAL API */
PXODLProperty XODLPropertyCreateEx(PXODLDocument lpXODL, const char *lpIdent, uint8_t nLength, bool bOptional)
{
    PMemoryManager lpMemMgr = MEMORY_MANAGER(lpXODL);
    PXODLProperty lpProp = MemoryManagerMalloc(lpMemMgr, sizeof(XODLProperty));

    assert(lpIdent && nLength);

    lpProp->dwSignature = XODL_PROPERTY_SIGNATURE;
    lpProp->lpXODL = lpXODL;
    if(lpXODL->eType == XODL_TYPE_Document) {
        lpProp->szIdent = MemoryManagerDuplicateString(lpMemMgr, lpIdent, nLength);
        lpProp->bSchemaProperty = false;
    } else {
        lpProp->lpRegExp = XODLRegExpCreate(lpMemMgr, lpIdent, nLength);
        lpProp->bSchemaProperty = true;
    }
    lpProp->lpValue = NULL;
    lpProp->lpResolvedValue = NULL;
    lpProp->bOptional = bOptional;

    return lpProp;
}

/* INTERNAL API */
__FORCE_INLINE__ void XODLPropertySetValue(PXODLProperty lpProp, PXODLValue lpVal)
{
    lpProp->lpValue = lpVal;
    lpVal->nRefCount++;
}

/*------------------------------------------------------------------------------
  XODL Value Implementation
------------------------------------------------------------------------------*/

PXODLValue XODLValueCreate (
    PXODLDocument           lpXODL,
    EXODLValueKind          eValueKind
) {
    if(unlikely(lpXODL->eType == XODL_TYPE_Document &&
        (IsValueInRange(eValueKind, XODL_SCHEMA_VALUE_START, XODL_SCHEMA_VALUE_END) || eValueKind == XODL_VALUE_RegExp)))
        return NULL;
    else if(unlikely(lpXODL->eType == XODL_TYPE_Schema && !IsValidSchemaValueKind(eValueKind)))
        return NULL;

    PXODLValue lpValue = MemoryManagerMalloc(MEMORY_MANAGER(lpXODL), sizeof(XODLValue));

    lpValue->eKind = eValueKind;
    lpValue->nRefCount = 0;

    if(eValueKind == XODL_VALUE_Nil || IsValueInRange(eValueKind, XODL_SCHEMA_VALUE_START, XODL_SCHEMA_VALUE_END))
        lpValue->bInitialised = true;
    else
        lpValue->bInitialised = false;

    return lpValue;
}

__FORCE_INLINE__ EXODLValueKind XODLValueGetKind(PXODLValue lpXODLValue)
{
    return lpXODLValue->eKind;
}

void XODLValueDestroy(PXODLValue lpXODLValue)
{
    if(--lpXODLValue->nRefCount <= 0) {
        if(lpXODLValue->bInitialised)
            FreeValuePayload(lpXODLValue);
        MemoryManagerFree(lpXODLValue);
    }
}

#define CHECK_VK(exp)       if(unlikely(lpXODLValue->eKind != exp || lpXODLValue->bInitialised)) return false
bool XODLValueSetInteger (
    PXODLValue              lpXODLValue,
    int64_t                 i64Value
) {
    CHECK_VK(XODL_VALUE_Integer);

    lpXODLValue->i64Val = i64Value;
    lpXODLValue->bInitialised = true;

    return true;
}

static void EscapeString(char *dst, const char *src, size_t srclen)
{
    char *p = (void*)src, *p2 = p + srclen;

    for(; p < p2;) {
        if(*p == '\\') {
            switch(*++p) {
                case '\'': *dst++ = '\''; p++; break;
                case '\"': *dst++ = '\"'; p++; break;
                case '\\': *dst++ = '\\'; p++; break;
                case 'b':  *dst++ = '\b'; p++; break;
                case 'f':  *dst++ = '\f'; p++; break;
                case 'n':  *dst++ = '\n'; p++; break;
                case 't':  *dst++ = '\t'; p++; break;
                case 'r':  *dst++ = '\r'; p++; break;
                case 'u':
                case 'U': {
                    uint32_t dwCodePoint = 0;
                    char c, v;
                    char *oldp = p - 1; // pointer to the beginning of this sequence
#define X       c = *p++; \
                v = (c & 0xF) + ((c >> 6) | ((c >> 3) & 0x8)); \
                dwCodePoint = (dwCodePoint << 4) | (uint32_t)v

                    // No need to check if the format of \uXXXX and \UXXXXXXXX is correct here,
                    // this has been done by the lexer
                    if(*p == 'u') {
                        p++; X;X;X;X;
                    } else {
                        p++; X;X;X;X;X;X;X;X;
                    }
#undef X
                    int n = ConvertUCSToUTF8(dwCodePoint, dst);
                    if(likely(n > 0))
                        dst += n;
                    else {
                        // treat the unicode escape sequence as normal string on error...
                        memmove(dst, oldp, p - oldp);
                    }
                }   break;
                default:
                    __UNREACHABLE("invalid escape sequence must've been captured by the lexer");
            }
        } else
            *dst++ = *p++;
    }
    *dst = 0;
}

bool XODLValueSetString (
    PXODLValue              lpXODLValue,
    const char *            lpStr,
    size_t                  ulSize
) {
    assert(ulSize > 0 && lpStr != NULL);
    PMemoryManager lpMemMgr = MemoryManagerGetInstance(lpXODLValue);

    if(unlikely(lpXODLValue->bInitialised))
        return false;

    lpXODLValue->szVal = MemoryManagerMalloc(lpMemMgr, ulSize + 1);

    if(lpXODLValue->eKind == XODL_VALUE_String) {
        EscapeString(lpXODLValue->szVal, lpStr, ulSize);
    } else if(lpXODLValue->eKind == XODL_VALUE_RawString) {
        strlcpy(lpXODLValue->szVal, lpStr, ulSize + 1);
    } else
        return false;

    lpXODLValue->bInitialised = true;

    return true;
}

/* INTERNAL API */
void XODLValueAppendString(PXODLValue lpXODLValue, const char *lpStr, size_t ulSize, bool bRawString)
{
    PMemoryManager lpMemMgr = MemoryManagerGetInstance(lpXODLValue);
    size_t oldsize = strlen(lpXODLValue->szVal);
    size_t newsize = oldsize+ ulSize + 1;

    if(ulSize == 0)
        return;
    lpXODLValue->szVal = MemoryManagerRealloc(lpMemMgr, lpXODLValue->szVal, newsize);
    if(bRawString == false)
        EscapeString(lpXODLValue->szVal + oldsize, lpStr, ulSize);
    else
        strlcpy(lpXODLValue->szVal + oldsize, lpStr, ulSize + 1);
}

/* INTERNAL API */
void XODLValueSetStringEx(PXODLValue lpValue, const char *lpStr, size_t ulSize)
{
    PMemoryManager lpMemMgr = MemoryManagerGetInstance(lpValue);

    lpValue->szVal = MemoryManagerDuplicateString(lpMemMgr, lpStr, ulSize);
    lpValue->bInitialised = true;
}

bool XODLValueSetData (
    PXODLValue              lpXODLValue,
    void *                  lpData,
    size_t                  ulSize
) {
    assert(lpData != NULL && ulSize > 0);

    CHECK_VK(XODL_VALUE_Data);

    PMemoryManager lpMemMgr = MemoryManagerGetInstance(lpXODLValue);
    struct SRawData *lpRawData = MemoryManagerMalloc(lpMemMgr, sizeof(struct SRawData) + ulSize);

    lpRawData->dwSize = ulSize;
    memmove(lpRawData->lpData, lpData, ulSize);
    lpXODLValue->lpRawData = lpRawData;
    lpXODLValue->bInitialised = true;

    return true;
}

bool XODLValueSetReal (
    PXODLValue              lpXODLValue,
    double                  dbValue
) {
    CHECK_VK(XODL_VALUE_Real);

    lpXODLValue->dbVal = dbValue;
    lpXODLValue->bInitialised = true;

    return true;
}

bool XODLValueSetBoolean (
    PXODLValue              lpXODLValue,
    bool                    bValue
) {
    CHECK_VK(XODL_VALUE_Boolean);

    lpXODLValue->i64Val = bValue;
    lpXODLValue->bInitialised = true;

    return true;
}

bool XODLValueSetObjectPath (
    PXODLValue              lpXODLValue,
    const char *            lpObjPath,
    size_t                  ulSize
) {
    CHECK_VK(XODL_VALUE_ObjectPath);
    if(unlikely(lpObjPath == NULL || ulSize == 0))
        return false;

    PMemoryManager lpMemMgr = MemoryManagerGetInstance(lpXODLValue);

    lpXODLValue->szVal = MemoryManagerDuplicateString(lpMemMgr, lpObjPath, ulSize);
    lpXODLValue->bInitialised = true;

    return true;
}

bool XODLValueSetObject (
    PXODLValue              lpXODLValue,
    PXODLObject             lpObject
) {
    CHECK_VK(XODL_VALUE_Object);
    if(unlikely(lpObject == NULL || lpObject->bHasBoundToValue == true))
        return false;

    lpXODLValue->lpObject = lpObject;
    lpObject->bHasBoundToValue = true;
    lpXODLValue->bInitialised = true;

    return true;
}

bool XODLValueSetObjectID (
    PXODLValue              lpXODLValue,
    XODLObjectId            OID
) {
    CHECK_VK(XODL_VALUE_ObjectID);
    if(unlikely(OID == OID_ANON))
        return false;

    lpXODLValue->szVal = MemoryManagerDuplicateString(MemoryManagerGetInstance(lpXODLValue), OID, XODL_OID_LENGTH);
    lpXODLValue->bInitialised = true;

    return true;
}

bool XODLValueSetRegularExpression (
    PXODLValue              lpXODLValue,
    const char *            lpRegExp,
    uint8_t                 nLength
) {
    CHECK_VK(XODL_VALUE_RegExp);
    if(unlikely(lpRegExp == NULL || nLength == 0))
        return false;

    PMemoryManager lpMemMgr = MemoryManagerGetInstance(lpXODLValue);

    lpXODLValue->lpRegExp = XODLRegExpCreate(lpMemMgr, lpRegExp, nLength);
    lpXODLValue->bInitialised = true;

    return true;
}

#define EXPECTED_KIND(v, k) if(unlikely(v->eKind != k || !v->bInitialised)) return false
bool XODLValueGetInteger(PXODLValue lpXODLValue, int64_t *lpValueOut)
{
    assert(lpValueOut != NULL);
    EXPECTED_KIND(lpXODLValue, XODL_VALUE_Integer);
    *lpValueOut = lpXODLValue->i64Val;

    return true;
}

bool XODLValueGetString(PXODLValue lpXODLValue, char **lppValueOut)
{
    assert(lppValueOut != NULL);
    if(unlikely(lpXODLValue->eKind != XODL_VALUE_String && lpXODLValue->eKind != XODL_VALUE_RawString) || !lpXODLValue->bInitialised)
        return false;
    *lppValueOut = lpXODLValue->szVal;

    return true;
}

bool XODLValueGetData(PXODLValue lpXODLValue, void **lppDataPtr, uint32_t *lpdwDataSize)
{
    assert(lppDataPtr != NULL && lpdwDataSize != NULL);
    EXPECTED_KIND(lpXODLValue, XODL_VALUE_Data);

    *lppDataPtr = lpXODLValue->lpRawData->lpData;
    *lpdwDataSize = lpXODLValue->lpRawData->dwSize;

    return true;
}

bool XODLValueGetReal(PXODLValue lpXODLValue, double *lpValueOut)
{
    assert(lpValueOut != NULL);
    EXPECTED_KIND(lpXODLValue, XODL_VALUE_Real);
    *lpValueOut = lpXODLValue->dbVal;

    return true;
}

bool XODLValueGetBoolean(PXODLValue lpXODLValue, bool *lpValueOut)
{
    assert(lpValueOut != NULL);
    EXPECTED_KIND(lpXODLValue, XODL_VALUE_Boolean);
    *lpValueOut = lpXODLValue->i64Val;

    return true;
}

bool XODLValueGetObjectPath (
    PXODLValue              lpXODLValue,
    char **                 lpszObjPath
) {
    assert(lpszObjPath != NULL);
    EXPECTED_KIND(lpXODLValue, XODL_VALUE_ObjectPath);

    *lpszObjPath = lpXODLValue->szVal;

    return true;
}

bool XODLValueGetObject(PXODLValue lpXODLValue, PXODLObject *lppObjectOut)
{
    assert(lppObjectOut != NULL);
    EXPECTED_KIND(lpXODLValue, XODL_VALUE_Object);
    *lppObjectOut = lpXODLValue->lpObject;

    return true;
}

bool XODLValueGetObjectID(PXODLValue lpXODLValue, XODLObjectId *lppOidOut)
{
    assert(lppOidOut != NULL);
    EXPECTED_KIND(lpXODLValue, XODL_VALUE_ObjectID);
    //memmove(OidOut, lpXODLValue->szVal, XODL_OID_LENGTH);
    *lppOidOut = lpXODLValue->szVal;

    return true;
}

bool XODLValueGetRegularExpression (
    PXODLValue              lpXODLValue,
    char **                 lppRegExp
) {
    assert(lppRegExp != NULL);
    EXPECTED_KIND(lpXODLValue, XODL_VALUE_RegExp);

    *lppRegExp = lpXODLValue->lpRegExp->szExp;

    return true;
}

/*------------------------------------------------------------------------------
  XODL Value List Implementation
------------------------------------------------------------------------------*/

bool XODLValueSetList (
    PXODLValue              lpXODLValue,
    uint32_t                dwItemCount
) {
    CHECK_VK(XODL_VALUE_List);

    lpXODLValue->lpList = ValueListCreate(MemoryManagerGetInstance(lpXODLValue), dwItemCount);
    lpXODLValue->bInitialised = true;

    return true;
}

bool XODLValueListAddItem(PXODLValue lpXODLValue, PXODLValue lpItem)
{
    EXPECTED_KIND(lpXODLValue, XODL_VALUE_List);
    ValueListAdd(MemoryManagerGetInstance(lpXODLValue), lpXODLValue->lpList, lpItem);

    return true;
}

bool XODLValueListDeleteItem(PXODLValue lpXODLValue, uint32_t dwIndex)
{
    EXPECTED_KIND(lpXODLValue, XODL_VALUE_List);
    return ValueListDelete(lpXODLValue->lpList, dwIndex);
}

PXODLValue XODLValueListGetItem(PXODLValue lpXODLValue, uint32_t dwIndex)
{
    EXPECTED_KIND(lpXODLValue, XODL_VALUE_List);
    return ValueListGet(lpXODLValue->lpList, dwIndex);
}

uint32_t XODLValueListGetItemCount(PXODLValue lpXODLValue)
{
    if(unlikely(lpXODLValue->eKind != XODL_VALUE_List))
        return 0;
    return lpXODLValue->lpList->dwCountOfItems;
}

bool XODLValueListIteratorInit(PXODLValue lpXODLValue)
{
    EXPECTED_KIND(lpXODLValue, XODL_VALUE_List);
    CleanUpValueList(lpXODLValue->lpList);
    lpXODLValue->lpList->i = 0;

    return true;
}

PXODLValue XODLValueListIteratorNextItem(PXODLValue lpXODLValue)
{
    assert(lpXODLValue->eKind == XODL_VALUE_List);
    PXODLValueList lpList = lpXODLValue->lpList;

    if(lpList->i < lpList->dwCountOfItems)
        return lpList->lppItems[lpList->i++];

    return NULL;
}

/*------------------------------------------------------------------------------
  Private Routines
------------------------------------------------------------------------------*/

static void FreeValuePayload(PXODLValue lpValue)
{
    switch(lpValue->eKind) {
        case XODL_VALUE_String:
        case XODL_VALUE_RawString:
        case XODL_VALUE_ObjectID:
        case XODL_VALUE_ObjectPath:
            MemoryManagerFree(lpValue->szVal);
            lpValue->szVal = NULL;
            break;
        case XODL_VALUE_Data:
            MemoryManagerFree(lpValue->lpRawData->lpData);
            MemoryManagerFree(lpValue->lpRawData);
            lpValue->lpRawData = NULL;
            break;
        case XODL_VALUE_Object:
            FreeObject(lpValue->lpObject);
            lpValue->lpObject = NULL;
            break;
        case XODL_VALUE_List:
            ValueListDestroy(lpValue->lpList);
            lpValue->lpList = NULL;
            break;
        case XODL_VALUE_RegExp:
            XODLRegExpDestroy(lpValue->lpRegExp);
            break;
        default:
            return;
    }
}

static void CleanUpValueList(PXODLValueList lpList)
{
    uint32_t i, j;

    if(lpList->dwIdx == lpList->dwCountOfItems)
        return;

    for(i = 0, j = 0; i < lpList->dwIdx; ) {
        void* t;

        while(i < lpList->dwIdx && lpList->lppItems[i] != &kDeletedValue) i++;
        if(i == lpList->dwIdx)
            break;
        j = i;
        while(j < lpList->dwIdx && lpList->lppItems[j] == &kDeletedValue) j++;
        t = lpList->lppItems[i];
        lpList->lppItems[i++] = lpList->lppItems[j];
        lpList->lppItems[j] = t;
    }
    lpList->dwIdx = lpList->dwCountOfItems;
}

static PXODLValueList ValueListCreate(PMemoryManager lpMemMgr, uint32_t dwCountOfItems)
{
    PXODLValueList lpList = MemoryManagerMalloc(lpMemMgr, sizeof(XODLValueList));

    lpList->dwAllocatedSlots = dwCountOfItems;
    lpList->dwCountOfItems = 0;
    lpList->lppItems = MemoryManagerMalloc(lpMemMgr, dwCountOfItems * sizeof(PXODLValue));
    lpList->i = 0;
    lpList->dwIdx = 0;

    return lpList;
}

static inline void ValueListAdd(PMemoryManager lpMemMgr, PXODLValueList lpList, PXODLValue lpValue)
{
    assert(lpValue != NULL && lpValue != &kDeletedValue);

    if(lpList->dwIdx >= lpList->dwAllocatedSlots) {
        lpList->dwAllocatedSlots = lpList->dwAllocatedSlots ? lpList->dwAllocatedSlots << 1 : 1;
        lpList->lppItems = MemoryManagerRealloc(lpMemMgr, lpList->lppItems, lpList->dwAllocatedSlots * sizeof(PXODLValue));
    }

    lpList->lppItems[lpList->dwIdx++] = lpValue;
    lpList->dwCountOfItems++;
    lpValue->nRefCount++;
}

static bool ValueListDelete(PXODLValueList lpList, uint32_t dwIndex)
{
    if(unlikely(false == IsValueInRangeUL(dwIndex, 0, lpList->dwCountOfItems - 1)))
        return false;

    CleanUpValueList(lpList);

    XODLValueDestroy(lpList->lppItems[dwIndex]);
    lpList->lppItems[dwIndex] = &kDeletedValue;
    lpList->dwCountOfItems--;

    return true;
}

static PXODLValue ValueListGet(PXODLValueList lpList, uint32_t dwIndex)
{
    if(!IsValueInRangeUL(dwIndex, 0, lpList->dwCountOfItems - 1))
        return NULL;

    CleanUpValueList(lpList);

    return lpList->lppItems[dwIndex];
}

static void ValueListDestroy(PXODLValueList lpList)
{
    for(uint32_t i = 0; i < lpList->dwAllocatedSlots; i++) {
        PXODLValue lpItem = lpList->lppItems[i];

        if(lpItem != &kDeletedValue)
            XODLValueDestroy(lpItem);
    }
    if(lpList->lppItems)
        MemoryManagerFree(lpList->lppItems);
    MemoryManagerFree(lpList);
}
