/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#ifndef LIBXODL_MMHASH64A_H
#define LIBXODL_MMHASH64A_H

#include <stdint.h>
#include <stdlib.h>

#ifndef DEFAULT_HASH_SEED
#   define DEFAULT_HASH_SEED       0xabcd1234effa8765ull
#endif

uint64_t MMHash64aHash(const void* lpDataPtr, size_t ulDataSize, const uint64_t qwSeed);

#endif /* LIBXODL_HASHTABLE_H */
