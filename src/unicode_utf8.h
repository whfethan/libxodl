/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#ifndef LIBXODL_UNICODE_UTF8_H
#define LIBXODL_UNICODE_UTF8_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

int     ConvertUCSToUTF8(uint32_t dwCodePoint, char *lpOutBuffer);
uint8_t ConvertUTF8ToUCS(const uint8_t *lpData, size_t ulSize, uint32_t *lpdwCodePoint);

#endif /* LIBXODL_UNICODE_UTF8_H */
