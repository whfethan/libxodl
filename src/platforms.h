/*>>===========================================================================
  Copyright (C) 2015-2020 Ethan Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/
#ifndef LIBXODL_PLATFORMS_H
#define LIBXODL_PLATFORMS_H

/* Microsoft Windows */
#ifdef _MSC_VER
#   define PLATFORM_WINDOWS     1
/* POSIX/UNIX[-like]systems */
#elif (defined(__linux__) && !defined(__ANDROID__)) || \
      defined(__APPLE__) || \
      (defined(__FreeBSD__) && !defined(__GLIBC__)) || defined(__NetBSD__) || defined(__OpenBSD__) || defined(__bsdi__) || defined(__DragonFly__) || \
      defined(__minix) || (defined(__sun) && defined(__SVR4)) || \
      defined(_AIX) && !defined(__PASE__)
#   define PLATFORM_POSIX       1
#else
#   error Unsupported platform
#endif

#endif /* LIBXODL_PLATFORMS_H */
