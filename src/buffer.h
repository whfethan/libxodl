/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#ifndef LIBXODL_BUFFER_H
#define LIBXODL_BUFFER_H

#include <stdint.h>

typedef struct {
    uint32_t    dwAllocated;
    uint32_t    i;
    char *      lpData;
} Buffer, *PBuffer;

void    BufferInit(PBuffer lpBuffer);
int     BufferWrite(void *lpBuffer, const char *lpData, int nLength);
int     BufferClose(void *lpBuffer);
uint32_t BufferGetPosition(PBuffer lpBuffer);
void    BufferClear(PBuffer lpBuffer);
void    BufferFree(PBuffer lpBuffer);

#endif /* LIBXODL_BUFFER_H */
