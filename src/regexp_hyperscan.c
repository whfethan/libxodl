/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#include <string.h>

#include "hs/hs.h"
#include "cmacros.h"
#include "regexp_interface.h"

void *RegularExpressionCompile (
    const char *        szRegExpr
) {
    hs_database_t *db;
    hs_compile_error_t *hs_error;

    if(HS_SUCCESS != hs_compile(szRegExpr, HS_FLAG_UTF8, HS_MODE_BLOCK, NULL, &db, &hs_error)) {
        hs_free_compile_error(hs_error);
        return NULL;
    }

    return db;
}

static int EventHandler(unsigned int id, unsigned long long from, unsigned long long to, unsigned int flags, void *context)
{
    (void)id;
    (void)from;
    (void)to;
    (void)flags;
    *(bool*)context = true;

    return 0;
}

bool RegularExpressionMatch (
    void *              lpCompiledRegExp,
    const char *        szString
) {
    hs_scratch_t *scratch = NULL;
    bool res = false;

    if(unlikely(HS_SUCCESS != hs_alloc_scratch(lpCompiledRegExp, &scratch)))
        return false;
    if(HS_SUCCESS != hs_scan(lpCompiledRegExp, szString, strlen(szString), 0, scratch, EventHandler, &res))
        return false;
    hs_free_scratch(scratch);

    return res;
}

void RegularExpressionFree(void *lpCompiledRegExp)
{
    hs_free_database(lpCompiledRegExp);
}
