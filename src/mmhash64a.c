/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#include <string.h>
#include "mmhash64a.h"

/* No magic here, these two values just happen to work well */
#ifndef DEFAULT_HASH_RVALUE
#   define DEFAULT_HASH_RVALUE     32
#endif
#ifndef DEFAULT_HASH_MVALUE
#   define DEFAULT_HASH_MVALUE     0xc6a4a7935bd1e995ull
#endif

uint64_t MMHash64aHash(const void* lpDataPtr, size_t ulDataSize, const uint64_t qwSeed)
{
    const uint64_t m = DEFAULT_HASH_MVALUE;
    uint64_t h, k, n;
    const uint8_t *p, *end;
    int r = DEFAULT_HASH_RVALUE;

    h = qwSeed ^ m;
    n = ulDataSize & ~0x7ull;
    end = lpDataPtr;
    end += n;
    for (p = lpDataPtr; p != end; p += 8) {
        memcpy(&k, p, sizeof(k));

        k *= m;
        k ^= k >> r;
        k *= m;

        h ^= k;
        h *= m;
    }

    switch (ulDataSize & 0x7) {
        case 7: h ^= (uint64_t)p[6] << 48;  /* fallthrough */
        case 6: h ^= (uint64_t)p[5] << 40;  /* fallthrough */
        case 5: h ^= (uint64_t)p[4] << 32;  /* fallthrough */
        case 4: h ^= (uint64_t)p[3] << 24;  /* fallthrough */
        case 3: h ^= (uint64_t)p[2] << 16;  /* fallthrough */
        case 2: h ^= (uint64_t)p[1] <<  8;  /* fallthrough */
        case 1: h ^= (uint64_t)p[0];
        h *= m;
    }

    k = ulDataSize;
    k *= m;
    k ^= k >> r;
    k *= m;
    h ^= k;

    h ^= h >> r;
    h *= m;
    h ^= h >> r;

    return h;
}
