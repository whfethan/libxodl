/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

/*-T-T--------------------------------------------------------------------------
  NEEDS:   src/mmhash64a.c
  NEEDS:   src/memmgr.c
--------------------------------------------------------------------------T-T-*/

#include <assert.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <stddef.h>
#include <stdlib.h>

#include "cmacros.h"
#include "hashtable.h"
#include "mmhash64a.h"
#include "memmgr.h"

typedef struct {
    char            *szKey;
    void            *lpDataPtr;
    uint64_t        qwHash;
    uint32_t        dwKeySize;
    uint32_t        dwNextSlot;
} Bucket, *PBucket;

struct SHashtableImpl {
    PBucket         arrBuckets;
    uint32_t        dwCountOfItems;
    uint32_t        dwUsedBuckets;
    uint32_t        dwAllocatedBuckets;
    uint32_t        bCloneKeyOnInsert : 1;
    uint32_t        bManagedByMemoryManager : 1;
    FStandardDataDestructor fnFree;
    uint32_t        dwIterPos;
};

#define HT_INVALID_IDX                  UINT32_MAX
#define HT_MAX_MEMSIZE                  0x80000000
#define HT_MAX_CAPACITY                 (HT_MAX_MEMSIZE / sizeof(Bucket))
#define HT_MAX_KEYSIZE                  (UINT32_MAX >> 1)

#define MBucketsSize(cap)               ((cap) * sizeof(Bucket))
#define MIndexTableSize(cap)            ((cap) * sizeof(uint32_t))
#define MHashTableSize(cap)             (MBucketsSize(cap) + MIndexTableSize(cap))
#define MIndexTable(ht)                 (uint32_t*)((char*)(ht)->arrBuckets)
#define MIIDX2IDX(ht, iidx)             (MIndexTable(ht))[(int32_t)iidx]
#define MBucketFromIDX(ht, idx)         (ht)->arrBuckets + idx
#define MGetMemoryHandle(ht)            (void*)((ptrdiff_t)ht->arrBuckets - MIndexTableSize(ht->dwAllocatedBuckets))

/* forward prototypes */
static void HashtableRehash(PHashtable lpHashtable);
static void HashtableGrow(PHashtable lpHashtable);
static PBucket HashtableQueryBucket(PHashtable lpHashtable, const char *lpKey, uint32_t dwKeySize);

static const char *kDeletedKey = "DELETED";

PHashtable HashtableCreate(PMemoryManager lpOptMemMgr, uint32_t dwSize, FStandardDataDestructor fnFree, bool bCloneKeyOnInsert)
{
    PHashtable lpHashtable;
    void *ptr;

    assert(dwSize > 0);

    if(lpOptMemMgr) {
        lpHashtable = MemoryManagerMalloc(lpOptMemMgr, sizeof(Hashtable));
        ptr = MemoryManagerMalloc(lpOptMemMgr, MHashTableSize(dwSize));
        lpHashtable->bManagedByMemoryManager = true;
    } else {
        lpHashtable = malloc(sizeof(Hashtable));
        ptr = malloc(MHashTableSize(dwSize));
        lpHashtable->bManagedByMemoryManager = false;
    }

    lpHashtable->dwAllocatedBuckets = dwSize;
    lpHashtable->dwUsedBuckets = 0;
    lpHashtable->dwCountOfItems = 0;
    lpHashtable->arrBuckets = (PBucket)((ptrdiff_t)ptr + MIndexTableSize(dwSize));
    lpHashtable->bCloneKeyOnInsert = bCloneKeyOnInsert;
    lpHashtable->fnFree = fnFree;
    lpHashtable->dwIterPos = 0;

    /* reset the index table */
    memset(ptr, HT_INVALID_IDX, MIndexTableSize(dwSize));

    return lpHashtable;
}

void** HashtableInsert(PHashtable lpHashtable, const char *lpKey, uint32_t dwKeySize)
{
    uint64_t qwKeyHash;
    uint32_t dwIndexTableIndex, dwIndex;
    PBucket lpBucket;

    assert(dwKeySize < HT_MAX_KEYSIZE && "abnormal key size!!");

    qwKeyHash = MMHash64aHash(lpKey, dwKeySize, DEFAULT_HASH_SEED);
    dwIndexTableIndex = qwKeyHash | -lpHashtable->dwAllocatedBuckets;
    dwIndex = MIIDX2IDX(lpHashtable, dwIndexTableIndex);

    while(dwIndex != HT_INVALID_IDX) {
        PBucket lpBucket = MBucketFromIDX(lpHashtable, dwIndex);

        if(lpBucket->szKey != kDeletedKey && lpBucket->dwKeySize == dwKeySize &&
            (lpBucket->szKey == lpKey || !strncmp(lpBucket->szKey, lpKey, dwKeySize)))
            return &lpBucket->lpDataPtr;
        dwIndex = lpBucket->dwNextSlot;
    }

    if(unlikely(lpHashtable->dwAllocatedBuckets >= HT_MAX_CAPACITY && lpHashtable->dwCountOfItems >= HT_MAX_CAPACITY))
        // No more free space for the new item
        return NULL;
    if(lpHashtable->dwUsedBuckets + 1 > lpHashtable->dwAllocatedBuckets) {
        HashtableGrow(lpHashtable);
        dwIndexTableIndex = qwKeyHash | -lpHashtable->dwAllocatedBuckets;
    }

    dwIndex = lpHashtable->dwUsedBuckets++;
    lpHashtable->dwCountOfItems++;
    lpBucket = MBucketFromIDX(lpHashtable, dwIndex);
    if(lpHashtable->bCloneKeyOnInsert) {
        lpBucket->szKey = malloc(dwKeySize + 1);
        memmove(lpBucket->szKey, lpKey, dwKeySize);
        lpBucket->szKey[dwKeySize] = 0;
    } else
        lpBucket->szKey = (void*)lpKey;
    lpBucket->dwKeySize = dwKeySize;
    lpBucket->qwHash = qwKeyHash;
    lpBucket->lpDataPtr = NULL;
    lpBucket->dwNextSlot = MIIDX2IDX(lpHashtable, dwIndexTableIndex);
    MIIDX2IDX(lpHashtable, dwIndexTableIndex) = dwIndex;

    return &lpBucket->lpDataPtr;
}

void* HashtableQuery(PHashtable lpHashtable, const char *lpKey, uint32_t dwKeySize)
{
    PBucket lpBucket = HashtableQueryBucket(lpHashtable, lpKey, dwKeySize);

    return lpBucket ? lpBucket->lpDataPtr : HT_ITEM_NOTFOUND;
}

bool HashtableDelete(PHashtable lpHashtable, const char *lpKey, uint32_t dwKeySize)
{
    PBucket lpBucket = HashtableQueryBucket(lpHashtable, lpKey, dwKeySize);

    if(lpBucket) {
        lpBucket->szKey = (void*)kDeletedKey;
        if(lpHashtable->fnFree && lpBucket->lpDataPtr)
            lpHashtable->fnFree(lpBucket->lpDataPtr);
        lpHashtable->dwCountOfItems--;
        return true;
    }
    return false;
}

bool HashtableMarkItemAsDeleted(PHashtable lpHashtable, const char *lpKey, uint32_t dwKeySize)
{
    PBucket lpBucket = HashtableQueryBucket(lpHashtable, lpKey, dwKeySize);

    if(lpBucket) {
        lpBucket->szKey = (void*)kDeletedKey;
        lpHashtable->dwCountOfItems--;
        return true;
    }
    return false;
}

void HashtableClear(PHashtable lpHashtable)
{
    memset(MGetMemoryHandle(lpHashtable), HT_INVALID_IDX, MIndexTableSize(lpHashtable->dwAllocatedBuckets));
    if(lpHashtable->bCloneKeyOnInsert || lpHashtable->fnFree) {
        PBucket arrBuckets = lpHashtable->arrBuckets;
        bool bDeallocKey = lpHashtable->bCloneKeyOnInsert;

        for(uint32_t i = 0; i < lpHashtable->dwUsedBuckets; i++) {
            PBucket lpBucket = &arrBuckets[i];

            if(lpBucket->szKey != kDeletedKey) {
                if(bDeallocKey) {
                    free(lpBucket->szKey);
                    lpBucket->szKey = NULL;
                    lpBucket->dwKeySize = 0;
                }
                if(lpHashtable->fnFree && lpBucket->lpDataPtr)
                    lpHashtable->fnFree(lpBucket->lpDataPtr);
                lpBucket->lpDataPtr = NULL;
            }
        }
    }
    lpHashtable->dwCountOfItems = 0;
    lpHashtable->dwUsedBuckets = 0;
    lpHashtable->dwIterPos = 0;
}

inline uint32_t HashtableGetItemCount(PHashtable lpHashtable)
{
    return lpHashtable->dwCountOfItems;
}

void HashtableDestroy(PHashtable lpHashtable)
{
    void* mem;

    HashtableClear(lpHashtable);
    mem = MGetMemoryHandle(lpHashtable);
    if(lpHashtable->bManagedByMemoryManager) {
        MemoryManagerFree(mem);
        MemoryManagerFree(lpHashtable);
    } else {
        free(mem);
        free(lpHashtable);
    }
}

bool HashtablePrepareForIteration(PHashtable lpHashtable)
{
    lpHashtable->dwIterPos = 0;
    return true;
}

bool HashtableIterate(PHashtable lpHashtable, const char **lppItemKey, void **lppItemDataPtr)
{
    if(lpHashtable->dwIterPos < lpHashtable->dwUsedBuckets) {
        PBucket arrBuckets = lpHashtable->arrBuckets;
        PBucket lpBucket = &arrBuckets[lpHashtable->dwIterPos++];

        for(; lpBucket->szKey == (void*)kDeletedKey && lpHashtable->dwIterPos < lpHashtable->dwUsedBuckets;
            lpBucket = &arrBuckets[lpHashtable->dwIterPos++]);

        if(lpBucket->szKey != (void*)kDeletedKey) {
            if(lppItemKey)
                *lppItemKey = lpBucket->szKey;
            if(lppItemDataPtr)
                *lppItemDataPtr = lpBucket->lpDataPtr;
            return true;
        }
    }

    if(lppItemKey) *lppItemKey = NULL;
    if(lppItemDataPtr) *lppItemDataPtr = NULL;

    return false;
}

static inline PBucket HashtableQueryBucket(PHashtable lpHashtable, const char *lpKey, uint32_t dwKeySize)
{
    uint64_t qwKeyHash;
    uint32_t dwIndexTableIndex, dwIndex;

    assert(dwKeySize < HT_MAX_KEYSIZE && "abnormal key size!!");

    qwKeyHash = MMHash64aHash(lpKey, dwKeySize, DEFAULT_HASH_SEED);
    dwIndexTableIndex = qwKeyHash | -lpHashtable->dwAllocatedBuckets;
    dwIndex = MIIDX2IDX(lpHashtable, dwIndexTableIndex);

    while(dwIndex != HT_INVALID_IDX) {
        PBucket lpBucket = MBucketFromIDX(lpHashtable, dwIndex);

        if(lpBucket->szKey != kDeletedKey && lpBucket->dwKeySize == dwKeySize &&
            (lpBucket->szKey == lpKey || !strncmp(lpBucket->szKey, lpKey, dwKeySize)))
            return lpBucket;
        dwIndex = lpBucket->dwNextSlot;
    }

    return NULL;
}

static void HashtableGrow(PHashtable lpHashtable)
{
    void* oldHandle, *newHandle;
    PBucket oldBuckets;
    uint32_t newCapacity = 0;

    oldHandle = MGetMemoryHandle(lpHashtable);
    oldBuckets = lpHashtable->arrBuckets;

    if(unlikely(lpHashtable->dwAllocatedBuckets == HT_MAX_CAPACITY)) {
        // compress the hashtable
        newHandle = lpHashtable->bManagedByMemoryManager ?
                        MemoryManagerMalloc(MemoryManagerGetInstance(lpHashtable), MHashTableSize(HT_MAX_CAPACITY)) :
                        malloc(MHashTableSize(HT_MAX_CAPACITY));
        lpHashtable->arrBuckets = (PBucket)((ptrdiff_t)newHandle + MIndexTableSize(HT_MAX_CAPACITY));
        lpHashtable->dwUsedBuckets = lpHashtable->dwCountOfItems;

        for(uint32_t i = 0, j = 0; i < HT_MAX_CAPACITY; i++) {
            PBucket lpOldBucket = &oldBuckets[i];

            if(lpOldBucket->szKey == kDeletedKey)
                memmove(&lpHashtable->arrBuckets[j++], lpOldBucket, sizeof(Bucket));
        }
    } else {
        newCapacity = MIN(lpHashtable->dwAllocatedBuckets << 1, HT_MAX_CAPACITY);
        newHandle = lpHashtable->bManagedByMemoryManager ?
                        MemoryManagerMalloc(MemoryManagerGetInstance(lpHashtable), MHashTableSize(newCapacity)) :
                        malloc(MHashTableSize(newCapacity));
        lpHashtable->dwAllocatedBuckets = newCapacity;
        lpHashtable->arrBuckets = (PBucket)((ptrdiff_t)newHandle + MIndexTableSize(newCapacity));
        memmove(lpHashtable->arrBuckets, oldBuckets, MBucketsSize(lpHashtable->dwUsedBuckets));
    }

    if(lpHashtable->bManagedByMemoryManager)
        MemoryManagerFree(oldHandle);
    else
        free(oldHandle);
    memset(newHandle, HT_INVALID_IDX, MIndexTableSize(newCapacity));
    HashtableRehash(lpHashtable);
}

static void HashtableRehash(PHashtable lpHashtable)
{
    PBucket lpBucket = lpHashtable->arrBuckets;

    for(uint32_t i = 0; i < lpHashtable->dwUsedBuckets; i++, lpBucket++) {
        uint32_t iidx = lpBucket->qwHash | -lpHashtable->dwAllocatedBuckets;

        lpBucket->dwNextSlot = MIIDX2IDX(lpHashtable, iidx);
        MIIDX2IDX(lpHashtable, iidx) = i;
    }
}

