/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#include <assert.h>
#include <stdint.h>
#include <string.h>

#include "xodl.h"
#include "cmacros.h"
#include "delegate.h"
#include "document.h"
#include "validate.h"
#include "hashtable.h"

typedef struct {
    void *              lpSchemaMember;
    bool                bConstraintSatisfied : 1;
} SchemaConstraint, *PSchemaConstraint;

/* forward prototypes */
static PHashtable CreateMandatoryConstraintTable(PXODLObject lpSchemaObject);
static PSchemaConstraint AddMandatoryConstraint(PHashtable lphtConstraints, void *lpSchemaMember);
static void SetMandatoryConstraintSatisfied(PHashtable lphtConstraints, void *lpSchemaMember);
static void *MatchSchemaMember(void *lpMember, PXODLObject lpSchemaObject, bool *lpbIsWildcard);
static bool ValidateValue(PXODLUserDelegate lpDelegate, const char *szFileName, PXODLValue lpValue, PXODLValue lpSchemaValue);
static bool ValidateObject(PXODLUserDelegate lpDelegate, const char *szFileName, PXODLObject lpObject, PXODLObject lpSchemaObject);

bool ValidateDocument (
    PXODLUserDelegate   lpDelegate,
    const char *        szFileName,
    PXODLDocument       lpXODL,
    PXODLDocument       lpXODLSchema
) {
    assert(lpXODL->eType == XODL_TYPE_Document && lpXODLSchema->eType == XODL_TYPE_Schema);
    assert(lpXODL->lpRootObject && lpXODLSchema->lpRootObject);

    bool bResult;

    if(XODLObjectGetMemberCount(lpXODLSchema->lpRootObject) == 0)
        // an empty schema means no restrictions on XODL document
        return true;

    bResult = ValidateObject(lpDelegate, szFileName, lpXODL->lpRootObject, lpXODLSchema->lpRootObject);

    return bResult;
}

static inline bool IsMemberOptional(void *lpMember)
{
    switch(XODLMemberGetKind(lpMember)) {
        case XODL_MEMBER_Object:
            return AS_OBJECT(lpMember)->bSchemaOptionalObject;
        case XODL_MEMBER_Property:
            return AS_PROP(lpMember)->bOptional;
    }
}

static bool ValidateObject(PXODLUserDelegate lpDelegate, const char *szFileName, PXODLObject lpObject, PXODLObject lpSchemaObject)
{
    void *lpMember;
    EXODLMemberKind eMemberKind;
    PHashtable lphtConstraints;
    PXODLObjectMemberIterator lpIterator;

    if(!XODLObjectGetMemberCount(lpSchemaObject))
        return true;

    lphtConstraints = CreateMandatoryConstraintTable(lpSchemaObject);

    lpIterator = XODLObjectMemberIteratorCreate(lpObject, true);
    while((lpMember = XODLObjectMemberIteratorNext(lpIterator, &eMemberKind))) {
        bool bIsWildcard;
        void *lpSchemaMember = MatchSchemaMember(lpMember, lpSchemaObject, &bIsWildcard);

        if(lpSchemaMember == NULL) {
            DelegateWriteObjectInfo(lpDelegate, lpObject);
            switch(eMemberKind) {
                case XODL_MEMBER_Property:
                    DelegatePrintf(lpDelegate, ": unexpected property \"%s\"", XODLPropertyGetIdentifier(lpMember));
                    break;
                case XODL_MEMBER_Object:
                    DelegatePrintf(lpDelegate, ": unexpected object ");
                    DelegateWriteObjectInfo(lpDelegate, lpMember);
                    break;
            }
            DelegateCallMessageHandler(lpDelegate, szFileName, MESSAGE_Error);
            goto obj_validate_failed;
        }

        switch(eMemberKind) {
            case XODL_MEMBER_Object:
                if(AS_OBJECT(lpMember)->szIdent[0] == ANON_OBJECT_IDENT_PREFIX)
                    // XXX: unnamed objects exempt from validation.
                    continue;
                if(false == ValidateObject(lpDelegate, szFileName, lpMember, lpSchemaMember))
                    goto obj_validate_failed;
                break;
            case XODL_MEMBER_Property:
                if(false == ValidateValue(lpDelegate, szFileName, AS_PROP(lpMember)->lpValue, AS_PROP(lpSchemaMember)->lpValue))
                    goto obj_validate_failed;
                break;
        }
        if(lphtConstraints && !bIsWildcard && !IsMemberOptional(lpSchemaMember))
            SetMandatoryConstraintSatisfied(lphtConstraints, lpSchemaMember);
    }

    if(lphtConstraints && HashtableGetItemCount(lphtConstraints)) {
        bool bHasUnsatisfiedConstraints = false;

        HashtablePrepareForIteration(lphtConstraints);
        for(;;) {
            PSchemaConstraint lpConstraint;

            if(!HashtableIterate(lphtConstraints, NULL, (void**)&lpConstraint))
                break;
            if(lpConstraint->bConstraintSatisfied == false) {
                bHasUnsatisfiedConstraints = true;
                DelegateWriteObjectInfo(lpDelegate, lpObject);
                switch(XODLMemberGetKind(lpConstraint->lpSchemaMember)) {
                    case XODL_MEMBER_Object:
                        DelegatePrintf(lpDelegate, ": missing object that matches rule \"%s\"",
                            AS_OBJECT(lpConstraint->lpSchemaMember)->lpRegExp->szExp);
                        break;
                    case XODL_MEMBER_Property:
                        DelegatePrintf(lpDelegate, ": missing property that matches \"%s\"",
                            AS_PROP(lpConstraint->lpSchemaMember)->lpRegExp->szExp);
                        break;
                }
                DelegateCallMessageHandler(lpDelegate, szFileName, MESSAGE_Error);
            }
        }

        if(bHasUnsatisfiedConstraints)
            goto obj_validate_failed;
    }
    HashtableDestroy(lphtConstraints);
    XODLObjectMemberIteratorDestroy(lpIterator);

    return true;

obj_validate_failed:
    if(lphtConstraints)
        HashtableDestroy(lphtConstraints);
    XODLObjectMemberIteratorDestroy(lpIterator);

    return false;
}

static bool ValidateValue(PXODLUserDelegate lpDelegate, const char *szFileName, PXODLValue lpValue, PXODLValue lpSchemaValue)
{
    switch(lpSchemaValue->eKind) {
        case XODL_VALUE_TYPE_NAME_ANYTYPE:
            return true;

        case XODL_VALUE_TYPE_NAME_INTEGER:
            return lpValue->eKind == XODL_VALUE_Integer;

        case XODL_VALUE_TYPE_NAME_BOOLEAN:
            return lpValue->eKind == XODL_VALUE_Boolean;

        case XODL_VALUE_TYPE_NAME_REAL:
            return lpValue->eKind == XODL_VALUE_Real;

        case XODL_VALUE_TYPE_NAME_STRING:
            return (lpValue->eKind == XODL_VALUE_String || lpValue->eKind == XODL_VALUE_RawString);

        case XODL_VALUE_TYPE_NAME_OID:
            return (lpValue->eKind == XODL_VALUE_ObjectID);

        case XODL_VALUE_TYPE_NAME_OBJPATH:
            return (lpValue->eKind == XODL_VALUE_ObjectPath);

        case XODL_VALUE_TYPE_NAME_LIST:
            return (lpValue->eKind == XODL_VALUE_List);

        case XODL_VALUE_List: {
            XODLValueListIteratorInit(lpSchemaValue);
            for(PXODLValue lpSchemaItem = XODLValueListIteratorNextItem(lpSchemaValue); lpSchemaItem;
                lpSchemaItem = XODLValueListIteratorNextItem(lpSchemaValue)) {
                if(ValidateValue(lpDelegate, szFileName, lpValue, lpSchemaItem))
                    return true;
            }
            return false;
        }

        case XODL_VALUE_RegExp:
            if((lpValue->eKind == XODL_VALUE_String || lpValue->eKind == XODL_VALUE_RawString) &&
                XODLRegExpMatch(lpSchemaValue->lpRegExp, lpValue->szVal))
                return true;
            return false;

        case XODL_VALUE_Object:
            if(lpValue->eKind != XODL_VALUE_Object)
                return false;
            return ValidateObject(lpDelegate, szFileName, lpValue->lpObject, lpSchemaValue->lpObject);

        default:
            __UNREACHABLE("bug: unexpected schema value kind");
    }
}

static void *MatchSchemaMember(void *lpMember, PXODLObject lpSchemaObject, bool *lpbIsWildcard)
{
    void *lpSchemaObjectMember, *lpWildcardSchemaMember = NULL;
    EXODLMemberKind eObjectMemberKind = XODLMemberGetKind(lpMember);
    EXODLMemberKind eSchemaObjectMemberKind;

    PXODLObjectMemberIterator lpIterator = XODLObjectMemberIteratorCreate(lpSchemaObject, true);
    while((lpSchemaObjectMember = XODLObjectMemberIteratorNext(lpIterator, &eSchemaObjectMemberKind))) {
        if(eSchemaObjectMemberKind == eObjectMemberKind) {
            switch(eObjectMemberKind) {
                case XODL_MEMBER_Object:
                    assert(AS_OBJECT(lpMember)->szIdent && AS_OBJECT(lpSchemaObjectMember)->lpRegExp);

                    if(XODLRegExpMatch(AS_OBJECT(lpSchemaObjectMember)->lpRegExp, AS_OBJECT(lpMember)->szIdent))
                        return lpSchemaObjectMember;

                    if(!strcmp(AS_OBJECT(lpSchemaObjectMember)->lpRegExp->szExp, "*"))
                        lpWildcardSchemaMember = lpSchemaObjectMember;
                    break;

                case XODL_MEMBER_Property:
                    if(XODLRegExpMatch(AS_PROP(lpSchemaObjectMember)->lpRegExp, AS_PROP(lpMember)->szIdent))
                        return lpSchemaObjectMember;

                    if(!strcmp(AS_PROP(lpSchemaObjectMember)->lpRegExp->szExp, "*"))
                        lpWildcardSchemaMember = lpSchemaObjectMember;
                    break;
            }
        }
    }
    *lpbIsWildcard = lpWildcardSchemaMember != NULL;

    return lpWildcardSchemaMember;
}

static __FORCE_INLINE__ void SetMandatoryConstraintSatisfied(PHashtable lphtConstraints, void *lpMemberOrPropIdent)
{
    PSchemaConstraint lpConstraint = HashtableQuery(lphtConstraints, (char*)&lpMemberOrPropIdent, sizeof(void*));

    assert(lpConstraint != HT_ITEM_NOTFOUND);
    lpConstraint->bConstraintSatisfied = true;
}

static __FORCE_INLINE__ PSchemaConstraint AddMandatoryConstraint(PHashtable lphtConstraints, void *lpSchemaMember)
{
    void **lppConstraint;

    lppConstraint = HashtableInsert(lphtConstraints, (char*)&lpSchemaMember, sizeof(void*));
    if(!*lppConstraint) {
        PSchemaConstraint lpConstraint = malloc(sizeof(SchemaConstraint));

        lpConstraint->bConstraintSatisfied = false;
        lpConstraint->lpSchemaMember = lpSchemaMember;
        *lppConstraint = lpConstraint;
    }

    return *lppConstraint;
}

static __FORCE_INLINE__ PHashtable CreateMandatoryConstraintTable(PXODLObject lpSchemaObject)
{
    if(XODLObjectGetMemberCount(lpSchemaObject)) {
        void *lpMember;
        EXODLMemberKind eKind;
        PHashtable lphtConstraints = HashtableCreate(NULL, XODLObjectGetMemberCount(lpSchemaObject), free, false);

        PXODLObjectMemberIterator lpIterator = XODLObjectMemberIteratorCreate(lpSchemaObject, false);
        while((lpMember = XODLObjectMemberIteratorNext(lpIterator, &eKind))) {
            switch(eKind) {
                case XODL_MEMBER_Property:
                    if(AS_PROP(lpMember)->lpRegExp->lpCompiledData == NULL)
                        continue;
                    if(AS_PROP(lpMember)->bOptional == false)
                        AddMandatoryConstraint(lphtConstraints, lpMember);
                    break;
                case XODL_MEMBER_Object:
                    if(AS_OBJECT(lpMember)->lpRegExp->lpCompiledData == NULL)
                        continue;
                    if(AS_OBJECT(lpMember)->bSchemaOptionalObject == false)
                        AddMandatoryConstraint(lphtConstraints, lpMember);
                    break;
            }
        }
        XODLObjectMemberIteratorDestroy(lpIterator);

        return lphtConstraints;
    }

    return NULL;
}
