/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "buffer.h"
#include "cmacros.h"

#ifndef DEFAULT_BUFFER_SIZE
#   define DEFAULT_BUFFER_SIZE      128
#endif

static void BufferGrow(PBuffer lpBuffer, int nIncrement);

void BufferInit(PBuffer lpBuffer)
{
    lpBuffer->lpData = malloc(DEFAULT_BUFFER_SIZE);
    lpBuffer->dwAllocated = DEFAULT_BUFFER_SIZE;
    lpBuffer->i = 0;
}

int BufferWrite(void *lpBuffer, const char *lpData, int nLength)
{
    PBuffer B = lpBuffer;

    BufferGrow(B, nLength);
    memmove(B->lpData + B->i, lpData, nLength);
    B->i += nLength;

    return nLength;
}

int BufferClose(void *lpBuffer)
{
    PBuffer B = lpBuffer;

    BufferGrow(B, 1);
    B->lpData[B->i] = 0;

    return 0;
}

__FORCE_INLINE__ uint32_t BufferGetPosition(PBuffer lpBuffer)
{
    return lpBuffer->i;
}

void BufferClear(PBuffer lpBuffer)
{
    lpBuffer->i = 0;
    lpBuffer->lpData[0] = 0;
}

void BufferFree(PBuffer lpBuffer)
{
    free(lpBuffer->lpData);
}

static __FORCE_INLINE__ void BufferGrow(PBuffer lpBuffer, int nIncrement)
{
    if(lpBuffer->i + nIncrement > lpBuffer->dwAllocated) {
        lpBuffer->dwAllocated += nIncrement;
        lpBuffer->lpData = realloc(lpBuffer->lpData, lpBuffer->dwAllocated);
    }
}
