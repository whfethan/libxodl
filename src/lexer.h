/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#ifndef LIBXODL_LEXER_H
#define LIBXODL_LEXER_H

#include <stdint.h>

#include "xodl.h"
#include "token.h"
#include "delegate.h"

typedef struct {
    const char *        lpStart;
    ETokenKind          eKind;
    uint32_t            dwLength;
    uint32_t            dwLine;
    uint32_t            dwCol;
} Token, *PToken;

typedef struct {
    const char *        szFileName;
    PXODLUserDelegate   lpDelegate;
    const char *        curp;
    uint32_t            dwCurrentLine;
    uint32_t            dwCurrentCol;
    bool                bErrorOccurred;
} LexerState, *PLexerState;

void LexerStateInit (
    PLexerState         lpLexerState,
    PXODLUserDelegate   lpDelegate,
    const char *        szFileName,
    const char *        szContent
);
bool LexerReadToken (
    PLexerState         lpLexerState,
    PToken              lpToken
);
ETokenKind LexerPeek (
    PLexerState         pLexerState,
    PToken              lpTokenOut,
    uint8_t             nCount
);

#endif /* LIBXODL_LEXER_H */
