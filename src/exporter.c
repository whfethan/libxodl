/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

/*-T-T--------------------------------------------------------------------------
  NEEDS:    src/base64.c
  NEEDS:    src/document.c
  NEEDS:    src/unicode_utf8.c
  NEEDS:    src/xodb.c
--------------------------------------------------------------------------T-T-*/

#include <assert.h>
#include <string.h>
#include <inttypes.h>

#include "base64.h"
#include "cmacros.h"
#include "document.h"
#include "unicode_utf8.h"
#include "xodb.h"
#include "xodl.h"

/* forward prototypes */
static void WriteIndents(FILE *lpStream, PXODLExportSettings lpExportSettings, int nIndentCount);
static void ExportObject(PXODLObject lpObject, PXODLExportSettings lpExportSettings, FILE *lpStream, int nDepth);
static void ExportProperty(PXODLProperty lpProp, PXODLExportSettings lpExportSettings, FILE *lpStream, int nIndent);
static void ExportValue(PXODLValue lpValue, PXODLExportSettings lpExportSettings, FILE *lpStream, int nDepth);

static XODLExportSettings DefaultExportSettings = {
    .bUseHardTab = false,
    .bUseBinaryForm = false,
    .bMinify = false,
    .nIndentSize = 4
};

bool XODLDocumentExport(PXODLDocument lpXODL, PXODLExportSettings lpExportSettings, FILE *lpStream)
{
    if(lpExportSettings == NULL)
        lpExportSettings = &DefaultExportSettings;
    if(lpExportSettings->bUseBinaryForm == false) {
        ExportObject(lpXODL->lpRootObject, lpExportSettings, lpStream, 0);
        if(lpExportSettings->bMinify == false)
            fputc('\n', lpStream);
    } else
        return XODBExportDocument(lpXODL, lpStream);

    return true;
}

static void ExportObject(PXODLObject lpObject, PXODLExportSettings lpExportSettings, FILE *lpStream, int nDepth)
{
    bool bMinify = lpExportSettings->bMinify;
    bool bHeadIsPrinted = false;

    if(lpObject->bSchemaObject == false) {
#ifndef NDEBUG
        if(lpObject != lpObject->lpXODL->lpRootObject)
            assert(lpObject->lpXODL->eType == XODL_TYPE_Document);
#endif
        if(lpObject->szIdent && lpObject->szIdent[0] != ANON_OBJECT_IDENT_PREFIX) {
            fputs(lpObject->szIdent, lpStream);
            bHeadIsPrinted = true;
        }
        if(lpObject->szInheritObjectPath) {
            if(bHeadIsPrinted && !bMinify)
                fputc(' ', lpStream);
            fprintf(lpStream, "[%s]", lpObject->szInheritObjectPath);
            bHeadIsPrinted = true;
        }
        if(lpObject->szObjectID != OID_ANON) {
            if(bHeadIsPrinted) {
                if(!bMinify)
                    fputs(" : #", lpStream);
                else
                    fputs(":#", lpStream);
            }
            fputs(lpObject->szObjectID, lpStream);
            bHeadIsPrinted = true;
        }
    } else {
        if(lpObject->bSchemaOptionalObject) {
            fputc('?', lpStream);
            if(bMinify == false)
                fputc(' ', lpStream);
        }
        if(lpObject->lpRegExp) {
            fprintf(lpStream, "\"%s\"", lpObject->lpRegExp->szExp);
            bHeadIsPrinted = true;
        }
    }
    if(bHeadIsPrinted && !bMinify)
        fputc(' ', lpStream);
    fputc('{', lpStream);
    if(bMinify == false && XODLObjectGetMemberCount(lpObject) > 0)
        fputc('\n', lpStream);

    PXODLObjectMemberIterator lpIterator = XODLObjectMemberIteratorCreate(lpObject, false);

    void *lpMember;
    EXODLMemberKind eKind;
    while((lpMember = XODLObjectMemberIteratorNext(lpIterator, &eKind)) != NULL) {
        WriteIndents(lpStream, lpExportSettings, nDepth + 1);
        switch(eKind) {
            case XODL_MEMBER_Object:
                ExportObject(lpMember, lpExportSettings, lpStream, nDepth + 1);
                break;
            case XODL_MEMBER_Property:
                ExportProperty(lpMember, lpExportSettings, lpStream, nDepth + 1);
                break;
        }
        if(bMinify == false)
            fputc('\n', lpStream);
    }
    XODLObjectMemberIteratorDestroy(lpIterator);

    if(XODLObjectGetMemberCount(lpObject))
        WriteIndents(lpStream, lpExportSettings, nDepth);
    fputc('}', lpStream);
}

static void ExportProperty(PXODLProperty lpProp, PXODLExportSettings lpExportSettings, FILE *lpStream, int nIndent)
{
    if(lpProp->bOptional) {
        fputc('?', lpStream);
        if(lpExportSettings->bMinify == false)
            fputc(' ', lpStream);
    }
    if(lpProp->bSchemaProperty == false) {
        if(lpExportSettings->bMinify == false)
            fprintf(lpStream, "%s = ", lpProp->szIdent);
        else
            fprintf(lpStream, "%s=", lpProp->szIdent);
    } else {
        if(lpExportSettings->bMinify == false)
            fprintf(lpStream, "\"%s\" = ", lpProp->lpRegExp->szExp);
        else
            fprintf(lpStream, "\"%s\"=", lpProp->lpRegExp->szExp);
    }
    ExportValue(lpProp->lpValue, lpExportSettings, lpStream, nIndent);
    fputc(';', lpStream);
}

static void ExportValue(PXODLValue lpValue, PXODLExportSettings lpExportSettings, FILE *lpStream, int nDepth)
{
    switch(lpValue->eKind) {
        case XODL_VALUE_Nil: fputs("nil", lpStream); break;
        case XODL_VALUE_String: {
            size_t ulLength = strlen(lpValue->szVal);

            fputc('\"', lpStream);
            for(char *p = lpValue->szVal; *p; ) {
                switch(*p) {
                    case '\"': fputs("\\\"", lpStream); p++; break;
                    case '\t': fputs("\\t", lpStream); p++; break;
                    case '\f': fputs("\\f", lpStream); p++; break;
                    case '\v': fputs("\\v", lpStream); p++; break;
                    case '\\': fputs("\\\\", lpStream); p++; break;
                    default:
                        if((uint8_t)*p <= 0x7f) {
                            fputc(*p, lpStream);
                            p++;
                        } else {
                            uint32_t dwCodePoint;
                            int n = ConvertUTF8ToUCS((uint8_t*)p, ulLength - (p - lpValue->szVal), &dwCodePoint);

                            if(dwCodePoint <= 0xffff)
                                fprintf(lpStream, "\\u%04x", dwCodePoint);
                            else
                                fprintf(lpStream, "\\U%08x", dwCodePoint);
                            p += n;
                        }
                }
            }
            fputc('\"', lpStream);
        }   break;
        case XODL_VALUE_RawString: {
            char ch = strchr(lpValue->szVal, '\'') ? '\"' : '\'';
            fprintf(lpStream, "r%c%s%c", ch, lpValue->szVal, ch);
        } break;
        case XODL_VALUE_Data:
            fputs("b\"", lpStream);
            Base64EncodeToStream(lpStream, lpValue->lpRawData->lpData, lpValue->lpRawData->dwSize);
            fputc('\"', lpStream);
            break;
        case XODL_VALUE_Integer:
            fprintf(lpStream, "%"PRId64, lpValue->i64Val);
            break;
        case XODL_VALUE_Real:
            if(lpValue->dbVal == XODL_NaN)
                fputs("nan", lpStream);
            else if(lpValue->dbVal == XODL_Inf)
                fputs("inf", lpStream);
            else
                fprintf(lpStream, "%f", lpValue->dbVal);
            break;
        case XODL_VALUE_Boolean:
            fputs(lpValue->i64Val ? "true" : "false", lpStream);
            break;
        case XODL_VALUE_Object:
            ExportObject(lpValue->lpObject, lpExportSettings, lpStream, nDepth);
            break;
        case XODL_VALUE_ObjectID:
            fprintf(lpStream, "#%s", lpValue->szVal);
            break;
        case XODL_VALUE_ObjectPath:
            fputs(lpValue->szVal, lpStream);
            break;
        case XODL_VALUE_List: {
            uint32_t dwItemCount = XODLValueListGetItemCount(lpValue);

            fputc('[', lpStream);
            if(lpExportSettings->bMinify == false)
                fputc('\n', lpStream);
            if(dwItemCount) {
                uint32_t i = 0;

                XODLValueListIteratorInit(lpValue);
                for(PXODLValue lpItem = XODLValueListIteratorNextItem(lpValue); lpItem;
                    lpItem = XODLValueListIteratorNextItem(lpValue), i++) {
                    WriteIndents(lpStream, lpExportSettings, nDepth + 1);
                    ExportValue(lpItem, lpExportSettings, lpStream, nDepth + 1);
                    if(i + 1 < dwItemCount) {
                        fputc(',', lpStream);
                        if(lpExportSettings->bMinify == false)
                            fputc('\n', lpStream);
                    }
                }
                if(lpExportSettings->bMinify == false)
                    fputc('\n', lpStream);
            }
            WriteIndents(lpStream, lpExportSettings, nDepth);
            fputc(']', lpStream);
        } break;
        case XODL_VALUE_TYPE_NAME_INTEGER:
            fputs("INTEGER", lpStream); break;
        case XODL_VALUE_TYPE_NAME_BOOLEAN:
            fputs("BOOLEAN", lpStream); break;
        case XODL_VALUE_TYPE_NAME_REAL:
            fputs("REAL", lpStream); break;
        case XODL_VALUE_TYPE_NAME_STRING:
            fputs("STRING", lpStream); break;
        case XODL_VALUE_TYPE_NAME_OID:
            fputs("OID", lpStream); break;
        case XODL_VALUE_TYPE_NAME_OBJPATH:
            fputs("OBJPATH", lpStream); break;
        case XODL_VALUE_TYPE_NAME_LIST:
            fputs("LIST", lpStream); break;
        case XODL_VALUE_TYPE_NAME_ANYTYPE:
            fputc('*', lpStream); break;
        default:
            __UNREACHABLE("unhandled value kind");
    }
}

static void WriteIndents(FILE *lpStream, PXODLExportSettings lpExportSettings, int nIndentCount)
{
    if(lpExportSettings->bMinify)
        return;
    if(lpExportSettings->bUseHardTab) {
        for(int i = 0; i < nIndentCount; i++)
            fputc('\t', lpStream);
    } else {
        for(int i = 0; i < nIndentCount; i++) {
            for(int j = 0; j < lpExportSettings->nIndentSize; j++)
                fputc(' ', lpStream);
        }
    }
}
