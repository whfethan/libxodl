/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#ifndef LIBXODL_BASE64_H
#define LIBXODL_BASE64_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

void     Base64Encode(char *lpOutBuffer, void *pData, uint32_t dwSize);
void     Base64EncodeToStream(FILE *lpStream, void *lpData, uint32_t dwSize);
void     Base64Decode(char *lpOutBuffer, const char *lpBase64Data, uint32_t dwSize);
uint32_t Base64GetEncodedDataSize(uint32_t dwDataSize);
uint32_t Base64GetDecodedDataSize(const char *lpBase64Data, uint32_t dwBase64Size);

#endif /* LIBXODL_BASE64_H */
