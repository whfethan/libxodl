/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#ifndef LIBXODL_RANGE_TEST_H
#define LIBXODL_RANGE_TEST_H

#include <stdint.h>
#include <stdlib.h>

static inline int IsValueInRange(unsigned int val, unsigned int min, unsigned int max)
{
    return (val - min) <= (max - min);
}

static inline int IsSignedValueInRange64(int64_t val, int64_t min, int64_t max)
{
    return (val - max) * (val - min) <= 0;
}

static inline int IsValueInRangeUL(size_t val, size_t min, size_t max)
{
    return (val - min) <= (max - min);
}

#endif /* LIBXODL_RANGE_TEST_H */
