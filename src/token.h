/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#ifndef LIBXODL_TOKEN_H
#define LIBXODL_TOKEN_H

typedef enum {
    TOKEN_ERROR,
    TOKEN_EOF,

    TOKEN_Comma,
    TOKEN_LeftBrace,
    TOKEN_RightBrace,
    TOKEN_Equal,
    TOKEN_LeftSquare,
    TOKEN_RightSquare,
    TOKEN_Semicolon,
    TOKEN_Colon,

    TOKEN_Identifier,
    TOKEN_ObjectId,
    TOKEN_String,
    TOKEN_RawString,
    TOKEN_Base64String,
    TOKEN_Integer,
    TOKEN_IntegerB,
    TOKEN_IntegerO,
    TOKEN_IntegerH,
    TOKEN_Real,
    TOKEN_Boolean,
    TOKEN_ObjectPath,
    TOKEN_Nil,
#if 0
    TOKEN_Inf,
    TOKEN_Nan,
#endif

    TOKEN_SCHEMA_START,

    TOKEN_QuestionMark,
    TOKEN_INTEGER,
    TOKEN_BOOLEAN,
    TOKEN_REAL,
    TOKEN_STRING,
    TOKEN_OID,
    TOKEN_OBJPATH,
    TOKEN_LIST,
    TOKEN_ANYTYPE
} ETokenKind;

const char *TokenKindToString(ETokenKind eTokenKind);

#endif /* LIBXODL_TOKEN_H */
