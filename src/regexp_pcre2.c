/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#include <assert.h>
#include <string.h>

#define PCRE2_CODE_UNIT_WIDTH 8
#include "pcre2.h"
#include "regexp_interface.h"

void *RegularExpressionCompile (
    const char *        szRegExpr
) {
    return pcre2_compile((PCRE2_SPTR)szRegExpr, PCRE2_ZERO_TERMINATED, 0, NULL, NULL, NULL);
}

bool RegularExpressionMatch (
    void *              lpCompiledRegExp,
    const char *        szString
) {
    assert(lpCompiledRegExp != NULL);
    pcre2_match_data *lpmd = pcre2_match_data_create_from_pattern(lpCompiledRegExp, NULL);

    int rc = pcre2_match(lpCompiledRegExp, (PCRE2_SPTR)szString, strlen(szString), 0, 0, lpmd, NULL);
    pcre2_match_data_free(lpmd);

    return !rc;
}

void RegularExpressionFree(void *lpCompiledRegExp)
{
    pcre2_code_free(lpCompiledRegExp);
}
