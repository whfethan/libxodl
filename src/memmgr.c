/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#include <assert.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

#include "cmacros.h"
#include "memmgr.h"
#include "range_test.h"

#ifndef OPT_FULL_MEMORY_SECURITY
#   ifndef NDEBUG
#       define OPT_FULL_MEMORY_SECURITY   1
#   else
#       define OPT_FULL_MEMORY_SECURITY   0
#   endif
#endif

#define MEMBLOCK_SIGNATURE                  0xABCD5678
#define MEMBLOCK_SIGNATURE_DEAD             0xDEAD0F0F
#define INVALID_CHECKSUM                    0xFFFF
#ifdef __LP64__
#   define MEMBLOCK_REGION_GUARD            0x1F2F3F4F5A6B7C8DULL
#   define MemGuard                         uint64_t
#else
#   define MEMBLOCK_REGION_GUARD            0x1234EFAB
#   define MemGuard                         uint32_t
#endif
#define MAX_MEM_SIZE                        (__SIZE_MAX__ - sizeof(void*))
#define NEXT(b)                             (b)->BlockInfo.lpNextBlock
#define PREV(b)                             (b)->BlockInfo.lpPrevBlock
#define MEMBLOCK(mem)                       (PMemblock)((char*)mem - __offsetof(Memblock, data))

typedef struct SMemblock Memblock, *PMemblock;
struct SMemblock {
#if OPT_FULL_MEMORY_SECURITY
    uint32_t            dwSignature;
    uint16_t            wChecksum;
#   ifdef __LP64__
    uint16_t            reserved0;
#   endif
#endif
    struct {
        PMemoryManager  lpMemMgr;
#if OPT_FULL_MEMORY_SECURITY
        void *          lpCorrptionGuard;
#endif
        PMemblock       lpNextBlock;
        PMemblock       lpPrevBlock;
    }                   BlockInfo;
    char                data[0];
};

struct SMemoryManagerImpl {
    PMemblock   lpLastBlock;
};

/* forward prototypes */
static size_t AdjustSize(size_t ulSize, size_t ulAlignment);
#if OPT_FULL_MEMORY_SECURITY
static bool MemoryBlockIntegrityCheck(PMemblock lpBlock);
static void InvalidateMemoryBlock(PMemblock lpBlock);
static void UpdateMemoryBlockChecksum(PMemblock lpBlock);
static uint16_t CRC16Compute(uint8_t *lpData, size_t ulSize);
#else
#define InvalidateMemoryBlock(x)
#define UpdateMemoryBlockChecksum(x)
#define CRC16Compute(x)
#endif
#if OPT_FULL_MEMORY_SECURITY
static const size_t kMemoryOverhead = sizeof(Memblock) + sizeof(MemGuard);
static const MemGuard kMemRegionGuard = MEMBLOCK_REGION_GUARD;
#else
static const size_t kMemoryOverhead = sizeof(Memblock);
#endif

PMemoryManager MemoryManagerCreate(void)
{
    PMemoryManager lpMemMgr = malloc(sizeof(MemoryManager));

    lpMemMgr->lpLastBlock = NULL;
    return lpMemMgr;
}

PMemoryManager MemoryManagerGetInstance(void *lpMem)
{
    PMemblock lpBlock = MEMBLOCK(lpMem);

    assert(true == MemoryBlockIntegrityCheck(lpBlock) && "invalid memory block");
    return lpBlock->BlockInfo.lpMemMgr;
}

#if OPT_FULL_MEMORY_SECURITY
static __FORCE_INLINE__ void SetupMemoryGuard(PMemblock lpBlock, size_t ulSize)
{
    lpBlock->BlockInfo.lpCorrptionGuard = lpBlock->data + ulSize;
    memmove(lpBlock->data + ulSize, &kMemRegionGuard, sizeof(MemGuard));
}
#else
#   define SetupMemoryGuard(a, b)
#endif

void *MemoryManagerMalloc(PMemoryManager lpMemMgr, size_t ulSize)
{
    PMemblock lpBlock;
    size_t ulActualSize;

    assert(lpMemMgr != NULL);
    assert(IsValueInRangeUL(ulSize, 0, MAX_MEM_SIZE));

    if(ulSize == 0) return NULL;

    ulActualSize = AdjustSize(ulSize + kMemoryOverhead, sizeof(void*));
    lpBlock = malloc(ulActualSize);

#if OPT_FULL_MEMORY_SECURITY
    lpBlock->dwSignature = MEMBLOCK_SIGNATURE;
#endif
    SetupMemoryGuard(lpBlock, ulSize);
    lpBlock->BlockInfo.lpMemMgr = lpMemMgr;
    NEXT(lpBlock) = NULL;
    PREV(lpBlock) = lpMemMgr->lpLastBlock;

    if(lpMemMgr->lpLastBlock) {
        NEXT(lpMemMgr->lpLastBlock) = lpBlock;
        UpdateMemoryBlockChecksum(lpMemMgr->lpLastBlock);
    }
    lpMemMgr->lpLastBlock = lpBlock;

    UpdateMemoryBlockChecksum(lpBlock);

    return lpBlock->data;
}

void *MemoryManagerRealloc(PMemoryManager lpMemMgr, void *lpOldPtr, size_t ulNewSize)
{
    if(lpOldPtr == NULL)
        return MemoryManagerMalloc(lpMemMgr, ulNewSize);

    assert(lpMemMgr != NULL && lpMemMgr == MemoryManagerGetInstance(lpOldPtr) &&
        "the given lpOldPtr doesn't belong to the supplied memory manager");

    size_t ulAdjustedMemBlockSize = AdjustSize(ulNewSize + kMemoryOverhead, sizeof(void*));
    PMemblock lpBlock = MEMBLOCK(lpOldPtr);
    void *lpOldBlock = lpBlock;

    lpBlock = realloc(lpBlock, ulAdjustedMemBlockSize);
    SetupMemoryGuard(lpBlock, ulNewSize);
    if(lpOldBlock != lpBlock) {
        NEXT(PREV(lpBlock)) = lpBlock;
        PREV(NEXT(lpBlock)) = lpBlock;
        UpdateMemoryBlockChecksum(PREV(lpBlock));
        UpdateMemoryBlockChecksum(NEXT(lpBlock));
    }
    UpdateMemoryBlockChecksum(lpBlock);

    return lpBlock->data;
}

void MemoryManagerFree(void *lpMem)
{
    assert(lpMem != NULL);
    PMemblock lpBlock = MEMBLOCK(lpMem);

    assert(MemoryBlockIntegrityCheck(lpBlock) == true && "invalid memory block");

    if(PREV(lpBlock)) {
        NEXT(PREV(lpBlock)) = NEXT(lpBlock);
        UpdateMemoryBlockChecksum(PREV(lpBlock));
    }
    if(lpBlock->BlockInfo.lpNextBlock) {
        PREV(NEXT(lpBlock)) = PREV(lpBlock);
        UpdateMemoryBlockChecksum(NEXT(lpBlock));
     } else {
        PMemoryManager lpMemMgr = lpBlock->BlockInfo.lpMemMgr;
        lpMemMgr->lpLastBlock = PREV(lpBlock);
    }

    InvalidateMemoryBlock(lpBlock);

    free(lpBlock);
}

void MemoryManagerDestroy(PMemoryManager lpMemMgr)
{
    assert(lpMemMgr != NULL);

    for(PMemblock lpBlock = lpMemMgr->lpLastBlock; lpBlock != NULL; ) {
        PMemblock lpNextBlock = PREV(lpBlock);

        assert(MemoryBlockIntegrityCheck(lpBlock) == true && "memory block integrity check failed");
        InvalidateMemoryBlock(lpBlock);
        free(lpBlock);

        lpBlock = lpNextBlock;
    }
    free(lpMemMgr);
}

char *MemoryManagerDuplicateString(PMemoryManager lpMemMgr, const char *lpStr, size_t ulLength)
{
    assert(lpMemMgr != NULL);

    char *p = MemoryManagerMalloc(lpMemMgr, ulLength + 1);
    strlcpy(p, lpStr, ulLength + 1);

    return p;
}

#if OPT_FULL_MEMORY_SECURITY
static bool MemoryBlockIntegrityCheck(PMemblock lpBlock)
{
    switch(lpBlock->dwSignature) {
        case MEMBLOCK_SIGNATURE: break;
        case MEMBLOCK_SIGNATURE_DEAD:
            return false;
        default:
            return false;
    }
    uint16_t chksum = CRC16Compute((uint8_t*)&lpBlock->BlockInfo, sizeof(lpBlock->BlockInfo));
    if(chksum != lpBlock->wChecksum)
        return false;
    if(memcmp(lpBlock->BlockInfo.lpCorrptionGuard, &kMemRegionGuard, sizeof(MemGuard)))
        return false;

    return true;
}

static inline void UpdateMemoryBlockChecksum(PMemblock lpBlock)
{
    lpBlock->wChecksum = CRC16Compute((uint8_t*)&lpBlock->BlockInfo, sizeof(lpBlock->BlockInfo));
}

static inline void InvalidateMemoryBlock(PMemblock lpBlock)
{
    lpBlock->dwSignature = MEMBLOCK_SIGNATURE_DEAD;
    lpBlock->wChecksum = INVALID_CHECKSUM;
    memset(&lpBlock->BlockInfo, 0, sizeof(lpBlock->BlockInfo));
}

static inline uint16_t CRC16Compute(uint8_t *lpData, size_t ulSize)
{
    uint8_t x;
    uint16_t crc = 0xFFFF;

    while (ulSize--){
        x = crc >> 8 ^ *lpData++;
        x ^= x>>4;
        crc = (crc << 8) ^ ((uint16_t)(x << 12)) ^ ((uint16_t)(x <<5)) ^ ((uint16_t)x);
    }
    return crc;
}
#endif

static inline size_t AdjustSize(size_t ulSize, size_t ulAlignment)
{
    return (ulSize + (ulAlignment - 1)) & ~(ulAlignment - 1);
}
