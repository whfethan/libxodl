/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#ifndef LIBXODL_MEMMGR_H
#define LIBXODL_MEMMGR_H

#include <stdlib.h>

typedef struct SMemoryManagerImpl MemoryManager, *PMemoryManager;

PMemoryManager MemoryManagerCreate(void);
PMemoryManager MemoryManagerGetInstance(void *lpMem);
void *         MemoryManagerMalloc(PMemoryManager lpMemMgr, size_t ulSize);
void *         MemoryManagerRealloc(PMemoryManager lpMemMgr, void *lpOldPtr, size_t ulNewSize);
void           MemoryManagerFree(void *lpMem);
void           MemoryManagerDestroy(PMemoryManager lpMemMgr);
char *         MemoryManagerDuplicateString(PMemoryManager lpMemMgr, const char *lpStr, size_t ulLength);

#endif /* LIBXODL_MEMMGR_H */
