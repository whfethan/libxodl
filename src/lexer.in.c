/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

/*-T-T--------------------------------------------------------------------------
  NEEDS:    src/token.c
  NEEDS:    src/charfact.c
  NEEDS:    src/delegate.c
--------------------------------------------------------------------------T-T-*/

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

#include "cmacros.h"
#include "delegate.h"
#include "lexer.h"
#include "charfact.h"
#include "range_test.h"
/*!rules:re2c
    re2c:define:YYCTYPE = "unsigned char";
    re2c:flags:utf-8 = 1;
    re2c:yyfill:enable = 0;
    re2c:define:YYCURSOR = p;
    re2c:define:YYMARKER = q;

    nul                 = "\000";
    whitespace          = [ \t\v\f]+;
    newline             = "\r\n"|"\n";
    identifier          = [A-Z_a-z][A-Z_a-z0-9.\-]*;
    string_start        = ['"];
    raw_string_start    = "r"string_start;
    base64_string_start = "b"string_start;
    _sign               = "+"|"-";
    _uint               = [1-9][0-9]*;
    i_dec               = _sign? [0-9]+;
    i_hex               = '0x'[0-9A-Fa-f]+;
    i_oct               = '0o'[0-7]+;
    i_bin               = '0b'[0|1]+;
    boolean             = "true"|"false";
    _exp                = 'e' _sign? _uint;
    real_0              = i_dec [.] _uint? _exp?;
    real_1              = _uint _exp;
    real_2              = [.] _uint _exp?;
    real_3              = _sign? "inf";
    real_4              = "nan";
    real                = real_0 | real_1 | real_2 | real_3 | real_4;
    oid                 = [#][A-Z0-9]+;
    objref              = "\\\\"[A-Z_a-z0-9.\-/]+;
    nil                 = "nil";
    list_open           = [\[];
    list_close          = [\]];
    comma               = [,];
    semicolon           = [;];
    question_mark       = [?];
    colon               = [:];
    comment_start       = [\x2f][\x2a];
    left_brace          = [{];
    right_brace         = [}];
    equal               = [=];
    sk_integer          = "INTEGER";
    sk_boolean          = "BOOLEAN";
    sk_real             = "REAL";
    sk_string           = "STRING";
    sk_oid              = "OID";
    sk_objpath          = "OBJPATH";
    sk_list             = "LIST";
    sk_any              = [*];
 */
void LexerStateInit (PLexerState lpLexerState, PXODLUserDelegate lpDelegate, const char *fname, const char *szContent)
{
    assert(szContent != NULL);

    lpLexerState->lpDelegate = lpDelegate;
    lpLexerState->szFileName = fname;
    lpLexerState->curp = szContent;
    lpLexerState->dwCurrentCol = lpLexerState->dwCurrentLine = 1;
    lpLexerState->bErrorOccurred = false;
}

#define CURRENT_LEXER_LINENO    lpLexerState->dwCurrentLine
#define CURRENT_COLNUM          lpToken->dwCol + p - start
#define CURRENT_LEXER_COLNUM    lpLexerState->dwCurrentCol
#define CURRENT_FILENAME        lpLexerState->szFileName
#define CURRENT_TOKEN_COLNUM    lpToken->dwCol
#define REPORT_ERROR(...)       DelegatePrintf(lpUserDelegate, __VA_ARGS__); \
                                DelegateCallParseErrorHandler(lpUserDelegate, CURRENT_FILENAME, CURRENT_LEXER_LINENO, CURRENT_TOKEN_COLNUM, CURRENT_COLNUM); \
                                lpLexerState->bErrorOccurred = true; \
                                lpToken->eKind = TOKEN_ERROR; \
                                goto exit_loop
bool LexerReadToken(PLexerState lpLexerState, PToken lpToken)
{
    const char *p = lpLexerState->curp, *q;
    const char *start;
    PXODLUserDelegate lpUserDelegate = lpLexerState->lpDelegate;

    for(; lpLexerState->bErrorOccurred == false; ) {
        start = p;
        lpToken->dwCol = CURRENT_LEXER_COLNUM;
        lpToken->dwLine = CURRENT_LEXER_LINENO;
        /*!use:re2c
        nul             { lpToken->eKind = TOKEN_EOF; return false; }
        whitespace {
            CURRENT_LEXER_COLNUM += p - start;
            continue;
        }
        newline {
            CURRENT_LEXER_LINENO++;
            CURRENT_LEXER_COLNUM = 1;
            continue;
        }
        string_start {
            char ch = p[-1];

            lpToken->eKind = TOKEN_String;
            for(; *p != 0; p++) {
                uint16_t cfacts = GetCharacterFacts(*p);

                if(cfacts == FACT_Newline)
                    break;
                else if(*p == ch && *(p - 1) != '\\') {
                    p++;
                    goto exit_loop;
                } else if(*p == '\\') {
                    if(GetCharacterFacts(p[1]) & FACT_ValidEscapeCode) {
                        int nHexToCheck = 0;

                        p++;
                        if(*p == 'u') { p++; nHexToCheck = 4; }
                        else if(*p == 'U') { p++; nHexToCheck = 8; }
                        if(nHexToCheck) {
                            for(int i = 0; i < nHexToCheck; i++) {
                                if(!(GetCharacterFacts(*p) & FACT_Hex)) {
                                    if(*p) {
                                        REPORT_ERROR("expected %d hexadecimal characters but got %d", nHexToCheck, i + 1);
                                    } else {
                                        REPORT_ERROR("%s", "unclosed string literal");
                                    }
                                }
                            }
                        }
                    } else {
                        REPORT_ERROR("bad escape sequence character u+%04x", p[1]);
                    }
                } else if(unlikely(cfacts == FACT_NonPrintable)) {
                    REPORT_ERROR("invalid character u+%04x", *p);
                }
            }
            REPORT_ERROR("%s", "unclosed string literal");
            break;
        }
        raw_string_start {
            char ch = p[-1];

            lpToken->eKind = TOKEN_RawString;
            for(; *p != 0; p++) {
                uint16_t cfacts = GetCharacterFacts(*p);

                if(cfacts == FACT_Newline)
                    break;
                else if(*p == ch && *(p - 1) != '\\') {
                    p++;
                    goto exit_loop;
                } else if(unlikely(cfacts == FACT_NonPrintable)) {
                    REPORT_ERROR("invalid character u+%04x", *p);
                }
            }
            REPORT_ERROR("%s", "unclosed raw string literal");
            break;
        }
        base64_string_start {
            char ch = p[-1];
            uint8_t padding = 0;
            uint8_t charfacts;

            lpToken->eKind = TOKEN_Base64String;
            for(; *p != 0 && (charfacts = GetCharacterFacts(*p)) != FACT_Newline; p++) {
                if(*p == ch) {
                    p++; break;
                } else {
                    if(charfacts & FACT_Base64) {
                        continue;
                    } else if(*p == '=') {
                        if(padding) {
                            REPORT_ERROR("%s", "malformed encoded byte (base64) string");
                            goto exit_loop;
                        }
                        while(*p == '=') padding++, p++;
                    } else {
                        REPORT_ERROR("encoded byte string contains unexpected character u+%04x", (uint8_t)*p);
                        goto exit_loop;
                    }
                }
            }
            uint32_t base64len = p - start - 3;
            if(padding > 2 || ((base64len >> 2) << 2) != base64len) {
                REPORT_ERROR("%s", "malformed encoded byte string (bad length)");
            } else if(*(p - 1) != ch) {
                REPORT_ERROR("%s", "unclosed encoded byte string literal");
            }
            break;
        }
        i_dec           { lpToken->eKind = TOKEN_Integer; break; }
        i_hex           { lpToken->eKind = TOKEN_IntegerH; break; }
        i_oct           { lpToken->eKind = TOKEN_IntegerO; break; }
        i_bin           { lpToken->eKind = TOKEN_IntegerB; break; }
        boolean         { lpToken->eKind = TOKEN_Boolean; break; }
        real            { lpToken->eKind = TOKEN_Real; break; }
        oid             { lpToken->eKind = TOKEN_ObjectId; break; }
        objref          {
            lpToken->eKind = TOKEN_ObjectPath;
            if(unlikely(p - start > XODL_OBJPATH_MAXLENGTH)) {
                REPORT_ERROR("object path exceeds the max limit of %d bytes", XODL_OBJPATH_MAXLENGTH);
            }
            break;
        }
        nil             { lpToken->eKind = TOKEN_Nil; break; }
        list_open       { lpToken->eKind = TOKEN_LeftSquare; break; }
        list_close      { lpToken->eKind = TOKEN_RightSquare; break; }
        comma           { lpToken->eKind = TOKEN_Comma; break; }
        semicolon       { lpToken->eKind = TOKEN_Semicolon; break; }
        colon           { lpToken->eKind = TOKEN_Colon; break; }
        question_mark   { lpToken->eKind = TOKEN_QuestionMark; break; }
        left_brace      { lpToken->eKind = TOKEN_LeftBrace; break; }
        right_brace     { lpToken->eKind = TOKEN_RightBrace; break; }
        equal           { lpToken->eKind = TOKEN_Equal; break; }
        sk_integer      { lpToken->eKind = TOKEN_INTEGER; break; }
        sk_boolean      { lpToken->eKind = TOKEN_BOOLEAN; break; }
        sk_real         { lpToken->eKind = TOKEN_REAL; break; }
        sk_string       { lpToken->eKind = TOKEN_STRING; break; }
        sk_oid          { lpToken->eKind = TOKEN_OID; break; }
        sk_objpath      { lpToken->eKind = TOKEN_OBJPATH; break; }
        sk_list         { lpToken->eKind = TOKEN_LIST; break; }
        sk_any          { lpToken->eKind = TOKEN_ANYTYPE; break; }
        comment_start {
            bool bBrokenComment = true;
            for(; *p != '\0'; p++) {
                switch(*p) {
                    case '*':
                        if(*(p + 1) == '/') {
                            p += 2;
                            bBrokenComment = false;
                            goto exit_comment_loop;
                        }
                        break;
                    case '\r': p++;
                    case '\n':
                        p++;
                        CURRENT_LEXER_LINENO++;
                        CURRENT_LEXER_COLNUM = 1;
                        break;
                }
            }
exit_comment_loop:
            if(bBrokenComment == false) {
                CURRENT_LEXER_COLNUM += p - start;
                lpLexerState->curp = p;
                continue;
            } else {
                REPORT_ERROR("%s", "unclosed comment block");
            }
        }
        identifier {
            lpToken->eKind = TOKEN_Identifier;
            if(unlikely(p - start > XODL_IDENTIFIER_MAXLENGTH)) {
                REPORT_ERROR("identifier is too long (max: %d)", XODL_IDENTIFIER_MAXLENGTH);
            }
            break;
        }
        * {
            REPORT_ERROR("%s", "unrecognised token");
            break;
        }
        */
    }
exit_loop:
    lpToken->lpStart = start;
    lpToken->dwLength = p - start;
    CURRENT_LEXER_COLNUM += p - start;
    lpLexerState->curp = p;

    return !lpLexerState->bErrorOccurred;
}

ETokenKind LexerPeek(PLexerState lpLexerState, PToken lpTokenOut, uint8_t nCount)
{
    Token t;
    uint32_t dwOldLine = lpLexerState->dwCurrentLine;
    uint32_t dwOldCol = lpLexerState->dwCurrentCol;
    const char *lastp = lpLexerState->curp;

    if(lpTokenOut == NULL)
        lpTokenOut = &t;

    while(nCount--)
        LexerReadToken(lpLexerState, lpTokenOut);

    lpLexerState->dwCurrentLine = dwOldLine;
    lpLexerState->dwCurrentCol = dwOldCol;
    lpLexerState->curp = lastp;

    return lpTokenOut->eKind;
}
