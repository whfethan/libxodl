/*>>============================================================================
  Copyright (C) 2021 Weihao Feng. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================>>*/

#ifndef LIBXODL_DELEGATE_H
#define LIBXODL_DELEGATE_H

#include "xodl.h"
#include "buffer.h"

typedef struct {
    FXODLMessageHandler     fnMessageHandler;
    void *                  lpUserMessageHandlerArgument;
    FXODLParseErrorHandler  fnParseErrorHandler;
    void *                  lpUserParseErrorHandlerArgument;
    Buffer                  MessageBuffer;
} XODLUserDelegate, *PXODLUserDelegate;

void DelegateInit (
    PXODLUserDelegate       lpDelegate
);
void DelegateSetMessageHandler (
    PXODLUserDelegate       lpDelegate,
    FXODLMessageHandler     fnMessageHandler,
    void *                  lpUserArg
);
void DelegateSetParseErrorHandler (
    PXODLUserDelegate       lpDelegate,
    FXODLParseErrorHandler  fnParseErrorHandler,
    void *                  lpUserArg
);
void DelegateFree (
    PXODLUserDelegate       lpDelegate
);
void DelegateClearMessage (
    PXODLUserDelegate       lpDelegate
);
void DelegatePrintf (
    PXODLUserDelegate       lpDelegate,
    const char *            szFormatString,
                            ...
);
void DelegateCallParseErrorHandler (
    PXODLUserDelegate       lpDelegate,
    const char *            szFileName,
    uint32_t                dwLine,
    uint32_t                dwCol,
    uint32_t                dwErrCol
);
void DelegateCallMessageHandler (
    PXODLUserDelegate       lpDelegate,
    const char *            szFileName,
    EMessageLevel           eMessageLevel
);

void DelegateWriteObjectInfo (
    PXODLUserDelegate       lpDelegate,
    PXODLObject             lpObject
);

#endif /* LIBXODL_DELEGATE_H */
